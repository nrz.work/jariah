<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => \Hash::make('password'),
            'phone_number' => '0123456789',
            'role' => 'Admin',
        ]);

        $comp = \DB::table('company')->insertGetId([
            'company_name' => 'Jariah Sdn Bhd',
            'owner_email' => 'admin@gmail.com',
            'company_email' => 'jariah@gmail.com',
            'company_phone' => '0123456789'
        ]);

        $cat = \DB::table('campaign_categories')->insertGetId([
            'category_name' => 'Pendidikan',
        ]);

        $camp = \DB::table('campaign')->insertGetId([
            'company_id' => $comp,
            'campaign_name' => 'Sumbangan Palestin',
            'campaign_category' => $cat,
            'image' => 'cover_kempen.jpeg',
            'start_date' => '2023-01-01',
            'end_date' => '2024-01-01',
            'campaign_description' => 'Lorem ipsum dolor sit amet',
            'total_donation' => 20000.00,
            'flag_kilat' => 1
        ]);

        foreach($camp as $a)
        \DB::table('campaign_likes')->insert([
            'campaign_id' => $a,
            'user_email' => 'admin@gmail.com'
        ]);

        \DB::table('campaign')->insert([
            'company_id' => $comp,
            'campaign_name' => 'Sumbangan Banjir',
            'campaign_category' =>  $cat,
            'image' => 'cover_kempen.jpeg',
            'start_date' => '2023-01-01',
            'end_date' => '2024-01-01',
            'campaign_description' => 'Lorem ipsum dolor sit amet',
            'total_donation' => 10000.00,
            'flag_kilat' => 0
        ]);
    }
}
