<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company', function (Blueprint $table) {
            //
            $table->string('ssm_no')->nullable();
            $table->text('company_details')->nullable();
            $table->text('address')->nullable();
            $table->string('bank_code')->nullable();
            $table->string('bank_owner')->nullable();
            $table->string('account_no')->nullable();
            $table->string('ssm_cert')->nullable();
            $table->string('company_profile')->nullable();
            $table->string('bank_statement')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company', function (Blueprint $table) {
            //
            $table->dropColumn('ssm_no');
            $table->dropColumn('company_details');
            $table->dropColumn('address');
            $table->dropColumn('bank_code');
            $table->dropColumn('bank_owner');
            $table->dropColumn('account_no');
            $table->dropColumn('ssm_cert');
            $table->dropColumn('company_profile');
            $table->dropColumn('bank_statement');
        });
    }
}
