<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('company');
            $table->string('campaign_name');
            $table->unsignedBigInteger('campaign_category_id');
            $table->foreign('campaign_category_id')->references('id')->on('campaign_categories');
            $table->string('image');
            $table->string('start_date');
            $table->string('end_date');
            $table->longText('campaign_description')->nullable();
            $table->decimal('total_donation',10,2);
            $table->boolean('flag_kilat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}
