<?php

use Illuminate\Support\Facades\Route;

require __DIR__ . '/auth.php';

/*
|--------------------------------------------------------------------------
| User
|--------------------------------------------------------------------------
*/

// Home
Route::get('/', [\App\Http\Controllers\HomeController::class, 'main'])->name('home');
Route::get('activities', [\App\Http\Controllers\HomeController::class, 'activities'])->name('activities');
Route::get('personal-account', [\App\Http\Controllers\AccountController::class, 'personal'])->name('personal-account');
Route::get('tentang-kami', [\App\Http\Controllers\HomeController::class, 'about'])->name('about');

Route::get('search-campaign', [\App\Http\Controllers\HomeController::class, 'search_campaign'])->name('search-campaign');
Route::get('list-campaign/{search}', [\App\Http\Controllers\HomeController::class, 'list_campaign'])->name('list-campaign');


// Donate Section
Route::get('donate', [App\Http\Controllers\User\DonateController::class, 'index'])->name('donate');
Route::post('donate/store', [App\Http\Controllers\User\DonateController::class, 'store'])->name('donate.store');
Route::post('donate/store-guest', [App\Http\Controllers\User\DonateController::class, 'store_guest'])->name('donate.store-guest');
Route::get('donate/show/{id}', [App\Http\Controllers\User\DonateController::class, 'show'])->name('donate.show');
Route::get('process-loading', [App\Http\Controllers\User\DonateController::class, 'loading'])->name('process-loading');
Route::get('thank-you', [App\Http\Controllers\User\DonateController::class, 'thankyou'])->name('thank-you');
Route::get('check-payment', [App\Http\Controllers\User\DonateController::class, 'check_payment'])->name('check-payment');
Route::get('check-payment-guest', [App\Http\Controllers\User\DonateController::class, 'check_payment_guest'])->name('check-payment-guest');
Route::get('payment-failed', [App\Http\Controllers\User\DonateController::class, 'failed'])->name('payment-failed');
// Route::get('receipt-email', [App\Http\Controllers\User\DonateController::class, 'receipt_email']);

// Campaign Section
Route::get('campaign', [App\Http\Controllers\User\CampaignController::class, 'index'])->name('campaign.index');
Route::post('campaign-like', [\App\Http\Controllers\CampaignController::class, 'campaign_likes'])->name('campaign.like');
Route::get('campaign/f/{filter}', [\App\Http\Controllers\User\CampaignController::class, 'campaign_filter'])->name('campaign.filter');
Route::get('campaign/show/{id}', [\App\Http\Controllers\User\CampaignController::class, 'show'])->name('campaign.show');
Route::get('campaign/campaign-progress/{campaign_id}', [\App\Http\Controllers\User\CampaignController::class, 'progress'])->name('campaign.progress');
Route::get('campaign/organization-profile/{company_id}', [\App\Http\Controllers\User\CampaignController::class, 'ngo_profile'])->name('campaign.ngo-profile');


/*
|--------------------------------------------------------------------------
| Admin Panel
|--------------------------------------------------------------------------
*/

// Dashboard
Route::get('/dashboard', [\App\Http\Controllers\Admin\DashboardController::class, 'dashboard'])->name('dashboard');

// Statistic
Route::get('statistic-by-date', [\App\Http\Controllers\Admin\DashboardController::class, 'statistic_by_date'])->name('statistic-by-date');
Route::get('statistic-by-year', [\App\Http\Controllers\Admin\DashboardController::class, 'statistic_by_year'])->name('statistic-by-year');

// Organization
Route::get('/organization', [\App\Http\Controllers\Admin\OrganizationController::class, 'index'])->name('organization.index');
Route::get('/update-organization', [\App\Http\Controllers\Admin\OrganizationController::class, 'update'])->name('organization.update');
Route::post('/edit-organization/{company_id}', [\App\Http\Controllers\Admin\OrganizationController::class, 'edit'])->name('organization.edit');

// Account
Route::get('/account', [\App\Http\Controllers\Admin\AccountController::class, 'index'])->name('account.index');
Route::post('/edit-account', [\App\Http\Controllers\Admin\AccountController::class, 'edit'])->name('account.edit');

// Campaign
Route::get('/new-campaign', [\App\Http\Controllers\Admin\CampaignController::class, 'create'])->name('campaign.create');
Route::post('/store-campaign', [\App\Http\Controllers\Admin\CampaignController::class, 'store'])->name('campaign.store');
Route::get('/update-campaign/{campaign_id}', [\App\Http\Controllers\Admin\CampaignController::class, 'update'])->name('campaign.update');
Route::post('/edit-campaign/{campaign_id}', [\App\Http\Controllers\Admin\CampaignController::class, 'edit'])->name('campaign.edit');
Route::get('/view-campaign', [\App\Http\Controllers\Admin\CampaignController::class, 'index'])->name('campaign.index');
Route::post('/delete/{campaign_id}', [\App\Http\Controllers\Admin\CampaignController::class, 'destroy'])->name('campaign.destroy');
Route::get('/statistic-by-campaign/{campaign_id}', [\App\Http\Controllers\Admin\CampaignController::class, 'statistic'])->name('campaign.statistic');

/// Campaign Progress
Route::get('/campaign-progress/{campaign_id}', [\App\Http\Controllers\Admin\CampaignProgressController::class, 'create'])->name('campaign-progress.create');
Route::post('/store-progress/{campaign_id}', [\App\Http\Controllers\Admin\CampaignProgressController::class, 'store'])->name('campaign-progress.store');
Route::get('/delete-progress/{progress_id}', [\App\Http\Controllers\Admin\CampaignProgressController::class, 'destroy'])->name('campaign-progress.destroy');
Route::post('/assign-party/{campaign_id}', [\App\Http\Controllers\Admin\CampaignProgressController::class, 'assign'])->name('campaign-progress.assign');
Route::post('edit-progress/{id}', [\App\Http\Controllers\Admin\CampaignProgressController::class, 'update'])->name('campaign-progress.update');

// Donation
Route::get('donation', [\App\Http\Controllers\Admin\DashboardController::class, 'donation'])->name('donation');

// Transaction History
Route::get('/transaction-list', [\App\Http\Controllers\Admin\DashboardController::class, 'transaction_list'])->name('transaction-list');
Route::get('/financial-report', [\App\Http\Controllers\Admin\DashboardController::class, 'financial_report'])->name('financial-report');

// User
Route::get('/view-admin', [\App\Http\Controllers\Admin\UserController::class, 'admin'])->name('admin');
Route::get('/new-admin', [\App\Http\Controllers\Admin\UserController::class, 'new_admin'])->name('new-admin');
Route::post('/store-admin', [\App\Http\Controllers\Admin\UserController::class, 'store_admin'])->name('store-admin');
Route::get('/view-user/{user_id}', [\App\Http\Controllers\Admin\UserController::class, 'view_user'])->name('view-user');
Route::post('/edit-user/{user_id}', [\App\Http\Controllers\Admin\UserController::class, 'edit_user'])->name('edit-user');
Route::get('delete-user/{user_id}', [\App\Http\Controllers\Admin\UserController::class, 'destroy'])->name('delete-user');
Route::get('/ngo', [\App\Http\Controllers\Admin\UserController::class, 'ngo'])->name('ngo');
Route::get('/new-ngo', [\App\Http\Controllers\Admin\UserController::class, 'new_ngo'])->name('new-ngo');
Route::post('/store-ngo', [\App\Http\Controllers\Admin\UserController::class, 'store_ngo'])->name('store-ngo');
Route::get('/donor', [\App\Http\Controllers\Admin\UserController::class, 'donor'])->name('donor');

Route::get('/sitemap.xml', [\App\Http\Controllers\Admin\SitemapController::class, 'index']);



