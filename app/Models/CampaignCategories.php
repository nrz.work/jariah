<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class CampaignCategories extends Model
{
  use HasFactory;

  protected $connection = 'mongodb';

  protected $fillable = [
    'category_name'
  ];

  // public function campaigns()
  // {
  //   return $this->belongsTo(Campaign::class, '_id');
  //   // return $this->hasMany(Campaign::class);
  // }
}
