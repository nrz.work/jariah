<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Campaign extends Model
{
    use HasFactory;

    // protected $primaryKey = '_id';

    protected $connection = 'mongodb';

    protected $table = 'campaign';

    // protected $dates = ['created_at', 'updated_at', 'start_date', 'end_date'];

    protected $fillable = [
        'company_id',
        'campaign_name',
        'campaign_category',
        'image',
        'start_date',
        'end_date',
        'campaign_description',
        'total_donation',
        'flag_kilat',
        'third_party_id',
        'campaign_progress_id',
        'slug',
        'campaign_status'
    ];

    // public function category()
    // {
    //     return $this->hasOne(CampaignCategories::class, '_id', 'campaign_category');
    //     // return $this->belongsTo(CampaignCategories::class, 'campaign_category');
    // }

    public function campaignProgresses()
    {
        return $this->hasMany(CampaignProgress::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function donation()
    {
        return $this->hasMany(Donation::class)->where('billplz_status', 'paid');
    }

    // public function likes()
    // {
    //     return $this->hasMany(CampaignLikes::class, 'campaign_id');
    // }
}
