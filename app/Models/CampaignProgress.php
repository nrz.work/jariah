<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class CampaignProgress extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $table = 'campaign_progress';

    protected $fillable = [
        'description',
        'image',
        'campaign_id'
    ];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
}
