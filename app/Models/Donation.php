<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Donation extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $table = 'donation';

    protected $fillable = [
        'email',
        'campaign_id',
        'payment_method',
        'amount',
        'billplz_id',
        'billplz_status',
        'name',
        'phone_number',
        'doa',
        'invoice_id',
        'agreement_name',
        'agreement_information'
    ];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getLastInvoiceNumber()
    {
        $lastDonation = self::orderByDesc('created_at')->first();

        if ($lastDonation) {
            $invoiceIdParts = explode(' - ', $lastDonation->invoice_id);

            if (count($invoiceIdParts) === 2) {
                return intval($invoiceIdParts[1]);
            }
        }

        // If no last donation or invoice ID format doesn't match, return an initial value
        return 999; // Adjust this as needed
    }
}
