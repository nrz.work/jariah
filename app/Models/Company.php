<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $table = 'company';

    protected $fillable = [
        'owner_email',
        'company_name',
        'company_email',
        'company_phone',
        'ssm_no',
        'company_details',
        'address',
        'bank_code',
        'bank_owner',
        'account_no',
        'ssm_cert',
        'company_profile',
        'bank_statement',
    ];
}
