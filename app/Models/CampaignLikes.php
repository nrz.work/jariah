<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class CampaignLikes extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $table = 'campaign_likes';

    protected $fillable = [
      'campaign_id',
      'user_email'
    ];

    // public function campaign()
    // {
    //     return $this->belongsTo(Campaign::class, '_id');
    // }
}
