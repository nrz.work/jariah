<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class JariahNotification extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $table = 'jariah_notifications';

    protected $fillable = [
        'campaign_id',
        'description',
        'type_of_notification',
        'notification_status',
        'user_email',
        'billplz_id',
        'billplz_status'
    ];
}
