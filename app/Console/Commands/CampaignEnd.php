<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Campaign;

class CampaignEnd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:end';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;

        $now = Carbon::now()->toDateString();

        // Using Laravel's Eloquent with MongoDB
        Campaign::where('end_date', '<', $now)
            // ->where('campaign_status', '!=', 'deactive')
            ->update(['campaign_status' => 'deactive']);

        $this->info('Expired campaigns have been deactivated.');
    }
}
