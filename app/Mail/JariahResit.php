<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Campaign;
use App\Models\Donation;

class JariahResit extends Mailable
{
    use Queueable, SerializesModels;

    public $fullname;
    public $totalDonation;
    public $campaignNames;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fullname, $totalDonation, $campaignNames)
    {
        $this->fullname = $fullname;
        $this->totalDonation = $totalDonation;
        $this->campaignNames = $campaignNames;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('emails.resit');

        $campaigns = Campaign::all();
        $randomCampaigns = $campaigns->random(2);

        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Terima kasih ' . $this->fullname . ' atas sumbangan anda!')
            ->view('emails.resit')
            ->with([
                'randomCampaigns' => $randomCampaigns,
                'fullname' => $this->fullname,
                'total_donation' => $this->totalDonation,
                'campaignNames' => $this->campaignNames
            ]);
    }
}
