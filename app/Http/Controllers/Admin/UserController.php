<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Company;
use App\Models\Campaign;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin()
    {
        $admins = User::where('role', 'Admin')->paginate(15);

        return view('admin.user.admin', ['admins' => $admins]);
    }

    public function new_admin()
    {
        return view('admin.user.new_admin');
    }

    public function store_admin(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'ic_no' => 'required',
            'email' => 'required',
            'phone_number' => 'required'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'password' => Hash::make($request->ic_no),
            'role' => 'Admin',
            'ic_no' => $request->ic_no,
            'status' => 1,
        ]);

        return redirect('admin')->with('addadmin', 'New admin has been created successfully.');
    }

    public function view_user($user_id)
    {
        $user = User::where('_id', $user_id)->first();
        $company = Company::where('owner_email', $user->email)->first();

        return view('admin.user.view', ['user' => $user, 'company' => $company]);
    }

    public function edit_user($user_id, Request $request)
    {
        $user = User::where('_id', $user_id)->first();
        $company = Company::where('owner_email', $user->email)->first();

        if ($request->hasFile('ic_copy')) {
            $image_path = 'admin/ic_copy';
            $path = 'img_' . uniqid() . '.' . $request->file('ic_copy')->extension();

            $request->file('ic_copy')->storeAs($image_path, $path, 'public');

            $ic_copy = 'https://jariah.berqat.com/storage/admin/ic_copy/' . $path;
        } else {
            $ic_copy = null;
        }

        if ($request->hasFile('organization_cert')) {
            $image_path = 'admin/organization_cert';
            $path = 'img_' . uniqid() . '.' . $request->file('organization_cert')->extension();

            $request->file('organization_cert')->storeAs($image_path, $path, 'public');

            $organization_cert = 'https://jariah.berqat.com/storage/admin/organization_cert/' . $path;
        } else {
            $organization_cert = null;
        }

        if ($request->hasFile('company_profile')) {
            $image_path = 'admin/company_profile';
            $path = 'img_' . uniqid() . '.' . $request->file('company_profile')->extension();

            $request->file('company_profile')->storeAs($image_path, $path, 'public');

            $company_profile = 'https://jariah.berqat.com/storage/admin/company_profile/' . $path;
        } else {
            $company_profile = null;
        }

        if ($request->hasFile('bank_statement')) {
            $image_path = 'admin/company_profile';
            $path = 'img_' . uniqid() . '.' . $request->file('bank_statement')->extension();

            $request->file('bank_statement')->storeAs($image_path, $path, 'public');

            $bank_statement = 'https://jariah.berqat.com/storage/admin/bank_statement/' . $path;
        } else {
            $bank_statement = null;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->password = Hash::make($request->password);
        $user->role = $request->role;
        $user->ic_no = $request->ic_no;
        $user->status = $request->status;
        if ($request->hasFile('ic_copy')) {
            $user->ic_copy = $ic_copy;
        }

        $company->owner_email = $request->email;
        $company->company_name = $request->company_name;
        $company->company_email = $request->company_email;
        $company->company_phone = $request->company_phone;
        $company->ssm_no = $request->ssm_no;
        $company->company_details = $request->company_details;
        $company->address = $request->address;
        $company->bank_code = $request->bank_code;
        $company->bank_owner = $request->bank_owner;
        $company->account_no = $request->account_no;
        $company->ssm_cert = $organization_cert;
        $company->company_profile = $company_profile;
        $company->bank_statement = $bank_statement;
        if ($request->hasFile('organization_cert')) {
            $company->ssm_cert = $organization_cert;
        }
        if ($request->hasFile('company_profile')) {
            $company->company_profile = $company_profile;
        }
        if ($request->hasFile('bank_statement')) {
            $company->bank_statement = $bank_statement;
        }
        
        $user->save();
        $company->save();

        return back()->with('updateuser', 'User has been updated successfully.');
    }

    public function ngo()
    {
        $ngos = User::where('role', 'NGO')->paginate(15);

        return view('admin.user.ngo', ['ngos' => $ngos]);
    }

    public function new_ngo()
    {
        return view('admin.user.new_ngo');
    }

    public function store_ngo(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'ic_no' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            // 'ssm_no' => 'required',
            // 'company_phone' => 'required',
            // 'bank_code' => 'required',
            // 'bank_owner' => 'required',
            // 'account_no' => 'required'
        ]);


        if ($request->hasFile('ic_copy')) {
            $image_path = 'admin/ic_copy';
            $path = 'img_' . uniqid() . '.' . $request->file('ic_copy')->extension();

            $request->file('ic_copy')->storeAs($image_path, $path, 'public');

            $ic_copy = 'https://jariah.berqat.com/storage/admin/ic_copy/' . $path;
        } else {
            $ic_copy = null;
        }

        if ($request->hasFile('organization_cert')) {
            $image_path = 'admin/organization_cert';
            $path = 'img_' . uniqid() . '.' . $request->file('organization_cert')->extension();

            $request->file('organization_cert')->storeAs($image_path, $path, 'public');

            $organization_cert = 'https://jariah.berqat.com/storage/admin/organization_cert/' . $path;
        } else {
            $organization_cert = null;
        }

        if ($request->hasFile('company_profile')) {
            $image_path = 'admin/company_profile';
            $path = 'img_' . uniqid() . '.' . $request->file('company_profile')->extension();

            $request->file('company_profile')->storeAs($image_path, $path, 'public');

            $company_profile = 'https://jariah.berqat.com/storage/admin/company_profile/' . $path;
        } else {
            $company_profile = null;
        }

        if ($request->hasFile('bank_statement')) {
            $image_path = 'admin/company_profile';
            $path = 'img_' . uniqid() . '.' . $request->file('bank_statement')->extension();

            $request->file('bank_statement')->storeAs($image_path, $path, 'public');

            $bank_statement = 'https://jariah.berqat.com/storage/admin/bank_statement/' . $path;
        } else {
            $bank_statement = null;
        }

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'password' => Hash::make($request->ic_no),
            'role' => 'NGO',
            'ic_no' => $request->ic_no,
            'status' => 1,
            'ic_copy' => $ic_copy,
        ]);

        Company::create([
            'owner_email' => $request->email,
            'company_name' => $request->company_name,
            'company_email' => $request->company_email,
            'company_phone' => $request->company_phone,
            'ssm_no' => $request->ssm_no,
            'company_details' => $request->company_details,
            'address' => $request->address,
            'bank_code' => $request->bank_code,
            'bank_owner' => $request->bank_owner,
            'account_no' => $request->account_no,
            'ssm_cert' => $organization_cert,
            'company_profile' => $company_profile,
            'bank_statement' => $bank_statement,
        ]);

        return redirect('ngo')->with('success', 'New NGO has been created successfully.');
    }

    public function donor()
    {
        $donors = User::where('role', 'User')->paginate(15);

        return view('admin.user.donor', ['donors' => $donors]);
    }

    public function destroy($user_id)
    {
        $user = User::find($user_id);
        // dd($user->email);
        $company = Company::where('owner_email', $user->email)->first();
        // dd($company->_id);
        $campaign = Campaign::where('company_id', $company['_id']);

        $campaign->delete();
        $company->delete();
        $user->delete();


        return back()->with('deleteuser', 'User has been deleted successfully.');
    }
}
