<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use Auth;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $company = Company::where('owner_email', Auth::user()->email)->first();
        // dd($company);
        return view('admin.organization.view', ['company'=> $company]);
    }

    public function update(){
        $company = Company::where('owner_email', Auth::user()->email)->first();
        
        return view('admin.organization.update', ['company'=> $company]);
    }

    public function edit($company_id, Request $request){    
        $company = Company::where('_id', $company_id)->first();

        if($request->hasFile('organization_cert'))
        {
            $cert_path = 'public/admin/organization_cert';
            $path = 'img_' . uniqid().'.'.$request->file('organization_cert')->extension();
            $request->file('organization_cert')->storeAs($cert_path, $path);
            $cert_image = 'https://jariah.berqat.com/storage/admin/organization_cert/' . $path;
        }

        if($request->hasFile('company_profile'))
        {
            $profile_path = 'public/admin/company_profile';
            $path = 'img_' . uniqid().'.'.$request->file('company_profile')->extension();
            $request->file('company_profile')->storeAs($profile_path, $path);
            $profile_image = 'https://jariah.berqat.com/storage/admin/company_profile/' . $path;
        }

        if($request->hasFile('bank_statement'))
        {
            $bank_path = 'public/admin/bank_statement';
            $path = 'img_' . uniqid().'.'.$request->file('bank_statement')->extension();
            $request->file('bank_statement')->storeAs($bank_path, $path);
            $bank_image = 'https://jariah.berqat.com/storage/admin/bank_statement/' . $path;
        }

        $company->ssm_no = $request->ssm_no;
        $company->address = $request->address;
        $company->company_phone = $request->company_phone;
        $company->company_details = $request->company_details;
        if($request->hasFile('organization_cert'))
        {
            $company->ssm_cert = $cert_image;
        }
        if($request->hasFile('company_profile'))
        {
            $company->company_profile = $profile_image;
        }
        if($request->hasFile('bank_statement'))
        {
            $company->bank_statement = $bank_image;
        }
        $company->bank_code = $request->bank_code;
        $company->bank_owner = $request->bank_owner;
        $company->account_no = $request->account_no;
        $company->save();

        return redirect('organization')->with('updatecompany','Organization has been updated successfully.');
    }

}
