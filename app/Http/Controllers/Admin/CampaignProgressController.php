<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\CampaignProgress;
use App\Models\Company;

class CampaignProgressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($campaign_id)
    {   
        $campaign = Campaign::where('_id', $campaign_id )->first();
        $progress = CampaignProgress::where('campaign_id', $campaign_id )->paginate(15);

        return view('admin.campaign.view_progress', [ 'campaign' => $campaign, 'progress' => $progress ]);
    }

    public function store($campaign_id, Request $request)
    {
        // if ($request->hasFile('image')) {
        //     $image_path = 'admin/campaign_progress';
        //     $path = 'img_' . uniqid() . '.' . $request->file('image')->extension();

        //     // Store the image in the 'public' disk
        //     $request->file('image')->storeAs($image_path, $path, 'public');

        //     // Generating the full URL for the image
        //     $image = asset('storage/' . $image_path . '/' . $path);
        // } else {
        //     // If no image is uploaded, we'll set the path to null or you can set a default image path
        //     $path = null;
        // }

        if ($request->hasFile('image')) {
            $image_path = 'admin/campaign_progress';
            $path = 'img_' . uniqid() . '.' . $request->file('image')->extension();

            // Store the image in the 'public' disk
            $request->file('image')->storeAs($image_path, $path, 'public');

            // Generating the full URL for the image
            $image = 'https://jariah.berqat.com/storage/admin/campaign_progress/' . $path;
        } else {
            // If no image is uploaded, we'll set the path to null or you can set a default image path
            $path = null;
        }

        CampaignProgress::create([
            'description' => $request->description,
            'image' => $image,  // If you want to store the full URL, change this to 'image' => $image,
            'campaign_id' => $campaign_id
        ]);


        return back()->with('addprogress','Progress has been created successfully.');
    }

    public function destroy($progress_id)
    {
        $progress = CampaignProgress::where('_id', $progress_id);
        $progress->delete();

        return back()->with('deleteprogress','Progress has been deleted successfully.');
    }

    public function assign($campaign_id, Request $request)
    {     
        $campaign = Campaign::where('_id', $campaign_id)->first();
        
        $campaign->third_party_id = $request->third_party_id;
        $campaign->save();

        return back()->with('updateparty','Third party has been assigned successfully.');
    }

    public function update(Request $request,$id)
    {   
        $get_campaign_progress = CampaignProgress::where('_id', $id)->first();

        if ($request->hasFile('image')) 
        {
            $image_path = 'admin/campaign_progress';

            $path = 'img_' . uniqid() . '.' . $request->file('image')->extension();

            $request->file('image')->storeAs($image_path, $path, 'public');

            $image = 'https://jariah.berqat.com/storage/admin/campaign_progress/' . $path;

            CampaignProgress::where('_id', $id)->update([
                'image' => $image,
                'description' => $request->description
            ]);

        } else {
            CampaignProgress::where('_id', $id)->update([
                'image' => $get_campaign_progress->image,
                'description' => $request->description
            ]);
        }

        return back()->with('successupdate', 'Progress has been updated successfully.');

    }
}
