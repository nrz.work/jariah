<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $user = User::where('email', Auth::user()->email)->first();

        return view('admin.account.view', compact('user'));
    }

    public function edit(Request $request){    
        $user = User::where('_id', Auth::user()->_id)->first();

        if($request->hasFile('ic_copy'))
        {
            $image_path = 'public/admin/ic_copy';
            $path = 'img_' . uniqid().'.'.$request->file('ic_copy')->extension();
            $request->file('ic_copy')->storeAs($image_path, $path);
            $image = 'https://jariah.berqat.com/storage/admin/ic_copy/' . $path;
        }

        $user->name = $request->name;
        $user->phone_number = $request->phone_number;
        $user->password = \Hash::make($request->password);
        if($request->hasFile('ic_copy'))
        {
            $user->ic_copy = $image;
        }
        $user->save();

        return back()->with('updateuser','Account has been updated successfully.');
    }
}
