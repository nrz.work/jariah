<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class SitemapController extends Controller
{
    public function index()
    {
        $sitemap = Sitemap::create();

        $allRoutes = [
            'home',
            'activities',
            'personal-account',
            'about',
            'search-campaign',
            // 'list-campaign',

            'donate',
            'donate.store',
            'donate.store-guest',
            // 'donate.show',
            'process-loading',
            'thank-you',
            'check-payment',
            'check-payment-guest',
            'payment-failed',

            'campaign.index',
            'campaign.like',
            // 'campaign.filter',
            // 'campaign.show',
            // 'campaign.progress',
            // 'campaign.ngo-profile',

            'dashboard',
            'statistic-by-date',
            'statistic-by-year',
            'organization.index',
            'organization.update',
            // 'organization.edit',
            'account.index',
            'account.edit',
            'campaign.create',
            'campaign.store',
            // 'campaign.update',
            // 'campaign.edit',
            'campaign.index',
            // 'campaign.destroy',
            // 'campaign.statistic',
            // 'campaign-progress.create',
            // 'campaign-progress.store',
            // 'campaign-progress.destroy',
            // 'campaign-progress.assign',
            // 'campaign-progress.update',
            'donation',
            'transaction-list',
            'financial-report',
            'admin',
            'new-admin',
            'store-admin',
            // 'view-user',
            // 'edit-user',
            // 'delete-user',
            'ngo',
            'new-ngo',
            'store-ngo',
            'donor',
        ];

        foreach ($allRoutes as $route) {
            $sitemap->add(Url::create(route($route)));
        }

        return $sitemap->render();
    }
}
