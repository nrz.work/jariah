<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CampaignCategories;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $categories = CampaignCategories::orderby('id', 'desc')->paginate(15);
        
        return view('admin.category.view', ['categories'=> $categories]);
    }
}
