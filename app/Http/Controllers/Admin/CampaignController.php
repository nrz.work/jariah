<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\Company;
use App\Models\CampaignCategories;
use App\Models\CampaignProgress;
use App\Models\Donation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $count = 1;

        if ($user->role == "NGO") {
            # code...
            $company = Company::where('owner_email', $user->email)->first();

            $campaign = Campaign::where('company_id', $company->_id)->orderBy('_id', 'asc')->paginate(15);
            $companies = Company::whereNotIn('owner_email', [$user->email])->get();
            // $companies = Company::where('owner_email', $user->email)->get();

            return view('admin.campaign.view', compact('campaign', 'companies', 'count'));

        }elseif ($user->role == "Admin") {
            # code...
            // $company = Company::where('owner_email', $user->email)->first();

            $campaign = Campaign::orderBy('_id', 'asc')->paginate(15);
            $companies = Company::whereNotIn('owner_email', [$user->email])->get();
            // $companies = Company::all();

            return view('admin.campaign.view', compact('campaign','companies', 'count'));
        }
    }

    public function create()
    {
        $allCampaign = Campaign::all();
        $categories = CampaignCategories::all();
        $company = Company::all();

        return view('admin.campaign.create', ['campaign' => $allCampaign->toJson(), 'categories' => $categories, 'company' => $company]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'campaign_name' => 'required',
            'campaign_category' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'campaign_description' => 'required|string',
            'total_donation' => 'required',
            'image' => 'required|image|dimensions:width=1080,height=1080',
        ]);

        $company = Company::where('owner_email', Auth::user()->email)->first();


        if ($request->hasFile('image')) {
            $image_path = 'admin/campaign';
            $path = 'img_' . uniqid() . '.' . $request->file('image')->extension();

            $request->file('image')->storeAs($image_path, $path, 'public');

            $image = 'https://jariah.berqat.com/storage/admin/campaign/' . $path;
        } else {
            
            $path = null;
        }

        if (Auth::user()->role == 'Admin') {
            # code...
            Campaign::create([
                'company_id' => $request->company_id,
                'campaign_name' => $request->campaign_name,
                'campaign_category' => $request->campaign_category,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'campaign_description' => $request->campaign_description,
                'total_donation' => $request->total_donation,
                'image' => $image,
                'flag_kilat' => $request->flag_kilat,
                'slug' => Str::slug($request->campaign_name),
                'campaign_status' => 'active'
            ]);

        }else{
            Campaign::create([
                'company_id' => $company->_id,
                'campaign_name' => $request->campaign_name,
                'campaign_category' => $request->campaign_category,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'campaign_description' => $request->campaign_description,
                'total_donation' => $request->total_donation,
                'image' => $image,
                'flag_kilat' => $request->flag_kilat,
                'slug' => Str::slug($request->campaign_name),
                'campaign_status' => 'active'
            ]);   
        }

        return redirect('view-campaign')->with('addcampaign', 'New campaign has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        return view('campaign_view', ['campaign' => $campaign->toJson()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update($campaign_id)
    {
        $campaign = Campaign::where('_id', $campaign_id)->first();
        $category = CampaignCategories::where('_id', $campaign->campaign_category)->first();
        $categories = CampaignCategories::all();

        return view('admin.campaign.update', ['campaign' => $campaign, 'category' => $category, 'categories' => $categories]);
    }

    public function edit($campaign_id, Request $request)
    {
        $campaign = Campaign::where('_id', $campaign_id)->first();

        if ($request->hasFile('image')) {
            $image_path = 'admin/campaign';
            
            $path = 'img_' . uniqid() . '.' . $request->file('image')->extension();

            $request->file('image')->storeAs($image_path, $path, 'public');

            $image = 'https://jariah.berqat.com/storage/admin/campaign/' . $path;
        } else {

            $path = null;
        }

        $campaign->campaign_name = $request->campaign_name;
        $campaign->campaign_category = $request->campaign_category;
        $campaign->start_date = $request->start_date;
        $campaign->end_date = $request->end_date;
        $campaign->campaign_description = $request->campaign_description;
        $campaign->total_donation = $request->total_donation;
        $campaign->flag_kilat = $request->flag_kilat;

        if ($request->hasFile('image')) {
            $campaign->image = $image;
        }

        $campaign->save();

        return back()->with('updatecampaign', 'Campaign has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy($campaign_id)
    {
        $campaign = Campaign::where('_id', $campaign_id);
        $donation = Donation::where('campaign_id', $campaign_id);
        $campaign_progress = CampaignProgress::where('campaign_id', $campaign_id);
        
        $campaign_progress->delete();
        $donation->delete();
        $campaign->delete();

        return redirect('view-campaign')->with('deletecampaign', 'Campaign has been deleted successfully.');
    }

    public function statistic($campaign_id)
    {
        $campaign = Campaign::where('_id', $campaign_id)->first();

        return view('admin.campaign.statistic', ['campaign' => $campaign]);
    }
}
