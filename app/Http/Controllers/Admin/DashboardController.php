<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Company;
use Auth;
use App\Models\Donation;
use Illuminate\Support\Carbon;
use MongoDB\BSON\UTCDateTime;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $startDate = new Carbon(date('Y-m-01'));
        $endDate = new Carbon(date('Y-m-31'));

        $total_donation = Donation::where('billplz_status', 'paid')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->count();
        $count_campaigns = Campaign::orderBy('id', 'desc')->count();

        $total_donation_sum = Donation::where('billplz_status', 'paid')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->sum('amount');

        $formatted_total_donation = $total_donation_sum * 0.06;

        $net_total = number_format($total_donation_sum - $formatted_total_donation, 2);

        return view('admin.dashboard', compact('count_campaigns', 'total_donation', 'net_total'));
    }

    public function statistic_by_date(Request $request)
    {
        $user = Auth::user();

        $company = Company::where('owner_email', $user->email)->first();

        $fromDate = $request->has('from_date') ? $request->from_date : date('Y-01-01');
        $toDate = $request->has('to_date') ? $request->to_date : date('Y-12-31');

        $getMonth = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

        if ($user->role == 'Admin') {
            $donation_count = Donation::where('billplz_status', 'paid')
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$fromDate} 00:00:00") * 1000), new UTCDateTime(strtotime("{$toDate} 23:59:59") * 1000)])
                ->count();

            $total_donation_sum = Donation::where('billplz_status', 'paid')
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$fromDate} 00:00:00") * 1000), new UTCDateTime(strtotime("{$toDate} 23:59:59") * 1000)])
                ->sum('amount');

            // Calculate the formatted value (6% of the total sum)
            $formatted_total_donation = number_format($total_donation_sum * 0.06, 2);


            $total_amount_campaign_donation = Donation::where('billplz_status', 'paid')
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$fromDate} 00:00:00") * 1000), new UTCDateTime(strtotime("{$toDate} 23:59:59") * 1000)])
                ->get();
        } else {

            $campaignIds = Campaign::where('company_id', $company->_id)->pluck('_id');

            $donation_count = Donation::where('billplz_status', 'paid')
                ->whereIn('campaign_id', $campaignIds)
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$fromDate} 00:00:00") * 1000), new UTCDateTime(strtotime("{$toDate} 23:59:59") * 1000)])
                ->count();

            $total_donation_sum = Donation::where('billplz_status', 'paid')
                ->whereIn('campaign_id', $campaignIds)
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$fromDate} 00:00:00") * 1000), new UTCDateTime(strtotime("{$toDate} 23:59:59") * 1000)])
                ->sum('amount');

            // Calculate the formatted value (6% of the total sum)
            $formatted_total_donation = number_format($total_donation_sum * 0.06, 2);

            $total_amount_campaign_donation = Donation::where('billplz_status', 'paid')
                ->whereIn('campaign_id', $campaignIds)
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$fromDate} 00:00:00") * 1000), new UTCDateTime(strtotime("{$toDate} 23:59:59") * 1000)])
                ->get();
        }

        return view('admin.statistic_date', compact('donation_count', 'total_amount_campaign_donation', 'formatted_total_donation', 'total_donation_sum', 'getMonth', 'company', 'user'));
    }

    public function statistic_by_year(Request $request)
    {
        $user = Auth::user();

        $company = Company::where('owner_email', $user->email)->first();

        $Year = $request->has('year') ? $request->year : date('Y');
        $getYear = ['2022', '2023', '2024', '2025', '2026'];

        if ($user->role == 'Admin') {
            $donation_count = Donation::where('billplz_status', 'paid')
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$Year}-01-01 00:00:00") * 1000), new UTCDateTime(strtotime("{$Year}-12-31 23:59:59") * 1000)])
                ->count();

            $total_donation_sum = Donation::where('billplz_status', 'paid')
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$Year}-01-01 00:00:00") * 1000), new UTCDateTime(strtotime("{$Year}-12-31 23:59:59") * 1000)])
                ->sum('amount');

            $total_net_donation = number_format($total_donation_sum * 0.06, 2);

            $total_amount_campaign_donation = Donation::where('billplz_status', 'paid')
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$Year}-01-01 00:00:00") * 1000), new UTCDateTime(strtotime("{$Year}-12-31 23:59:59") * 1000)])
                ->get();
        } else {

            $campaignIds = Campaign::where('company_id', $company->_id)->pluck('_id');

            $donation_count = Donation::where('billplz_status', 'paid')
                ->whereIn('campaign_id', $campaignIds)
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$Year}-01-01 00:00:00") * 1000), new UTCDateTime(strtotime("{$Year}-12-31 23:59:59") * 1000)])
                ->count();

            $total_donation_sum = Donation::where('billplz_status', 'paid')
                ->whereIn('campaign_id', $campaignIds)
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$Year}-01-01 00:00:00") * 1000), new UTCDateTime(strtotime("{$Year}-12-31 23:59:59") * 1000)])
                ->sum('amount');

            $total_net_donation = number_format($total_donation_sum * 0.06, 2);

            $total_amount_campaign_donation = Donation::where('billplz_status', 'paid')
                ->whereIn('campaign_id', $campaignIds)
                ->whereBetween('created_at', [new UTCDateTime(strtotime("{$Year}-01-01 00:00:00") * 1000), new UTCDateTime(strtotime("{$Year}-12-31 23:59:59") * 1000)])
                ->get();
        }

        return view('admin.statistic_year', compact('donation_count', 'total_amount_campaign_donation', 'total_net_donation', 'total_donation_sum', 'getYear', 'company', 'user'));
    }

    public function donation(Request $request)
    {
        $user = Auth::user();

        $company = Company::where('owner_email', $user->email)->first();

        if ($user->role == 'Admin') {
            # code...
            $campaignIds = Campaign::where('company_id', $company->_id)->pluck('_id');

            if ($request->has('billplz_id')) {

                $donation = Donation::where('billplz_status', 'paid')->where('billplz_id', $request->billplz_id)->get();

                return view('admin.donation', compact('donation'));
            }

            $donation = Donation::where('billplz_status', 'paid')->paginate(10);

            return view('admin.donation', compact('donation'));
        } else {
            $campaignIds = Campaign::where('company_id', $company->_id)->pluck('_id');

            if ($request->has('billplz_id')) {

                $donation = Donation::whereIn('campaign_id', $campaignIds)->where('billplz_status', 'paid')->where('billplz_id', $request->billplz_id)->get();

                return view('admin.donation', compact('donation'));
            }

            $donation = Donation::whereIn('campaign_id', $campaignIds)->where('billplz_status', 'paid')->paginate(10);

            return view('admin.donation', compact('donation'));
        }
    }

    public function transaction_list()
    {
        $user = Auth::user();
        
        if ($user->role == "NGO") {

            $company = Company::where('owner_email', $user->email)->first();
            $campaigns = Campaign::where('company_id', $company->_id)->orderBy('_id', 'asc')->paginate(15);

            return view('admin.transaction_record.transaction_list', compact('campaigns', 'company'));

        }elseif ($user->role == "Admin") {

            $campaigns = Campaign::orderBy('_id', 'asc')->paginate(15);
            $companies = Company::whereNotIn('owner_email', [$user->email])->get();

            return view('admin.transaction_record.transaction_list', compact('campaigns', 'companies'));

        }
    }

    public function financial_report()
    {

        $company = Company::where('owner_email', Auth::user()->email)->first();

        return view('admin.transaction_record.financial_report', compact('company'));
    }
}
