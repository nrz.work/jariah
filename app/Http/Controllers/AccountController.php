<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Campaign;
use App\Models\Company;
use App\Models\Donation;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function personal()
    {
        $donations = Donation::where('email', Auth::user()->email)->get();

        return view('personal', compact('donations'));
    }
}
