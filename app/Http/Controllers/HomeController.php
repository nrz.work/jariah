<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\Donation;
use App\Models\CampaignCategories;
use App\Models\CampaignLikes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\JariahNotification;
use App\Models\CampaignProgress;

class HomeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function main()
    {
        // $user_email = Auth::user()->email;

        $dt = Carbon::now()->toISOString();

        $category = CampaignCategories::all();
        $c1 = CampaignCategories::where('_id', '649e17e234a322d755074a3d')->first();
        $c2 = CampaignCategories::where('_id', '64be594f99ecf9e07044b8f1')->first();
        $c3 = CampaignCategories::where('_id', '64bfad2799ecf9e07044b900')->first();
        $c4 = CampaignCategories::where('_id', '64bfad5899ecf9e07044b903')->first();
        $c5 = CampaignCategories::where('_id', '64e782b6c3cea9efa39691b4')->first();
        $c6 = CampaignCategories::where('_id', '64e782c5c3cea9efa39691b5')->first();
        $c7 = CampaignCategories::where('_id', '64e782d0c3cea9efa39691b6')->first();
        $c8 = CampaignCategories::where('_id', '64e782ddc3cea9efa39691b7')->first();
        $c9 = CampaignCategories::where('_id', '64e782edc3cea9efa39691b8')->first();
        $c10 = CampaignCategories::where('_id', '64e78301c3cea9efa39691b9')->first();
        $c11 = CampaignCategories::where('_id', '64e78310c3cea9efa39691ba')->first();
        $c12 = CampaignCategories::where('_id', '64e78319c3cea9efa39691bb')->first();

        $campaigns = Campaign::where('start_date', '<', $dt)->where('end_date', '>', $dt)->where('campaign_status', 'active')->latest()->get();

        $campaigns_flag_kilat = Campaign::where('start_date', '<', $dt)->where('end_date', '>', $dt)->where('campaign_status', 'active')->where('flag_kilat', '1')->orderBy('created_at', 'desc')->limit(50)->get();

        // $likedCampaignIds = CampaignLikes::where('user_email', $user_email)->pluck('campaign_id')->toArray();;

        return view('main', compact('campaigns','category', 'campaigns_flag_kilat', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12',));
    }

    public function campaign()
    {
        return view('campaign');
    }

    public function view_campaign()
    {
        return view('campaign_view');
    }

    public function fund_campaign()
    {
        return view('campaign_fund');
    }

    public function activities()
    {
        $dt = Carbon::now()->toISOString();
        $campaignId = Campaign::where('start_date', '<', $dt)->where('end_date', '>', $dt)->pluck('_id');
        $campaign_progress = CampaignProgress::whereIn('campaign_id', $campaignId)->paginate(7);

        return view('activities', compact('campaign_progress'));
    }

    public function cart()
    {
        return view('cart');
    }

    public function search_campaign(Request $request)
    {
        // dd($request->search);
        $campaign = Campaign::where('campaign_name', 'LIKE', "%{$request->search}%")
            ->Where('campaign_description', 'LIKE', "%{$request->search}%")
            ->get();

        return redirect('list-campaign' . '/' . $request->search);
    }

    public function list_campaign($search)
    {
        $list_campaign = Campaign::where('campaign_name', 'LIKE', "%{$search}%")
            ->orWhere('campaign_description', 'LIKE', "%{$search}%")
            ->get();

        return view('campaign_search', compact('list_campaign'));
    }

    public function about()
    {
        return view('about');
    }
}
