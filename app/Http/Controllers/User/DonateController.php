<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Donation;
use App\Models\Campaign;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Billplz\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\JariahResit;

class DonateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = null;

        if (Auth::check()) {
            # code...
            $user = User::where('_id', Auth::user()->id)->first();
        }

        return view('cart', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $apiKey = env('BILLPLZ_API_KEY');
        // $xSignature = env('BILLPLZ_X_SIGNATURE');
        $apiKey = "af9cff96-df35-4704-b3bb-1974d96e954a";
        $xSignature = "S-rPdlYqS9r02bChSYIOXuqg";
        // $apiKey = env('SANDBOX_BILLPLZ_API_KEY');
        // $xSignature = env('SANDBOX_BILLPLZ_X_SIGNATURE');

        $jsonData = $request->json()->all();

        // // Get the cartData array from the jsonData
        $cartData = $jsonData['cartData'];

        // return response()->json(['message' => $cartData]);

        // // Initialize an array to store the created donations
        $donations = [];

        // Get last invoice
        $lastInvoiceNumber = Donation::getLastInvoiceNumber();
        $nextInvoiceNumber = $lastInvoiceNumber + 1;
        $formattedInvoiceNumber = str_pad($nextInvoiceNumber, 4, '0', STR_PAD_LEFT);

        $billplz = Client::make($apiKey, $xSignature);
        // $billplz->useSandbox();

        $bill = $billplz->bill();

        $response = $bill->create(
            'nkpcc1fe',
            // 'gsvpex7v8', // Sandbox use
            Auth::user()->email,
            Auth::user()->phone_number,
            Auth::user()->name,
            $jsonData['totalAmount'] * 100,
            'http://example.com/webhook/',
            'INV - ' . $formattedInvoiceNumber,
            // 'Sumbangan Derma',
            ['redirect_url' => url('check-payment')]

        );

        // Store bill
        $create_bill = $response->toArray();

        // Loop through each array in cartData and save the data in the Donation model
        foreach ($cartData as $item) {
            $donation = Donation::create([
                'email' => Auth::user()->email,
                'campaign_id' => $item['campaign_id'],
                'payment_method' => $jsonData['payment_method'],
                'amount' => $item['amount'],
                'billplz_id' => $create_bill['id'],
                'billplz_status' => $create_bill['state'],
                'doa' => $jsonData['doaPenderma'],
                'agreement_information' => $request->agreement_information,
                'agreement_name' => $request->agreement_name,
                'invoice_id' => 'INV - ' . $formattedInvoiceNumber
            ]);

            // Add the created donation to the $donations array
            $donations[] = $donation;
        }

        return response()->json(['redirect_url' => $create_bill['url']]);
    }

    public function store_guest(Request $request)
    {
        // $apiKey = env("BILLPLZ_API_KEY");
        // $xSignature = env("BILLPLZ_X_SIGNATURE");
        $apiKey = "af9cff96-df35-4704-b3bb-1974d96e954a";
        $xSignature = "S-rPdlYqS9r02bChSYIOXuqg";
        // $apiKey = env('SANDBOX_BILLPLZ_API_KEY');
        // $xSignature = env('SANDBOX_BILLPLZ_X_SIGNATURE');

        $jsonData = $request->json()->all();
        // return response()->json(['message' => $jsonData['guest_name']]);

        // // Get the cartData array from the jsonData
        $cartData = $jsonData['cartData'];

        // // Initialize an array to store the created donations
        $donations = [];

        // Get last invoice
        $lastInvoiceNumber = Donation::getLastInvoiceNumber();
        $nextInvoiceNumber = $lastInvoiceNumber + 1;
        $formattedInvoiceNumber = str_pad($nextInvoiceNumber, 4, '0', STR_PAD_LEFT);

        // echo $nextInvoiceNumber;

        $billplz = Client::make($apiKey, $xSignature);
        // $billplz->useSandbox();

        $bill = $billplz->bill();

        $response = $bill->create(
            'nkpcc1fe',
            // 'gsvpex7v8', // Sandbox use
            $jsonData['guest_email'],
            $jsonData['guest_phone_number'],
            $jsonData['guest_name'],
            $jsonData['totalAmount'] * 100,
            'http://example.com/webhook/',
            // 'Sumbangan Derma',
            'INV - ' . $formattedInvoiceNumber,
            ['redirect_url' => url('check-payment-guest')]

        );

        // Store bill
        $create_bill = $response->toArray();

        foreach ($cartData as $item) 
        {
            $donation = Donation::create([
                'email' => $jsonData['guest_email'],
                'campaign_id' => $item['campaign_id'],
                'payment_method' => $jsonData['payment_method'],
                'amount' => $item['amount'],
                'billplz_id' => $create_bill['id'],
                'billplz_status' => $create_bill['state'],
                'name' => $jsonData['guest_name'],
                'phone_number' => $jsonData['guest_phone_number'],
                'doa' => $jsonData['doaPenderma'],
                'agreement_information' => $request->agreement_information,
                'agreement_name' => $request->agreement_name,
                'invoice_id' => 'INV - ' . $formattedInvoiceNumber
            ]);

            // Add the created donation to the $donations array
            $donations[] = $donation;
        }

        return response()->json(['redirect_url' => $create_bill['url']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Donation $donation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $campaign = Campaign::where('_id', $id)->get();
        $campaign = Campaign::where('_id', $id)->first();
        return view('campaign_fund', ['campaign' => $campaign]);
    }

    public function check_payment()
    {
        // Init API
        // $apiKey = env('BILLPLZ_API_KEY');
        // $xSignature = env('BILLPLZ_X_SIGNATURE');
        $apiKey = "af9cff96-df35-4704-b3bb-1974d96e954a";
        $xSignature = "S-rPdlYqS9r02bChSYIOXuqg";
        // $apiKey = env('SANDBOX_BILLPLZ_API_KEY');
        // $xSignature = env('SANDBOX_BILLPLZ_X_SIGNATURE');

        $billplz = Client::make($apiKey, $xSignature);
        // $billplz->useSandbox();

        // / Get the value of billplz[paid] from the request
        $paid = request('billplz')['paid'];
        $billplz_id = request('billplz')['id'];

        $bill = $billplz->bill();
        $response = $bill->get($billplz_id);
        $get_billplz = $response->toArray();

        // Check if billplz[paid] is true or false
        if ($paid === 'true') 
        {
            Donation::where('billplz_id', $billplz_id)->update([
                'billplz_status' => $get_billplz['state']
            ]);

            $get_donation = Donation::where('billplz_id', $billplz_id)->get();

            $fullname = Auth::user()->name;
            $email = Auth::user()->email;

            // dd($email);

            $totalDonation = $get_donation->sum('amount');

            $campaign_names = []; // Initialize an array to store campaign names

            foreach ($get_donation as $donation) {
                $campaign = Campaign::where('_id', $donation->campaign_id)->first();

                $campaign_names[] = [
                    'campaign_name' => $campaign->campaign_name,
                    'amount' => $donation->amount,
                    'company' => $campaign->company->company_name
                ];
            }

            // dd($campaign_names);

            $this->receipt_email($fullname, $totalDonation, $campaign_names, $email);

            return redirect('thank-you');
        } else {
            Donation::where('billplz_id', $billplz_id)->update([
                'billplz_status' => $get_billplz['state']
            ]);

            return redirect('payment-failed'); // Tukar ke failed page

        }
    }

    public function check_payment_guest()
    {
        // Init API
        // $apiKey = env('SANDBOX_BILLPLZ_API_KEY');
        // $xSignature = env('SANDBOX_BILLPLZ_X_SIGNATURE');
        // $apiKey = env('BILLPLZ_API_KEY');
        // $xSignature = env('BILLPLZ_X_SIGNATURE');
        $apiKey = "af9cff96-df35-4704-b3bb-1974d96e954a";
        $xSignature = "S-rPdlYqS9r02bChSYIOXuqg";

        $billplz = Client::make($apiKey, $xSignature);
        // $billplz->useSandbox();

        // / Get the value of billplz[paid] from the request
        $paid = request('billplz')['paid'];
        $billplz_id = request('billplz')['id'];

        $bill = $billplz->bill();
        $response = $bill->get($billplz_id);
        $get_billplz = $response->toArray();

        // Check if billplz[paid] is true or false
        if ($paid === 'true') 
        {
            Donation::where('billplz_id', $billplz_id)->update([
                'billplz_status' => $get_billplz['state']
            ]);

            $get_donation = Donation::where('billplz_id', $billplz_id)->get();

            $fullname = $get_donation->first()->name;

            $total_donation = $get_donation->sum('amount');

            $campaign_names = []; // Initialize an array to store campaign names

            foreach ($get_donation as $donation) {
                $campaign = Campaign::where('_id', $donation->campaign_id)->first();

                $campaign_names[] = [
                    'campaign_name' => $campaign->campaign_name,
                    'amount' => $donation->amount,
                    'company' => $campaign->company->company_name
                ];
            }            

            $this->receipt_email($fullname, $total_donation, $campaign_names, $get_donation);

            return redirect('thank-you');

        } else {
            Donation::where('billplz_id', $billplz_id)->update([
                'billplz_status' => $get_billplz['state']
            ]);

            return redirect('payment-failed'); // Tukar ke failed page

        }
    }

    public function receipt_email($fullname, $totalDonation, $campaign_names, $email)
    {
        // return view('receipt');

        Mail::to($email)->send(new JariahResit($fullname, $totalDonation, $campaign_names));

    }

    public function thankyou()
    {
        return view('thankyou');
    }

    public function failed()
    {
        return view('payment_failed');
    }

        // public function loading()
    // {
    //     return view('please_wait');
    // }
}
