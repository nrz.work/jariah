<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DateTime;
use MongoDB\BSON\ObjectId;
use App\Models\Campaign;
use App\Models\CampaignProgress;
use App\Models\CampaignCategories;
use App\Models\CampaignLikes;
use App\Models\Company;
use App\Models\Donation;
use App\Models\User;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dt = Carbon::now()->toISOString();

        // Categories
        $category = CampaignCategories::all();

        // $campaigns = Campaign::where('start_date', '<', $dt)->where('end_date', '>', $dt)->get();
        $allCampaign = Campaign::where('start_date', '<', $dt)->where('campaign_status', 'active')->where('end_date', '>', $dt)->paginate(7);

        $donations = Donation::all();

        // Get the campaign IDs from the $campaigns collection
        $campaignIds = $allCampaign->pluck('_id');

        // Filter the $donations collection based on the campaign IDs
        $donationsForCampaigns = $donations->whereIn('campaign_id', $campaignIds);

        // Count the number of donations for each campaign
        // $donationCountPerCampaign = $donationsForCampaigns->groupBy('campaign_id')
        //     ->map(function ($donations) {
        //         return $donations->count();
        //     });

        return view('campaign', compact('category', 'allCampaign'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Campaign title
        $campaign = Campaign::where('slug', $id)->first();

        // Company Details
        $get_company_id_campaign = Campaign::where('_id', $campaign['_id'])->pluck('company_id');

        // dd($get_company_id_campaign);
        
        $objectId = $get_company_id_campaign[0];
        $companyId = new ObjectId($objectId);

        $company = Company::where('_id', $companyId)->first();

        // Total Donation
        $get_total_donation = Donation::where('billplz_status', 'paid')->where('campaign_id', $campaign['_id'])->get();
        $totalDonationAmount = 0;

        foreach ($get_total_donation as $donation) {
            $totalDonationAmount += $donation->amount;
        }

        // List Donation
        $donations = Donation::where('billplz_status', 'paid')->where('campaign_id', $campaign['_id'])->get();

        $users = User::all();

        return view('campaign_view', compact('campaign', 'company', 'totalDonationAmount', 'donations', 'users'));
    }

    public function campaign_filter($filter)
    {
        $dt = Carbon::now()->toISOString();

        // Categories
        $category = CampaignCategories::all();

        $campaigns = Campaign::where('start_date', '<', $dt)->where('end_date', '>', $dt)->where('campaign_status', 'active')->where('campaign_category', $filter)->get();

        // dd($campaigns);

        $allCampaign = Campaign::where('start_date', '<', $dt)->where('end_date', '>', $dt)->where('campaign_status', 'active')->where('campaign_category', $filter)->paginate(7);

        $donations = Donation::all();

        // Get the campaign IDs from the $campaigns collection
        $campaignIds = $campaigns->pluck('_id');

        // Filter the $donations collection based on the campaign IDs
        $donationsForCampaigns = $donations->whereIn('campaign_id', $campaignIds);

        // Count the number of donations for each campaign
        // $donationCountPerCampaign = $donationsForCampaigns->groupBy('campaign_id')
        //     ->map(function ($donations) {
        //         return $donations->count();
        //     });

        return view('campaign', compact('category', 'campaigns', 'allCampaign'));

        // dd($campaigns);
    }

    public function campaign_likes(Request $request)
    {

        $get_campaign = Campaign::where('_id', $request->campaign_id)->first();

        CampaignLikes::create([
            'campaign_id' => $request->campaign_id,
            'user_email' => Auth::user()->email,
        ]);

        Campaign::where('_id', $request->campaign_id,)->update([
            'flag_kilat' => $get_campaign->flag_kilat + 1
        ]);
    }

    public function progress($campaign_id)
    {
        $campaign = Campaign::where('_id', $campaign_id)->first();
        $company = Company::where('_id', $campaign->company_id)->first();
        $progress = CampaignProgress::where('campaign_id', $campaign_id)->get();

        return view('campaign_progress', compact('campaign', 'company', 'progress'));
    }

    public function ngo_profile($company_id)
    {
        $active_campaign = Carbon::now()->toISOString();

        $company = Company::where('_id', $company_id)->first();
        $active = Campaign::where('company_id', $company_id)->where('start_date', '<', $active_campaign)->where('end_date', '>', $active_campaign)->get();
        $expired = Campaign::where('company_id', $company_id)->where('end_date', '<', $active_campaign)->get();

        return view('ngo_profile', compact('company', 'active', 'expired'));
    }
}
