<ul class="navbar-nav bg-gradient-light sidebar sidebar-light accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <img src="{{ asset('backend/img/icon.png') }}" alt="icon" width="60rem">
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="/dashboard">
            <i class="fas fa-fw fa-tachometer-alt text-dark"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-chart-bar text-dark"></i>
            <span>Statistic</span>
        </a>
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Statistic</h6>
                <a class="collapse-item" href="/statistic-by-date">By Date</a>
                <a class="collapse-item" href="/statistic-by-year">By Year</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="/organization">
            <i class="fas fa-briefcase text-dark"></i>
            <span>Organization</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="/account">
            <i class="fas fa-id-card text-dark"></i>
            <span>Account</span>
        </a>
    </li>

    {{-- @if(Auth::user()->role == 'Admin')
        <li class="nav-item">
            <a class="nav-link" href="/view-category">
                <i class="fas fa-filter"></i>
                <span>Category</span>
            </a>
        </li>
    @else
        
    @endif --}}

    <li class="nav-item">
        <a class="nav-link" href="/view-campaign">
            <i class="fas fa-bullhorn text-dark"></i>
            <span>Campaign</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="/donation">
            <i class="fas fa-list-alt text-dark"></i>
            <span>Donation</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="/transaction-list">
            <i class="fas fa-file-invoice-dollar text-dark pr-1"></i>
            <span>Transaction Record</span>
        </a>
    </li>

    @if(Auth::user()->role == 'Admin')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree"
                aria-expanded="true" aria-controls="collapseThree">
                <i class="fas fa-users text-dark"></i>
                <span>User</span>
            </a>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">User</h6>
                    <a class="collapse-item" href="/view-admin">Admin</a>
                    <a class="collapse-item" href="/ngo">NGO</a>
                    <a class="collapse-item" href="/donor">Donor</a>
                </div>
            </div>
        </li>
    @else
        
    @endif
    
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>