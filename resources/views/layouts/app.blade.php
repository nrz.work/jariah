<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="description" content="Platform eJariah daripada Yayasan Digital Malaysia ini berpegang teguh dalam meningkatkan ummah melalui penyediaan perkhidmatan Islam jariah yang terbaik dan efisien kepada umat manusia.">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('og:image')

    <title>@yield('title') | Yayasan Digital Malaysia</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('backend/img/icon.png') }}" />

    <link
        href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,700,700i,900,900i%7CSource+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/styles/bootstrap.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css?v=1"
        integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous" />
    <link rel="manifest" href="{{ asset('frontend/_manifest.json') }}" data-pwa-version="set_in_manifest_and_pwa_js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .full-height {
            height: 100vh;
            /* Set the height to the full viewport height */
            overflow-y: auto;
            /* Add vertical scroll if necessary */
        }
    </style>

    @yield('styles')

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-62XK5LD6E1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-62XK5LD6E1');
    </script>

</head>

<body class="theme-light" data-highlight="highlight-red">
    <div class="row justify-content-center">
        <div class="mobile-view full-height" style="width: 100%; max-width: 500px;">
            <div class="card-body" style="padding-left: 0px; padding-right: 0px;">

                <div id="preloader">
                    <div class="spinner-border color-highlight" role="status"></div>
                </div>

                <div id="page" data-swup="0" class="device-is-ios">
                    <nav class="navbar navbar-light bg-white pb-2 px-2">
                        <div class="d-flex align-items-start pt-1">
                            <button class="navbar-toggler border-0 ps-3" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <img src="{{ asset('backend/img/logo.png') }}" alt="logo" width="100"
                                    class="header-logo image-fluid d-inline-block align-text-top" style="padding-top: 0.4rem">
                            </a>
                        </div>

                        {{-- <div class="d-flex align-items-end"> --}}
                        <div class="d-flex align-items-center">
                            <div class="px-3">
                                @auth
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"
                                        class="d-flex align-items-center"><i class="fa-solid fa-right-from-bracket"></i>
                                        <i style="font-size: 25px;" class="fas fa-sign-out-alt fa-lg mr-2"></i>
                                        &nbsp;&nbsp;
                                        <span>Log Keluar</span>
                                    </a>
                                @else
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                        class="d-flex align-items-center">
                                        {{-- <i style="font-size: 30px;" class="fas fa-user-circle fa-lg mr-2"></i> --}}
                                        <svg width="30" height="30" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <rect width="40" height="40" rx="20" fill="#EFEFFD"/>
                                            <path d="M28 29C28 27.6044 28 26.9067 27.8278 26.3389C27.44 25.0605 26.4395 24.06 25.1611 23.6722C24.5933 23.5 23.8956 23.5 22.5 23.5H17.5C16.1044 23.5 15.4067 23.5 14.8389 23.6722C13.5605 24.06 12.56 25.0605 12.1722 26.3389C12 26.9067 12 27.6044 12 29M24.5 15.5C24.5 17.9853 22.4853 20 20 20C17.5147 20 15.5 17.9853 15.5 15.5C15.5 13.0147 17.5147 11 20 11C22.4853 11 24.5 13.0147 24.5 15.5Z" stroke="#4E4DE7" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                        
                                            
                                        &nbsp;&nbsp;
                                        <span>Log Masuk / Daftar</span>
                                    </a>
                                @endauth

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </div>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ps-3">
                                @auth
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('dashboard') }}">
                                            <div class="row m-0">
                                                <div class="col-12">
                                                    <span class="text-primary ps-2">Dashboard</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>

                                    <hr class="my-1 me-3">

                                @else
                                @endauth

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/') }}">
                                        <div class="row m-0">
                                            <div class="col-12">
                                                <span class="text-primary ps-2">Utama</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('campaign') }}">
                                        <div class="row m-0">
                                            <div class="col-12">
                                                <span class="text-primary ps-2">Kempen</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('activities') }}">
                                        <div class="row m-0">
                                            <div class="col-12">
                                                <span class="text-primary ps-2">Aktiviti</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('donate') }}">
                                        <div class="row m-0">
                                            <div class="col-12">
                                                <span class="text-primary ps-2">Bakul</span>
                                            </div>

                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('personal-account') }}">
                                        <div class="row m-0">
                                            <div class="col-12">
                                                <span class="text-primary ps-2">Akaun</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <hr class="my-1 me-3">

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('tentang-kami') }}">
                                        <div class="row m-0">
                                            <div class="col-12">
                                                <span class="text-primary ps-2">Tentang Kami</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <!-- Add more nav items as needed -->
                            </ul>
                        </div>
                    </nav>

                    {{-- Search --}}
                    <div class="row mb-3">
                        <form id="search-form">
                            <div class="px-4">
                                <div class="search-box search-header background-changer bg-theme">
                                    <i class="fa fa-search"></i>
                                    <input type="text" class="rounded-s" name="search" id="search"
                                        placeholder="Cari Kempen">
                                    {{-- <a href="#" class="clear-search disabled mt-0"><i class="fa fa-times color-red-dark"></i></a> --}}
                                </div>
                            </div>
                        </form>
                    </div>
                    {{-- End Search --}}

                    @yield('content')

                    <div class="row">
                        <div id="footer-bar" class="footer-bar-5">

                            {{-- Utama --}}
                            <a href="{{ url('/') }}" class="color-blue-dark py-2">
                                <svg width="27" height="25" viewBox="0 0 27 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.125 8.76245L13.5 1.19995L25.875 8.76245" stroke="#4E4DE7" stroke-width="1.6875" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M23.125 13.5748V22.3748C23.125 22.8305 22.7557 23.1998 22.3 23.1998H4.7C4.24437 23.1998 3.875 22.8305 3.875 22.3748V13.5748" stroke="#4E4DE7" stroke-width="1.6875" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M10.75 17.7H16.25" stroke="#4E4DE7" stroke-width="1.6875" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                <span class="font-12 py-2">Utama</span>
                            </a>

                            {{-- Kempen --}}
                            <a href="{{ url('campaign') }}" class="color-blue-dark py-2">
                                <svg width="31" height="25" viewBox="0 0 31 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21.25 4.27714C21.25 5.1417 20.9086 5.97207 20.2988 6.58628C18.8952 8.00073 17.5341 9.47557 16.078 10.8387C15.7444 11.1466 15.2149 11.1354 14.8955 10.8135L10.7009 6.58628C9.43303 5.30855 9.43303 3.24573 10.7009 1.968C11.9813 0.677698 14.0671 0.677698 15.3473 1.968L15.4999 2.12164L15.6522 1.96808C16.2662 1.34911 17.1022 1 17.9755 1C18.8489 1 19.685 1.34908 20.2988 1.968C20.9086 2.5823 21.25 3.41263 21.25 4.27714Z" stroke="#4E4DE7" stroke-width="1.6875" stroke-linejoin="round"/>
                                    <path d="M24.125 24L29.6224 18.5026C29.7842 18.3408 29.875 18.1215 29.875 17.8928V10.3438C29.875 9.15288 28.9096 8.1875 27.7188 8.1875C26.5279 8.1875 25.5625 9.15288 25.5625 10.3438V16.8125" stroke="#4E4DE7" stroke-width="1.6875" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M24.125 18.25L25.3585 17.0165C25.4892 16.8858 25.5625 16.7087 25.5625 16.5242C25.5625 16.2604 25.4134 16.0193 25.1775 15.9013L24.5409 15.583C23.4341 15.0296 22.0973 15.2465 21.2223 16.1215L19.9358 17.4079C19.3966 17.9471 19.0938 18.6784 19.0938 19.4409V24" stroke="#4E4DE7" stroke-width="1.6875" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M6.875 24L1.37763 18.5026C1.21586 18.3408 1.125 18.1215 1.125 17.8928V10.3438C1.125 9.15288 2.09038 8.1875 3.28125 8.1875C4.47212 8.1875 5.4375 9.15288 5.4375 10.3438V16.8125" stroke="#4E4DE7" stroke-width="1.6875" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M6.875 18.25L5.64144 17.0165C5.51086 16.8858 5.4375 16.7087 5.4375 16.5242C5.4375 16.2604 5.58651 16.0193 5.8224 15.9013L6.45909 15.583C7.56592 15.0296 8.90272 15.2465 9.77776 16.1215L11.0642 17.4079C11.6034 17.9471 11.9062 18.6784 11.9062 19.4409V24" stroke="#4E4DE7" stroke-width="1.6875" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                <span class="font-12 py-2">Kempen</span>
                            </a>

                            {{-- Aktiviti --}}
                            <a href="{{ url('activities') }}" class="color-blue-dark py-2">
                                <svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M22.7491 2.89045H18.3698V0.103638H16.7774V2.89045H7.2226V0.103638H5.63014V2.89045H1.25087C0.934218 2.89082 0.630646 3.01677 0.406742 3.24067C0.182838 3.46458 0.0568873 3.76815 0.0565186 4.0848V20.8057C0.0568873 21.1223 0.182838 21.4259 0.406742 21.6498C0.630646 21.8737 0.934218 21.9996 1.25087 22H22.7491C23.0658 21.9996 23.3693 21.8737 23.5932 21.6498C23.8171 21.4259 23.9431 21.1223 23.9435 20.8057V4.0848C23.9431 3.76815 23.8171 3.46458 23.5932 3.24067C23.3693 3.01677 23.0658 2.89082 22.7491 2.89045ZM22.351 20.4075H1.64898V4.48291H5.63014V6.47349H7.2226V4.48291H16.7774V6.47349H18.3698V4.48291H22.351V20.4075Z" fill="#4E4DE7"/>
                                    <path d="M11.3729 13.7079L8.66643 11.0015L7.54041 12.1275L11.3729 15.9599L17.624 9.70878L16.498 8.58276L11.3729 13.7079Z" fill="#4E4DE7"/>
                                </svg>
                                <span class="font-12 py-2">Aktiviti</span>
                            </a>

                            {{-- Bakul --}}
                            {{-- <a href="/donate" class="active-nav color-blue-dark py-2"> --}}
                            <a href="{{ url('donate') }}" class="color-blue-dark py-2">
                                <button type="button" class="position-relative">
                                    {{-- <svg width="36" height="35" viewBox="0 0 36 35" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <rect x="0.613281" width="34.7733" height="34.7733" rx="11.8377"
                                            fill="#304C83" />
                                        <g clip-path="url(#clip0_0_1)">
                                            <path
                                                d="M27.5408 10.3975C27.3064 10.1162 27.0129 9.88996 26.6812 9.73485C26.3495 9.57973 25.9878 9.49955 25.6216 9.5H12.1483L12.1133 9.2075C12.0417 8.59951 11.7494 8.03894 11.292 7.63206C10.8346 7.22519 10.2438 7.00028 9.63161 7H9.44661C9.2256 7 9.01364 7.0878 8.85736 7.24408C8.70108 7.40036 8.61328 7.61232 8.61328 7.83333C8.61328 8.05435 8.70108 8.26631 8.85736 8.42259C9.01364 8.57887 9.2256 8.66667 9.44661 8.66667H9.63161C9.83573 8.66669 10.0327 8.74163 10.1853 8.87726C10.3378 9.0129 10.4352 9.19979 10.4591 9.4025L11.6058 19.1525C11.7248 20.1665 12.212 21.1015 12.9749 21.78C13.7377 22.4585 14.7232 22.8334 15.7441 22.8333H24.4466C24.6676 22.8333 24.8796 22.7455 25.0359 22.5893C25.1922 22.433 25.2799 22.221 25.2799 22C25.2799 21.779 25.1922 21.567 25.0359 21.4107C24.8796 21.2545 24.6676 21.1667 24.4466 21.1667H15.7441C15.2283 21.1652 14.7256 21.0043 14.3049 20.7059C13.8842 20.4075 13.566 19.9863 13.3941 19.5H23.3274C24.3044 19.5001 25.2502 19.1569 25.9998 18.5304C26.7494 17.9039 27.255 17.0339 27.4283 16.0725L28.0824 12.4442C28.1477 12.0842 28.1331 11.7142 28.0395 11.3605C27.9459 11.0068 27.7756 10.6781 27.5408 10.3975ZM26.4466 12.1483L25.7916 15.7767C25.6876 16.3542 25.3837 16.8767 24.9332 17.2527C24.4827 17.6287 23.9142 17.8342 23.3274 17.8333H13.1291L12.3449 11.1667H25.6216C25.744 11.1659 25.8651 11.1922 25.9762 11.2435C26.0874 11.2949 26.1858 11.3701 26.2646 11.4638C26.3433 11.5576 26.4005 11.6675 26.4319 11.7858C26.4634 11.9041 26.4684 12.0279 26.4466 12.1483Z"
                                                fill="white" />
                                            <path
                                                d="M14.4467 27C15.3672 27 16.1134 26.2538 16.1134 25.3333C16.1134 24.4128 15.3672 23.6666 14.4467 23.6666C13.5262 23.6666 12.78 24.4128 12.78 25.3333C12.78 26.2538 13.5262 27 14.4467 27Z"
                                                fill="white" />
                                            <path
                                                d="M22.7799 27C23.7004 27 24.4466 26.2538 24.4466 25.3333C24.4466 24.4128 23.7004 23.6666 22.7799 23.6666C21.8595 23.6666 21.1133 24.4128 21.1133 25.3333C21.1133 26.2538 21.8595 27 22.7799 27Z"
                                                fill="white" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0_0_1">
                                                <rect width="20" height="20" fill="white"
                                                    transform="translate(8.61328 7)" />
                                            </clipPath>
                                        </defs>
                                    </svg> --}}
                                    <span
                                    class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger"
                                    id="totalCountBadge"></span>
                                    <svg width="22" height="18" viewBox="0 0 22 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M20.8202 3.73725C20.5624 3.42782 20.2396 3.17895 19.8747 3.00833C19.5099 2.83771 19.1119 2.74951 18.7092 2.75H3.8885L3.85 2.42825C3.77121 1.75947 3.44977 1.14284 2.94662 0.695271C2.44346 0.247705 1.79358 0.000313311 1.12017 0L0.916667 0C0.673552 0 0.440394 0.0965771 0.268485 0.268485C0.0965771 0.440394 0 0.673551 0 0.916667C0 1.15978 0.0965771 1.39294 0.268485 1.56485C0.440394 1.73676 0.673552 1.83333 0.916667 1.83333H1.12017C1.34469 1.83336 1.56139 1.91579 1.72917 2.06499C1.89696 2.21418 2.00415 2.41977 2.03042 2.64275L3.29175 13.3677C3.4227 14.4831 3.9586 15.5116 4.79775 16.258C5.63689 17.0044 6.72087 17.4167 7.84392 17.4167H17.4167C17.6598 17.4167 17.8929 17.3201 18.0648 17.1482C18.2368 16.9763 18.3333 16.7431 18.3333 16.5C18.3333 16.2569 18.2368 16.0237 18.0648 15.8518C17.8929 15.6799 17.6598 15.5833 17.4167 15.5833H7.84392C7.27655 15.5817 6.72357 15.4047 6.26077 15.0765C5.79798 14.7483 5.44804 14.2849 5.25892 13.75H16.1856C17.2602 13.7501 18.3006 13.3725 19.1252 12.6834C19.9497 11.9943 20.5059 11.0373 20.6965 9.97975L21.4161 5.98858C21.4879 5.59259 21.4718 5.18564 21.3688 4.79658C21.2658 4.40752 21.0786 4.04587 20.8202 3.73725ZM19.6167 5.66317L18.8962 9.65433C18.7817 10.2896 18.4474 10.8643 17.9519 11.2779C17.4563 11.6915 16.8311 11.9176 16.1856 11.9167H4.96742L4.10483 4.58333H18.7092C18.8438 4.58253 18.977 4.6114 19.0992 4.6679C19.2215 4.7244 19.3298 4.80713 19.4164 4.91022C19.503 5.01331 19.5659 5.13422 19.6005 5.26436C19.6351 5.39449 19.6406 5.53066 19.6167 5.66317Z" fill="#4E4DE7"/>
                                    </svg>
                                </button>
                                <span class="font-12 py-2">Bakul</span>
                            </a>

                            {{-- Akaun --}}
                            <a href="{{ url('personal-account') }}" class="color-blue-dark py-2">
                                <svg width="19" height="22" viewBox="0 0 19 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4 5.5C4 2.46243 6.46243 0 9.5 0C12.5375 0 15 2.46243 15 5.5C15 8.53752 12.5375 11 9.5 11C6.46243 11 4 8.53751 4 5.5ZM9.5 1.73684C7.42166 1.73684 5.73684 3.42166 5.73684 5.5C5.73684 7.5783 7.42167 9.26316 9.5 9.26316C11.5783 9.26316 13.2632 7.57829 13.2632 5.5C13.2632 3.42167 11.5783 1.73684 9.5 1.73684Z" fill="#4E4DE7"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.49995 12.7368C5.47553 12.7368 2.21308 15.9769 2.21308 19.9737V21.1316C2.21308 21.6112 1.82159 22 1.33866 22C0.855726 22 0.464233 21.6112 0.464233 21.1316V19.9737C0.464233 15.0176 4.50967 11 9.49995 11C14.4902 11 18.5357 15.0176 18.5357 19.9737V21.1316C18.5357 21.6112 18.1442 22 17.6612 22C17.1783 22 16.7868 21.6112 16.7868 21.1316V19.9737C16.7868 15.9769 13.5244 12.7368 9.49995 12.7368Z" fill="#4E4DE7"/>
                                </svg>
                                <span class="font-12 py-2">Akaun</span>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('frontend/scripts/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/scripts/custom.js?v=1') }}"></script>

    {{-- <script>
        function myFunction(x) {
            x.classList.toggle("fa-heart-o");
        }
    </script> --}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#search-form').on('submit', function(event) {
                event.preventDefault();

                var query = $('#search').val();
                window.location = '/search-campaign?search=' + encodeURIComponent(query);
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            var cartData = JSON.parse(localStorage.getItem('cartData'));
            // console.log(cartData);
            var totalCount = 0;
            if (Array.isArray(cartData)) {
                totalCount = cartData.length;
            }
            // console.log(totalCount);

            var totalCountBadge = document.getElementById('totalCountBadge');
            totalCountBadge.textContent = totalCount;
        });
    </script>

    @yield('js')
</body>
