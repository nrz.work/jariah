@extends('layouts.app')

@section('title')
    Tentang Kami
@endsection

@section('styles')
<style>
    /* read tentang penganjur */
    .dynamic-content {
        overflow: hidden;
        max-height: 80px; /* Adjust this value based on your design */
        transition: max-height 0.3s ease-out;
    }

    .dynamic-content.expanded {
        max-height: none;
    }

    .see-more-button {
        color: #4E4DE7;
    }

    .indented {
        text-indent: 2em; /* Adjust the indentation as needed */
    }

    .social-icon i {
        font-size: 18px; /* You can adjust the size as needed */
    }
</style>
@endsection

@section('content')
<div class="row mb-3">

    <div class="col-md-12 px-4">
        <h4 class="color-blue-dark font-500">Tentang Kami</h4>
    </div>

</div>

<div class="row mb-0">
    <picture>
        <source type="image/png" srcset="{{ asset('backend/img/tentang kami.png') }}">
        <img src="{{ asset('backend/img/tentang kami.png') }}" alt="tentang_kami">
    </picture>
</div>

<div class="row border-bottom p-4 mb-0">

    <div class="col-md-12 text-center">
        <p class="text-dark font-14 indented pt-2 mb-2" style="text-align: justify"><span class="color-blue-dark font-500">eJariah</span> adalah sebuah platform digital yang berperanan sebagai wadah untuk masyarakat berbuat kebaikan berlandaskan nilai-nilai Islam. Kami mempunyai visi untuk menjadi platform kebaikan yang terunggul di Malaysia, dengan komitmen kepada pelaksanaan amalan-amalan yang mendalam dan penuh makna, sejajar dengan ajaran Islam yang mulia.</p>
    </div>

</div>

<div class="row border-bottom p-4 mb-0">

    <div class="col-md-12 text-center pb-4">
        <h6 class="color-blue-dark font-500">Misi eJariah</h6>
        <p class="text-dark indented font-14 mb-0" style="text-align: justify">Komited dalam usaha memperkasa ummah dengan penyampaian perkhidmatan jariah yang terbaik dan efisien.</p>
    </div>

    <div class="col-md-12 text-center">
        <h6 class="color-blue-dark font-500">Visi eJariah</h6>
        <p class="text-dark indented font-14 mb-0" style="text-align: justify">Sebagai satu institusi yang menerajui penyaluran amal jariah secara inklusif dan berkesan bagi kesejahteraan ummah.</p>
    </div>


</div>

<div class="row p-4 border-bottom mb-0">
    <div class="col-md-12 text-center px-2">

        <h6 class="color-blue-dark font-500 mb-2">Mutiara Kata</h6>

        <picture>
            <source type="image/jpeg" srcset="{{ asset('frontend/img/about-logo.jpeg') }}">
            <img src="{{ asset('frontend/img/about-logo.jpeg') }}" width="130rem" alt="ydm">
        </picture>

        <p class="text-dark font-14 indented pt-2 mb-2" style="text-align: justify">
            Allah menjanjikan kemurahan rezeki dan kelapangan hidup bagi mereka yang sentiasa bersedekah di jalan-Nya, malah pepatah Arab turut menyatakan bahawa 
            tangan yang memberi itu jauh lebih baik dari tangan yang menerima. 
        </p>
        <p class="text-dark font-14 indented mb-2" style="text-align: justify">  
            Sedekah merupakan terapi jiwa bagi mereka yang ikhlas, redha dan berserah kepada Allah.
        </p>
        <p class="text-dark font-14 indented mb-2" style="text-align: justify">
            Menerusi amalan bersedekah juga, ianya bertujuan memberi keinsafan buat kita untuk meletakkan sepenuh kebergantungan kepada Allah dan bukannya manusia. 
            Sedekah bukan sahaja dalam bentuk wang ringgit, perkongsian ilmu pengetahuan serta segala perbuatan baik yang berlandaskan niat bagi mencari keredhaan Allah, 
            juga adalah sebahagian daripada sedekah.
        </p>
        <p class="text-dark font-14 indented" style="text-align: justify">
            Sedekah mengajar kita agar sentiasa ikhlaskan diri dalam segala amal yang dilakukan dan sesungguhnya Allah pemilik segala rezeki. Sebagai manusia, kita perlu 
            meletakkan sepenuh kepercayaan pada janji Allah.
        </p>


        <p class="text-dark font-14" style="text-align: center">
            ‎الَّذِينَ يُنْفِقُونَ أَمْوَالَهُمْ بِاللَّيْلِ وَالنَّهَارِ سِرًّا وَعَلانِيَةً فَلَهُمْ
            <br>
            ‎ أَجْرُهُمْ عِنْدَ رَبِّهِمْ وَلا خَوْفٌ عَلَيْهِمْ وَلا هُمْ يَحْزَنُونَ
            <br>
        </p>
        <p class="text-dark font-14 indented mb-0" style="text-align: justify">
            “Orang-orang yang menginfakkan hartanya di waktu malam dan siang secara sembunyi dan terang-terangan maka mereka mendapat pahala dari Tuhannya. Maka tidak ada ketakutan atas mereka dan tidak ada berduka cita bagi mereka.”
             Surat Al-Baaqarah: 274
        </p>
    </div>
    
</div>

<div class="row border-bottom p-4 mb-0">

        <div class="col-md-12 text-center px-3">
            <h6 class="color-blue-dark font-500 pb-4">Ketua Pegawai Eksekutif / Pengasas</h6>

            <picture>
                <source type="image/jpeg" srcset="{{ asset('backend/img/pengasas.jpeg') }}">
                <img src="{{ asset('backend/img/pengasas.jpeg') }}" width="300rem" alt="ydm">
            </picture>
        </div>
        <div class="col-md-12 text-center px-3">
            <p class="text-dark font-14 mt-4 mb-0">Ameera Nadia Mohd Shafiee</p>
        </div>

</div>

<div class="row p-4">
    <div class="d-flex">
        <div class="w-50 px-4">
            <picture>
                <source type="image/jpeg" srcset="{{ asset('frontend/img/about-logo.jpeg') }}">
                <img src="{{ asset('frontend/img/about-logo.jpeg') }}" width="150rem" alt="ydm">
            </picture>
        </div>
        <div class="w-50">
            <p class="mb-2">No.95B, Aras 2, Jalan Diplomatik, Presint 15, 62050 Wilayah Persekutuan Putrajaya</p>
            <p class="mb-2">+603-8861 6792</p>
            <p class="mb-2">ejariah.my@gmail.com</p>
            <p>
                <a href="https://www.facebook.com/ejariahmy" class="pe-1 mt-2">
                    <picture>
                        <source type="image/png" srcset="{{ asset('frontend/img/fb.png') }}">
                        <img src="{{ asset('frontend/img/fb.png') }}" width="25rem" alt="ydm">
                    </picture>
                </a>
                <a href="https://www.instagram.com/ejariahdotmy/" class="pe-1 mt-2">
                    <picture>
                        <source type="image/png" srcset="{{ asset('frontend/img/ig.png') }}">
                        <img src="{{ asset('frontend/img/ig.png') }}" width="25rem" alt="ydm">
                    </picture>
                </a>
                <a href="https://www.tiktok.com/@ejariah.my" class="social-icon">
                    <picture>
                        <source type="image/png" srcset="{{ asset('frontend/img/tiktok.png') }}">
                        <img src="{{ asset('frontend/img/tiktok.png') }}" width="25rem" alt="ydm">
                    </picture>
                </a>                
            </p>
        </div>
    </div>
</div>
   
@endsection
