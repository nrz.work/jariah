@extends('layouts.app')

@section('title')
    Pembayaran Gagal
@endsection

@section('content')
    {{-- Content --}}
    <div class="pt-2 px-4">

        <div class="row mb-0">
            <div class="col-md-12 text-center py-5">
                <h3>Pembayaran tidak berjaya!</h3>
                <i class="fa fa-times-circle-o text-danger py-5" style="font-size:150px" aria-hidden="true"></i>
                <p class="m-0">Harap maaf, proses pembayaran gagal.</p>
                <p class="m-0">Sila cuba sekali lagi atau <a href="">hubungi kami</a> jika terdapat sebarang pertanyaan.</p>
            </div>
        </div>
        
    </div>
    {{-- End Content --}}
@endsection
