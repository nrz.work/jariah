@extends('layouts.app')

@section('title')
    Bakul
@endsection

@section('content')
    <div class="pt-2 px-4 pb-5">
        <div class="row my-0">
            @auth
                {{-- <a class="btn btn-primary bg-blue font-500 rounded-2" href="{{ url('test-payment') }}">Beri Sumbangan</a> --}}
                <div class="col-md-12 pb-2">
                    <h6>Cara Pembayaran</h6>
                </div>

                <div class="row">
                    <div class="col-md-12 border rounded p-2">
                        <div class="form-check d-flex align-items-center">
                            <input class="form-check-input" type="radio" name="btnradio" id="perbankan_online"
                                value="Perbankan Online" checked>
                            <label class="form-check-label" for="perbankan_online">
                                Perbankan Online
                            </label>
                            <div class="ms-auto d-flex gap-2">
                                <img src="{{ asset('/frontend/img/billplz.png') }}" width="45" alt="billplz">
                                <img src="{{ asset('/frontend/img/visa.png') }}" width="30" alt="visa">
                                <img src="{{ asset('/frontend/img/mastercard.png') }}" width="30" alt="mastercard">
                            </div>
                        </div>
                        {{-- <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                            <input type="radio" class="btn-check" name="btnradio" id="perbankan_online" value="Perbankan Online"
                                autocomplete="off" checked>
                            <label class="btn btn-xs btn-outline-blue rounded-s mb-1" for="perbankan_online">Perbankan
                                Online</label>
                        </div> --}}
                    </div>
                </div>

                {{-- Butiran Penyumbang --}}
                {{-- <form action="{{ url('donate/store') }}" method="POST" id="donateForm"> --}}
                <form id="donateForm">
                    @csrf
                    <div class="row mb-0 pt-2">
                        <div class="col-md-12 pb-2">
                            <h6>Butiran Penyumbang</h6>
                        </div>
                        <div class="col-md-12">
                            {{-- <input type="text" class="border-0" value="{{ $user->name ?? '' }}" readonly> --}}
                            <p class="text-dark mb-0">
                                <b>Nama : </b> {{ $user->name ?? '' }} <br>
                                <b>Nombor Telefon : </b> {{ $user->phone_number ?? '' }} <br>
                                <b>Emel : </b> {{ $user->email ?? '' }}
                            </p>

                            <div class="form-group pt-3">
                                <textarea type="text" class="form-control" id="doapenderma"
                                    placeholder="Tulis doa untuk kempen ini atau untuk diri anda. Doa ini akan dipaparkan pada halaman kempen."
                                    name="doa" rows="4" style="font-size: 9pt"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-0 pt-2">
                        <hr>
                        <div class="col-md-12 pb-2">
                            <h6>Maklumat Kempen</h6>
                        </div>

                        <div class="col-md-12">
                            <table class="table" id="bakul">
                                <tbody>

                                </tbody>
                            </table>
                            <p id="empty-cart-message" class="text-center" style="display: none;">Maaf, tiada bakul ditemui,
                                sila ke
                                bahagian<br> kempen untuk memilih jariah anda.</p>
                        </div>
                    </div>

                    <div class="form-check mt-3">
                        <input class="form-check-input" type="checkbox" name="agreement_name" value="Yes">
                        <label class="form-check-label">
                            Jangan paparkan nama saya
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="agreement_information" value="Yes">
                        <label class="form-check-label">
                            Saya bersetuju untuk menerima informasi bantuan sumbangan bagi mereka yang memerlukan.
                        </label>
                    </div>

                    <hr>

                    <div class="row my-0">
                        <div class="col-8 pb-2">
                            <h6>Jumlah Jariah</h6>
                        </div>
                        <div class="col-4 text-end pb-2">
                            <h6>RM <span id="amount">0</span></h6>
                        </div>
                    </div>


                    <div class="row mt-3">
                        <button type="submit" class="btn btn-primary bg-blue font-500 rounded-2" id="beriSumbanganBtn"
                            disabled>BERI JARIAH</button>
                        {{-- <a class="btn btn-primary bg-blue font-500 rounded-2" href="https://www.billplz.com/bills/0mltxstp">Beri Sumbangan</a> --}}
                    </div>
                </form>
            @endauth

            @guest
                <div class="col-md-12 pb-2">
                    <h6>Cara Pembayaran</h6>
                </div>

                <div class="row">
                    <div class="col-md-12 border rounded p-2">
                        <div class="form-check d-flex align-items-center">
                            <input class="form-check-input" type="radio" name="btnradio" id="perbankan_online"
                                value="Perbankan Online" checked>
                            <label class="form-check-label" for="perbankan_online">
                                Perbankan Online
                            </label>
                            <div class="ms-auto d-flex gap-2">
                                <img src="{{ asset('/frontend/img/billplz.png') }}" width="45" alt="billplz">
                                <img src="{{ asset('/frontend/img/visa.png') }}" width="30" alt="visa">
                                <img src="{{ asset('/frontend/img/mastercard.png') }}" width="30" alt="mastercard">
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Butiran Penyumbang --}}
                <form id="donateFormGuest">
                    @csrf
                    <div class="row mb-0">
                        <div class="col-md-12 pb-2">
                            <h6>Butiran Penyumbang</h6>
                        </div>
                        <div class="col-md-12">
                            {{-- <input type="text" class="border-0" placeholder="Masukkan Nama"> --}}
                            {{-- <p class="text-dark">
                                <b>Nama : </b> {{ $user->name ?? '' }} <br>
                                <b>Nombor Telefon : </b> {{ $user->phone_number ?? '' }} <br>
                                <b>Emel : </b> {{ $user->email ?? '' }}
                            </p> --}}
                            <div class="form-group">
                                {{-- <label for="nama">Nama :</label> --}}
                                <input type="text" class="form-control" id="name" placeholder="Masukkan Nama Penuh"
                                    name="name" required>
                            </div>
                            <div class="form-group py-3">
                                {{-- <label for="nombor_telefon">Nombor Telefon :</label> --}}
                                <input type="text" class="form-control" id="phone_number" oninput="validatePhoneNumber(this)"
                                    placeholder="Masukkan No Telefon" name="phone_number" required>
                            </div>
                            <div class="form-group">
                                {{-- <label for="emel">Emel :</label> --}}
                                <input type="email" class="form-control" id="email" placeholder="Masukkan Emel"
                                    name="email" required>
                            </div>
                            <div class="form-group pt-3">
                                <textarea type="text" class="form-control" id="doapenderma"
                                    placeholder="Tulis doa untuk kempen ini atau untuk diri anda. Doa ini akan dipaparkan pada halaman kempen."
                                    name="doa" rows="4" style="font-size: 9pt"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-0 pt-4">
                        <hr>
                        <div class="col-md-12 pb-2">
                            <h6>Maklumat Kempen</h6>
                        </div>

                        <div class="col-md-12">
                            <table class="table" id="bakul">
                                <tbody>

                                </tbody>
                            </table>
                            <p id="empty-cart-message" class="text-center" style="display: none;">Maaf, tiada bakul ditemui,
                                sila ke
                                bahagian<br> kempen untuk memilih jariah anda.</p>
                        </div>
                    </div>

                    <div class="form-check mt-3">
                        <input class="form-check-input" type="checkbox" name="agreement_name" value="Yes">
                        <label class="form-check-label">
                            Jangan paparkan nama saya
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="agreement_information" value="Yes">
                        <label class="form-check-label">
                            Saya bersetuju untuk menerima informasi bantuan sumbangan bagi mereka yang memerlukan.
                        </label>
                    </div>

                    <hr>

                    <div class="row my-0">
                        <div class="col-8 pb-2">
                            <h6>Jumlah Jariah</h6>
                        </div>
                        <div class="col-4 text-end pb-2">
                            <h6>RM <span id="amount">0</span></h6>
                        </div>
                    </div>


                    <div class="row mt-3">
                        <button class="btn btn-primary bg-blue font-500 rounded-2" id="sumbanganGuest" disabled>BERI
                            JARIAH</button>
                        {{-- <a class="btn btn-primary bg-blue font-500 rounded-2" href="https://www.billplz.com/bills/0mltxstp">Beri Sumbangan</a> --}}
                    </div>
                </form>
            @endguest
        </div>
    </div>
@endsection

@section('js')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> --}}
    <script>
        function validatePhoneNumber(input) {
            // Remove any non-numeric characters from the input
            input.value = input.value.replace(/\D/g, '');

            // Limit the input to a maximum of 11 digits
            if (input.value.length > 11) {
                input.value = input.value.slice(0, 11);
            }
        }
    </script>
    <script>
        // function checkListCart() 
        // {
        //     var cartData = JSON.parse(localStorage.getItem('cartData'));
        //     console.log(cartData);

        //     var totalAmount = 0;
        //     var cartIsEmpty = true; // Flag to track if the cart is empty

        //     // Loop through the table rows to extract each amount and calculate the total
        //     $('#bakul tbody tr').each(function() {
        //         var amount = parseFloat($(this).find('td.amount').text());
        //         if (!isNaN(amount)) {
        //             totalAmount += amount;
        //             cartIsEmpty = false; // Set the flag to false if there's at least one item in the cart
        //         }
        //     });

        //     // Show or hide the empty cart message based on the cartIsEmpty flag
        //     if (cartIsEmpty) {
        //         console.log("Sini");
        //         $('#empty-cart-message').show(); // Show the message if the cart is empty
        //         $('#beriSumbanganBtn').prop('disabled', true); // Disable the button when the cart is empty
        //         $('#sumbanganGuest').prop('disabled', true);
        //     } else {
        //         // console.log("Sini");
        //         $('#empty-cart-message').hide(); // Hide the message if the cart is not empty
        //         $('#beriSumbanganBtn').prop('disabled', false); // Enable the button when the cart is not empty
        //         $('#sumbanganGuest').prop('disabled', false);
        //     }
        // }

        /*
        |--------------------------------------------------------------------------
        | Save to database
        |--------------------------------------------------------------------------
        */

        function addToCartAndSave() {
            // Get the CSRF token from the meta tag
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var cartData = [];

            // Loop through the table rows to extract each campaign name and amount
            $('#bakul tbody tr').each(function() {
                var campaign_id = $(this).find('#campaignId').val();
                var campaign_name = $(this).find('.name').text();
                var amountText = $(this).find('#singleAmount').text().replace(' RM', ''); // Remove ' RM' from the text
                var amount = parseFloat(amountText);
                // var amount = $(this).find('#singleAmount').text();

                // console.log(amount);

                cartData.push({
                    campaign_id: campaign_id,
                    campaign_name: campaign_name,
                    amount: amount,
                });

                // console.log(campaign_id);
            });

            // Prepare the data to be sent to the server
            var donationData = {
                cartData: cartData,
                totalAmount: parseFloat($('#amount').text()),
                payment_method: $('input[name="btnradio"]:checked').val(),
                doaPenderma: $("#doapenderma").val()
            };

            // Convert the JavaScript object to a JSON string
            var jsonData = JSON.stringify(donationData);

            // console.log(jsonData);

            // Send the AJAX request to save the donation data
            $.ajax({
                url: '{{ url('donate/store') }}', // New Url
                type: 'POST',
                data: jsonData, // Use the JSON string as the data to be sent
                contentType: 'application/json', // Specify the content type as JSON
                headers: {
                    'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the request headers
                },
                xhrFields: {
                    withCredentials: true,
                },
                success: function(response) {
                    // Handle the success response from the server if needed
                    console.log(response);

                    // Extract the redirect URL from the response
                    var redirectUrl = response.redirect_url;

                    // Perform the redirect using window.location.href
                    window.location.href = redirectUrl;

                    // Remove the table rows to clear the list cart
                    $('#bakul tbody').empty();

                    // Clear the data from local storage
                    localStorage.removeItem('cartData');

                    // checkListCart();
                    // Call the function to display cart data from sessionStorage
                    displayDataFromLocalStorage();
                },
                error: function(xhr) {
                    // Handle errors if needed
                    console.log(xhr.responseText);
                    // Perform any action you want on error
                }
            });
        }

        /*
        |--------------------------------------------------------------------------
        | Save guest to database
        |--------------------------------------------------------------------------
        */

        function addToCartAndSaveGuest() {

            // Get the CSRF token from the meta tag
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var guest_name = $('#name').val();
            var guest_phone_number = $('#phone_number').val();
            var guest_email = $('#email').val();
            var payment_method = $('input[name="btnradio"]:checked').val();
            var cartData = [];

            // Loop through the table rows to extract each campaign name and amount
            $('#bakul tbody tr').each(function() {
                var campaign_id = $(this).find('#campaignId').val();
                var campaign_name = $(this).find('.name').text();
                var amountText = $(this).find('#singleAmount').text().replace(' RM', ''); // Remove ' RM' from the text
                var amount = parseFloat(amountText);

                cartData.push({
                    campaign_id: campaign_id,
                    campaign_name: campaign_name,
                    amount: amount,
                });
            });

            // Prepare the data to be sent to the server
            var donationData = {
                cartData: cartData,
                totalAmount: parseFloat($('#amount').text()),
                payment_method: payment_method,
                guest_name: guest_name,
                guest_phone_number: guest_phone_number,
                guest_email: guest_email,
                doaPenderma: $("#doapenderma").val()
            };

            // Convert the JavaScript object to a JSON string
            var jsonData = JSON.stringify(donationData);

            // console.log(jsonData);

            // Send the AJAX request to save the donation data
            $.ajax({
                url: '{{ url('donate/store-guest') }}', // New url
                type: 'POST',
                data: jsonData, // Use the JSON string as the data to be sent
                contentType: 'application/json', // Specify the content type as JSON
                headers: {
                    'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the request headers
                },
                xhrFields: {
                    withCredentials: true,
                },
                success: function(response) {
                    // Handle the success response from the server if needed
                    console.log(response);

                    // Extract the redirect URL from the response
                    var redirectUrl = response.redirect_url;

                    // Perform the redirect using window.location.href
                    window.location.href = redirectUrl;

                    // Remove the table rows to clear the list cart
                    $('#bakul tbody').empty();

                    // Clear the data from local storage
                    localStorage.removeItem('cartData');

                    // checkListCart();
                    // Call the function to display cart data from sessionStorage
                    displayDataFromLocalStorage();
                },
                error: function(xhr) {
                    // Handle errors if needed
                    console.log(xhr.responseText);
                    // Perform any action you want on error
                }
            });
        }


        /*
        |--------------------------------------------------------------------------
        | Display list campaign
        |--------------------------------------------------------------------------
        */

        // Function to display data from localStorage
        function displayDataFromLocalStorage() {

            var cartData = JSON.parse(localStorage.getItem('cartData'));
            console.log(cartData);

            var tableBody = $('#bakul tbody');
            tableBody.empty();

            var totalAmount = 0; // Declare and initialize totalAmount variable

            if (!Array.isArray(cartData) || cartData.length === 0) {
                // Display empty cart message if cartData is not an array or if it is empty
                $('#empty-cart-message').show();
                $('#beriSumbanganBtn').prop('disabled', true); // Disable the button when the cart is empty
                $('#sumbanganGuest').prop('disabled', true);
            } else {
                // Hide empty cart message if cartData is an array and not empty
                $('#empty-cart-message').hide();
                $('#beriSumbanganBtn').prop('disabled', false); // Enable the button when the cart is not empty
                $('#sumbanganGuest').prop('disabled', false);

                // Loop through the cart items and display them in the table
                cartData.forEach(function(item, index) {
                    var newRow = $('<tr>');

                    newRow.append('<input type="hidden" id="campaignId" value="' + item.campaign_id + '">');
                    newRow.append('<td class="line-height-s name">' + '<span class="color-blue-dark" style="font-size: 12px;">' + item.company_name + '</span>' + '<br><br>' + item.campaign_name + '</td>');
                    newRow.append('<td id="singleAmount">' + '<br> RM' + item.amount + '</td>');
                    newRow.append('<td><br><i class="fas fa-times btn btn-sm btn-danger remove-item" data-index="' + index + '"></i></td>');

                    tableBody.append(newRow);

                    totalAmount += parseFloat(item.amount);


                });

                $('#amount').text(totalAmount.toFixed(2));
            }

        }

        // Function to remove an item from the cart and save the updated cart data in localStorage
        function removeFromCartAndSave(index) {
            var cartData = localStorage.getItem('cartData');
            if (cartData) {
                // Parse the cartData from localStorage
                cartData = JSON.parse(cartData);

                // Remove the item at the specified index from the cartData array
                if (index >= 0 && index < cartData.length) {
                    cartData.splice(index, 1); // Remove one element at the specified index
                }

                // Save the updated cartData back to localStorage
                localStorage.setItem('cartData', JSON.stringify(cartData));

                // Display the updated cart data in the table
                displayDataFromLocalStorage();

                // Recalculate the total amount and update the button state
                // checkListCart();
            }
        }


        $(document).on('click', '.remove-item', function() {
            // Get the parent table row and remove it

            var index = $(this).data('index');
            removeFromCartAndSave(index);
            // $(this).closest('tr').remove();
            location.reload();

            // checkListCart();
            // Call the function to display cart data from sessionStorage
            displayDataFromLocalStorage();
        });

        $(document).ready(function() {
            $('#donateForm').submit(function(event) {
                event.preventDefault();

                // Your logic for adding to cart and saving data
                addToCartAndSave();
            });

            $('#donateFormGuest').submit(function(event) {
                event.preventDefault();

                addToCartAndSaveGuest();
            });

            // Call the function to display cart data from sessionStorage
            displayDataFromLocalStorage();

            // Calculate total amount and update the button status
            // checkListCart();
        });
    </script>
@endsection
