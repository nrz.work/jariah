@extends('layouts.app')

@section('title')
    Aktiviti
@endsection

@section('styles')
    <style>
        .container {
            display: flex;
            align-items: center;
        }

        .text {
            padding-right: 10px;
            /* Add spacing between text and HR */
        }

        .divider {
            flex-grow: 1;
            margin: 0;
            border-color: darkgray;
            /* Change to the desired color (e.g., red) */
            border-width: 1px;
            /* Set the border width */
            border-style: solid;
            /* Set the border style */
        }
    </style>
@endsection

@section('content')
    <div class="row pb-5">
        @auth
            @if ($campaign_progress->isEmpty())
                <div class="container d-flex justify-content-center align-items-center" style="height: 70vh;">
                    <p class="text-center" style="word-wrap: break-word; padding: 0 20px;">
                        Maaf, Tiada progres kempen terbaru buat masa ini.
                    </p>
                </div>
            @else
                <div class="col-md-12 px-4">
                    <h4 class="color-blue-dark font-500">Aktiviti</h4>
                </div>
                <div class="col-md-12 px-3">
                    @foreach ($campaign_progress as $item)
                        <div class="container py-2">
                            <div class="text">
                                {{ $item->created_at->format('d M Y') }}
                            </div>
                            <hr class="divider">
                        </div>
                        <div class="card">
                            <a href="{{ url('campaign/show') }}/{{ $item->campaign->slug }}">
                                <div class="card-body">
                                    <span class="color-blue-dark d-inline-block">
                                        <img class="pe-1" width="22rem" src="{{ asset('backend/img/ydm.png') }}"
                                            alt="logo">
                                        Yayasan Digital Malaysia
                                    </span>
                                    <h6 class="card-text font-14 font-500 pt-2">{{ $item->campaign->campaign_name }}</h6>
                                </div>
                                <img width="100%" src="{{ $item->campaign->image }}" alt="kempen">
                                <div class="card-footer d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center mt-4">
                                        <p class="two-line-truncate">{!! Str::limit($item->description, 160, ' ...') !!}</p>
                                    </div>
                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endif

            <div class="d-flex justify-content-center">
                {{ $campaign_progress->links() }}
            </div>
        @else
            <div class="container d-flex justify-content-center align-items-center" style="height: 70vh;">
                <p class="text-center" style="word-wrap: break-word; padding: 0 20px;">
                    Maaf, Paparan ini hanya boleh diakses <br>oleh pengguna berdaftar sahaja.
                </p>
            </div>
        @endauth

    </div>
@endsection
