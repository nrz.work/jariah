@extends('layouts.app')

{{-- @dd($campaign) --}}

@section('title')
    Kempen
@endsection

@section('content')
    <div class="row mb-3">
        <img src="{{ $campaign->image }}" alt="">
    </div>

    <div class="row px-4">

        <h3 class="font-500 mt-0 mb-3">{{ $campaign->campaign_name }}</h3>

        <div class="row pt-1 mb-0">
            <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                <div class="row mb-0">
                    <div class="col-6 pe-2">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio1" value="10"
                            autocomplete="off" checked>
                        <label class="btn btn-outline-blue rounded-s m-1" for="btnradio1" style="width:100%">RM
                            10</label>
                    </div>
                    <div class="col-6 pe-2">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio2" value="20"
                            autocomplete="off">
                        <label class="btn btn-outline-blue rounded-s m-1" for="btnradio2" style="width:100%">RM
                            20</label>
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-6 pe-2">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio3" value="50"
                            autocomplete="off">
                        <label class="btn btn-outline-blue rounded-s m-1" for="btnradio3" style="width:100%">RM
                            50</label>
                    </div>
                    <div class="col-6 pe-2">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio4" value="100"
                            autocomplete="off">
                        <label class="btn btn-outline-blue rounded-s m-1" for="btnradio4" style="width:100%">RM
                            100</label>
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-6 pe-2">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio5" value="200"
                            autocomplete="off">
                        <label class="btn btn-outline-blue rounded-s m-1" for="btnradio5" style="width:100%">RM
                            200</label>
                    </div>
                    <div class="col-6 pe-2">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio6" value="500"
                            autocomplete="off">
                        <label class="btn btn-outline-blue rounded-s m-1" for="btnradio6" style="width:100%">RM
                            500</label>
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="row mb-0 pt-3 px-1">
            <input class="form-control form-control-lg" type="number" id="other" placeholder="Masukkan amaun lain"
                name="other" />
        </div> --}}

        <div class="row px-1 py-4">
            {{-- <button type="button" onclick="removeCartData()" class="btn btn-primary bg-blue font-500 rounded-2">Masuk Ke
                Bakul</button> --}}
            <button type="button" onclick="addToCart()" class="btn btn-primary bg-blue font-500 rounded-2">MASUK KE BAKUL</button>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        // Function to handle the click event on the "Masuk Ke Bakul" button and save the selected value to sessionStorage
        function addToCart() {
            // Get the selected value from the checked radio button
            var amount = $('input[name="btnradio"]:checked').val();
            var campaign_name = '{{ $campaign->campaign_name }}';
            var campaign_id = '{{ $campaign->_id }}';
            var company_name = '{{ $campaign->company->company_name }}';

            // console.log(company_name);


            var cartData = {
                amount: amount,
                campaign_name: campaign_name,
                campaign_id: campaign_id,
                company_name: company_name
            };

            // Retrieve the existing data from localStorage (if any)
            var existingCartData = localStorage.getItem('cartData');
            var cartArray = existingCartData ? JSON.parse(existingCartData) : [];

            console.log(cartArray);

            // Add the new campaign data to the cart array
            cartArray.push(cartData);

            // Save the updated cart array back to localStorage
            localStorage.setItem('cartData', JSON.stringify(cartArray));

            // Redirect to the other page
            window.location.href = '{{ url('donate') }}';

        }

        // Function to select the radio button based on the data from sessionStorage
        function selectRadioButton() {
            // Check if there is a previously selected value in sessionStorage
            var cartData = localStorage.getItem('cartData');

            // console.log(cartData);

            if (cartData) {
                // Parse the cartData from sessionStorage
                cartData = JSON.parse(cartData);

                // Mark the corresponding radio button as checked
                $('input[name="btnradio"][value="' + cartData.amount + '"]').prop('checked', true);
            }

            return cartData;
        }

        // Call the selectRadioButton function to select the radio button based on sessionStorage data when the page loads
        $(document).ready(function() {
            var cartData = selectRadioButton();
            console.log('Cart data:', cartData);
        });
    </script>
@endsection
