@extends('layouts.app')

@section('title')
    Pembayaran Gagal
@endsection

@section('content')
    {{-- Content --}}
    <div class="pt-2 px-4">

        <div class="row mb-0">
            <div class="col-md-12 text-center py-5">
                <h3>Proses pembayaran sedang dilakukan.</h3>
                <i class="fa fa-clock-o text-warning py-5" style="font-size:150px" aria-hidden="true"></i>
                <p class="m-0">Terima kasih kerana menunggu.</p>
                <p class="m-0"><a href="">Hubungi kami</a> jika terdapat sebarang pertanyaan.</p>
            </div>
        </div>
        
    </div>
    {{-- End Content --}}
@endsection
