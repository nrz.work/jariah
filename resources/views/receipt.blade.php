@extends('layouts.email')

@section('content')
    <style>
        a {
            display: block;
            width: 200px;
            height: 40px;
        }
    </style>

    <table style="border: none; cellpadding: 0; cellspacing: 0;" class="body">
        <tr>
            <td>&nbsp;</td>
            <td class="container">
                <div class="content">
                    <table class="main">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper">
                                <table>
                                    <tr>
                                        <td class="align-center">
                                            {{-- logo --}}
                                            {{-- <img src="{{ asset('backend/img/logo.png') }}" alt="logo" width="150"
                                                style="padding-top: 0.4rem"> --}}
                                                <img src="https://jariah.berqat.com/backend/img/logo.png" alt="logo" width="150"
                                                style="padding-top: 0.4rem">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="padding-top: 0.3rem">
                                                <tr>
                                                    <td
                                                        style="background-color: #EFEFFD; border-radius: 8px; padding: 1rem">
                                                        <h3>Terima kasih, Haekal Azrei!</h3>
                                                        <p style="color: #64748B; text-align:justify"> Dengan penuh rendah hati, kami ingin mengungkapkan penghargaan dan berjuta-juta terima 
                                                            kasih atas sumbangan serta sokongan yang amat berharga dari Tuan/Puan. Sumbangan yang telah diberikan oleh Tuan/Puan diharapkan akan 
                                                            memberi manfaat yang besar kepada ekosistem masyarakat Islam di Malaysia serta kepada mereka yang memerlukan.</p>
                                                        <p style="color: #64748B;">Yang ikhlas,</p>
                                                        <p style="color: #64748B; line-height: 1pt"><b>Ameera Nadia Mohd Shafiee</b></p>
                                                        <p style="color: #64748B; line-height: 1pt"><b>Pengasas eJariah</b></p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 0.3rem;">
                                            <table style="width: 100%; border-collapse: collapse;">
                                                <tr style="border-bottom: 1px solid #e3e3e3;">
                                                    <td class="w-50" style="color: #64748B;">Kempen & Penganjur</td>
                                                    <td class="align-right w-50">Amaun Sumbangan</td>
                                                </tr>
                                                <tr style="border-bottom: 1px solid #e3e3e3;">
                                                    <td>
                                                        <span class="text-primary" style="font-size: 10pt">Nama Penganjur
                                                            1</span>
                                                        <br>Dana Pembangunan Untuk Pembinaan Maahad Tahfiz Johor
                                                    </td>
                                                    <td class="align-right valign-bottom">
                                                        RM200
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom: 1px solid #e3e3e3;">
                                                    <td>
                                                        <span class="text-primary" style="font-size: 10pt">Nama Penganjur
                                                            2</span>
                                                        <br>Dana Pembangunan Untuk Pembinaan Maahad Tahfiz Johor
                                                    </td>
                                                    <td class="align-right valign-bottom">RM50</td>
                                                </tr>
                                                <tr style="border-bottom: 1px solid #e3e3e3;">
                                                    <td><b style="color: #64748B;">Jumlah</b></td>
                                                    <td class="text-primary align-right"><b>RM250</b></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 2rem;">
                                            <b style="color: #64748B;">Lain-lain Kempen Daripada Kami</b>
                                            <table>
                                                <tr>
                                                    <td class="w-50">
                                                        <a href="" style="width: 90%">
                                                            <img style="border-radius: 8px;"
                                                                src="{{ asset('frontend/img/infaq_makanan.jpeg') }}"
                                                                alt="">
                                                            <br>
                                                            <p><b>Derma Kilat Bencana Kelantan</b></p>
                                                            <span style="color: #64748B; font-size: 10pt">Yayasan Digital
                                                                Malaysia</span>
                                                        </a>
                                                    </td>
                                                    <td class="w-50">
                                                        <a href="" style="width: 90%">
                                                            <img style="border-radius: 8px;"
                                                                src="{{ asset('frontend/img/mangsa_kebakaran.jpeg') }}"
                                                                alt="">
                                                            <br>
                                                            <p><b>Infaq muharram untuk baju dan peralatan ibadah pelajar
                                                                    tahfiz di kedah</b></p>
                                                            <span style="color: #64748B; font-size: 10pt">Yayasan Digital
                                                                Malaysia</span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bottom-line" style="padding: 6rem">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="w-50">
                                                        <img src="https://jariah.berqat.com/frontend/img/about-logo.jpeg"
                                                            alt="logo" width="100">
                                                    </td>
                                                    <td class="w-50" style="color: #64748B;">
                                                        <ul class="social">
                                                            <li> <a href="https://www.facebook.com/ejariahmy"><img src="https://jariah.berqat.com/frontend/img/fb.png" alt="facebook_icon" width="30rem"></a>
                                                            </li>
                                                            <li> <a href="https://www.tiktok.com/@ejariah.my"><img src="https://jariah.berqat.com/frontend/img/tiktok.png" alt="twitter_icon" width="30rem"></a>
                                                            </li>
                                                            <li> <a href="https://www.instagram.com/ejariahdotmy/"><img src="https://jariah.berqat.com/frontend/img/ig.png" alt="instagram_icon" width="30rem"></a>
                                                            </li>
                                                        </ul>
                                                        <br>
                                                        <br>
                                                        <p>
                                                            No.95B, Aras 2, Jalan Diplomatik, Presint 15, 62050 Wilayah
                                                            Persekutuan Putrajaya
                                                        </p>
                                                        <p>
                                                            +603-8861 6792
                                                        </p>
                                                        <p>
                                                            info@ydigital.my
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>

                    <!-- END CENTERED WHITE CONTAINER -->
                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
@endsection
