@extends('layouts.app')

@section('title')
    Akaun
@endsection

@section('content')
    {{-- Content --}}
    <div class="px-4" style="height: 50rem;">

        {{-- @if (Auth::user()->role == 'Admin')
            <div class="row pt-2">
                <div class="col-md-12">
                    <h6>Tetapan</h6>
                </div>
            </div>
            <div class="row mb-0">
                <div class="col-md-12">

                    <div class="pb-3">
                        <h6 class="text-primary font-500">Emel</h6>
                        <input class="form-control form-control-lg" name="email" value="{{ $user->email }}" disabled />
                    </div>

                    <div class="pb-3">
                        <h6 class="text-primary font-500">Nama Syarikat</h6>
                        <input class="form-control form-control-lg" name="company_name" value="{{ $company->company_name }}"
                            disabled />
                    </div>

                    <div class="pb-3">
                        <h6 class="text-primary font-500">Emel Syarikat</h6>
                        <input class="form-control form-control-lg" name="company_email"
                            value="{{ $company->company_email }}" disabled />
                    </div>

                    <div class="pb-3">
                        <h6 class="text-primary font-500">No Tel.</h6>
                        <input class="form-control form-control-lg" name="company_phone"
                            value="{{ $company->company_phone }}" disabled />
                    </div>

                </div>
            </div>
        @else --}}
        <div class="row mb-1">
            <div class="col-md-12">
                <h4 class="color-blue-dark font-500">Jariah Anda</h4>
            </div>
        </div>

        <div class="row mb-0">
            <div class="col-md-12">
                @if (count($donations) > 0)
                    <table class="table">
                        <tbody>
                            @foreach ($donations as $donation)
                                <tr>
                                    <td class="line-height-s">
                                        {{ $donation->created_at }}
                                        <p class="mb-0">{{ $donation->campaign->campaign_name }}</p>
                                    </td>
                                    <td class="w-15">RM {{ number_format($donation->amount) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <p class="text-center">Tiada transaksi untuk dipaparkan.</p>
                @endif
            </div>
        </div>
        {{-- @endif --}}

    </div>
    {{-- End Content --}}
@endsection
