@extends('layouts.app')

@section('title')
    Akaun
@endsection

@section('content')
    {{-- Content --}}
    <div class="pt-2 px-4" style="height: 50rem;">

        <div class="row px-5 text-center pt-2">
            <div class="col-md-12">
                <h6 class="text-primary">Log Masuk Atau Daftar Untuk Menggunakan Kemudahan Dan Manfaat Jariah Serta Ciri-Ciri
                    Lain</h6>
            </div>

            <div class="col-md-12">
                <!-- Validation Errors -->
                {{-- <x-auth-validation-errors class="mb-4" :errors="$errors" /> --}}

                @error('name')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                @enderror

                @error('email')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                @enderror

                @error('password')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                @enderror

                @error('mobile')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
        </div>

        {{-- Tab --}}
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <button class="nav-link active" id="activity-tab" data-bs-toggle="tab" data-bs-target="#nav-activity"
                    type="button" role="tab" aria-controls="nav-activity" aria-selected="true">Log Masuk</button>
                <button class="nav-link" id="notification-tab" data-bs-toggle="tab" data-bs-target="#nav-notification"
                    type="button" role="tab" aria-controls="nav-notification" aria-selected="false">Daftar</button>
            </div>
        </nav>
        {{-- End Tab --}}

        <div class="tab-content" id="nav-tabContent">

            {{-- Log Masuk --}}
            <div class="tab-pane fade show active" id="nav-activity" role="tabpanel" aria-labelledby="activity-tab">

                <div class="row mb-0 pt-4">
                    <div class="col-md-12">

                        <form action="{{ route('login') }}" method="post">
                            @csrf

                            <div class="pb-3">
                                <h6 class="text-primary font-500">Emel</h6>
                                <input id="email" type="email" class="form-control form-control-lg" name="email"
                                    placeholder="Emel" :value="old('email')" required autofocus />
                                <i class="fa fa-check disabled valid color-green-dark"></i>
                                <i class="fa fa-check disabled invalid color-red-dark"></i>
                                <em></em>
                            </div>

                            <div class="pb-3">
                                <h6 class="text-primary font-500">Kata Laluan</h6>
                                <input id="password" type="password" class="form-control form-control-lg" name="password"
                                    placeholder="Kata Laluan" required autocomplete="current-password" />
                                <i class="fa fa-check disabled valid color-green-dark"></i>
                                <i class="fa fa-check disabled invalid color-red-dark"></i>
                                <em></em>
                            </div>

                            <div class="row mt-3">
                                <x-button class="btn btn-primary bg-blue font-500 rounded-2">
                                    {{ __('Log in') }}
                                </x-button>
                            </div>
                        </form>

                    </div>
                </div>

                <div class="row text-center">
                    {{-- <a href="" class="text-primary text-decoration-underline">Ingin Mendaftar Akaun Baru?</a> --}}
                    {{-- @if (Route::has('password.request')) --}}
                    <a class="text-primary text-decoration-underline" href="#linkwhatsapp">
                        {{-- <a class="text-primary text-decoration-underline" href="{{ route('password.request') }}"> --}}
                        {{ __('Lupa Kata Laluan?') }}
                    </a>
                    {{-- @endif --}}
                </div>

            </div>
            {{-- End Log Masuk --}}

            {{-- Daftar --}}
            <div class="tab-pane fade" id="nav-notification" role="tabpanel" aria-labelledby="notification-tab">

                <div class="row mb-0">
                    <div class="col-md-12 text-center py-3">
                        <h3 class="text-primary">Daftar Akaun Baru</h3>
                    </div>
                    <div class="col-md-12">

                        <form action="{{ route('register') }}" method="post">
                            @csrf

                            <div class="pb-3">
                                <h6 class="text-primary font-500">Nama Penuh</h6>
                                <input class="form-control form-control-lg" name="name" placeholder="Nama Penuh" />
                                <i class="fa fa-check disabled valid color-green-dark"></i>
                                <i class="fa fa-check disabled invalid color-red-dark"></i>
                                <em></em>
                            </div>

                            <div class="pb-3">
                                <h6 class="text-primary font-500">Emel</h6>
                                <input class="form-control form-control-lg" name="email" placeholder="Emel" />
                                <i class="fa fa-check disabled valid color-green-dark"></i>
                                <i class="fa fa-check disabled invalid color-red-dark"></i>
                                <em></em>
                            </div>

                            <div class="pb-3">
                                <h6 class="text-primary font-500">No Telefon</h6>
                                <input class="form-control form-control-lg" name="mobile"
                                    oninput="validatePhoneNumber(this)" pattern="[0-9]{1,11}" placeholder="No Telefon" />
                                <i class="fa fa-check disabled valid color-green-dark"></i>
                                <i class="fa fa-check disabled invalid color-red-dark"></i>
                                <em></em>
                            </div>

                            <div class="pb-3">
                                <h6 class="text-primary font-500">Kata Laluan</h6>
                                <input type="password" class="form-control form-control-lg" name="password"
                                    placeholder="Kata Laluan" />
                                <i class="fa fa-check disabled valid color-green-dark"></i>
                                <i class="fa fa-check disabled invalid color-red-dark"></i>
                                <em></em>
                            </div>

                            <div class="pb-3">
                                <h6 class="text-primary font-500">Pengesahan Kata Laluan</h6>
                                <input type="password" class="form-control form-control-lg" name="password_confirmation"
                                    placeholder="Pengesahan Kata Laluan" />
                                <i class="fa fa-check disabled valid color-green-dark"></i>
                                <i class="fa fa-check disabled invalid color-red-dark"></i>
                                <em></em>
                            </div>

                            <div class="row mt-3">
                                <button type="submit" class="btn btn-primary bg-blue font-500 rounded-2">Daftar</button>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
            {{-- End Daftar --}}
        </div>

    </div>
    {{-- End Content --}}
@endsection

@section('js')
    <script>
        function validatePhoneNumber(input) {
            // Remove any non-numeric characters from the input
            input.value = input.value.replace(/\D/g, '');

            // Limit the input to a maximum of 11 digits
            if (input.value.length > 11) {
                input.value = input.value.slice(0, 11);
            }
        }
    </script>
@endsection
