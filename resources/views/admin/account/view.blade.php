@extends('layouts.admin')

@section('title')
Account
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ $user->name }}</h1>
</div>

@if ($message = Session::get('updateuser'))
<div class="alert alert-info alert-block">
    <strong>{{ $message }}</strong>
</div>
@endif

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <div class="card shadow mb-4">
            <div class="card-body">

                <div class="row-fluid border-bottom">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Email</label>
                            <p class="text-dark">{{ $user->email }}</p> 
                        </div> 
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>IC No.</label>
                            <p class="text-dark">{{ $user->ic_no }}</p> 
                        </div> 
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Status</label>
                            @if ( Auth::user()->status == '1' )
                                <p class="text-success">Active</p> 
                            @else
                                <p class="text-danger">Not Active</p> 
                            @endif
                        </div> 
                    </div>

                </div>

                <div class="row-fluid">

                    <div class="col-md-8">
                        <h5 class="py-4">Update Profile</h5>

                        <form action="/edit-account" method="post">
                            @csrf
                            <div class="form-group mb-3">
                                <label class="text-dark">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $user->name }}"/> 
                            </div> 
                            <div class="form-group mb-3">
                                <label class="text-dark">Copy of IC</label>
                                <div class="row-fluid mb-3">
                                    <input class="p-1" name="ic_copy" type="file">
                                </div>
                            </div> 
                            <div class="form-group mb-3">
                                <label class="text-dark">Phone No.</label>
                                <input type="text" class="form-control" name="phone_number" value="{{ $user->phone_number }}"/> 
                            </div> 

                            <div class="form-group mb-3">
                                <label class="text-dark">Password</label>
                                <div class="input-group">
                                    <input type="password" id="passwordInput" name="password" class="form-control" placeholder="Fill in to update your password">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button" id="togglePasswordBtn">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </div>
                                </div>
                            </div> 

                            <div class="text-right py-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-4">
                        <ul class="py-4">
                            @if ( $user->ic_copy !== null )
                                <li><a href="">Copy of Identification Card</a></li>
                            @else 
                                <li class="text-danger">No copy of identification card has been uploaded</a></li>
                            @endif
                        </ul>
                    </div>

                </div>

                
                        
            </div>
        </div>

    </div>

</div>

@endsection