@extends('layouts.admin')

@section('title')
Transaction Record
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Financial Report</h1>
    <a href="/" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-download fa-sm text-white-50"></i> Download
    </a>
</div>

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <div class="card shadow mb-4">
            <div class="card-body">

                <div class="row p-4">
                    <div class="col-md-8">
                        <img src="{{ asset('backend/img/icon.png') }}" alt="logo" width="100">
                    </div>
                    <div class="col-md-4 text-right">
                        <h5 class="text-dark">Yayasan Digital Malaysia</h5>
                        <p class="mb-0">No. 95B, Aras 2,</p>
                        <p class="mb-0">Jalan Diplomatik, Presint 15,</p>
                        <p class="mb-0">62050 Wilayah Persekutuan Putrajaya.</p>
                    </div>
                </div>

                <hr>

                <div class="row p-4">
                    <div class="col-md-12">
                        <p class="mb-0">Report for : {{ $company->company_name }}</p>
                        <p class="mb-0">Campaign : </p>
                        <p class="mb-0">Campaign Date: </p>
                    </div>
                </div>

                <div class="row px-4">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                  <tr class="table-primary text-dark">
                                    <th scope="col">Status</th>
                                    <th scope="col">Total Donation (RM)</th>
                                    <th scope="col">Platform Charge (RM)</th>
                                    <th scope="col">Net Total (RM)</th>
                                  </tr>
                                </thead>
                                <tbody class="table-group-divider">
                                    <tr class="table-secondary">
                                        <td class="w-25">Expired</td>
                                        <td class="w-25">RM 445.00</td>
                                        <td class="w-25">RM 53.00</td>
                                        <td class="w-25">RM 392.00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row px-4">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                  <tr class="text-dark">
                                    <th scope="col">Details</th>
                                    <th scope="col">Total (RM)</th>
                                  </tr>
                                </thead>
                                <tbody class="table-group-divider">
                                    <tr>
                                        <td>Jumlah Keseluruhan NAMA KEMPEN</td>
                                        <td class="w-25">RM 445.00</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Platform Charge jariah.berqat.com (Charge Includes Payment Gateway)
                                            <br>
                                            (Charge per Transaction: 5% or RM2, which higher)
                                        </td>
                                        <td class="w-25">RM 53.00</td>
                                    </tr>
                                    <tr class="table-success">
                                        <td class="text-dark">Total Net Donation Balance (Weekly)</td>
                                        <td class="text-dark w-25">RM 392.00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                        
                <div class="row px-4">
                    <div class="col-md-6 py-2">
                        <p class="text-dark">Prepared by</p>
                        <table>
                            <tr>
                                <td class="w-50">Name</td>
                                <td>: Mifzal bin Othman</td>
                            </tr>
                            <tr>
                                <td class="w-50">Designation</td>
                                <td>: Admin</td>
                            </tr>
                            <tr>
                                <td class="w-50">Date</td>
                                <td>: 07 Jun 2023</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6 py-2">
                        <p class="text-dark">Verified by</p>
                        <table>
                            <tr>
                                <td class="w-50">Name</td>
                                <td>: Mifzal bin Othman</td>
                            </tr>
                            <tr>
                                <td class="w-50">Designation</td>
                                <td>: Admin</td>
                            </tr>
                            <tr>
                                <td class="w-50">Date</td>
                                <td>: 07 Jun 2023</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6 py-2">
                        <p class="text-dark">Received by</p>
                        <table>
                            <tr>
                                <td class="w-50">Name</td>
                                <td>: Mifzal bin Othman</td>
                            </tr>
                            <tr>
                                <td class="w-50">Designation</td>
                                <td>: Admin</td>
                            </tr>
                            <tr>
                                <td class="w-50">Date</td>
                                <td>: 07 Jun 2023</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

@endsection