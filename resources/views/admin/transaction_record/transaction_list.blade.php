@extends('layouts.admin')

@section('title')
Transaction Record
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Transaction Record</h1>
    {{-- <a href="{{ url('new-campaign') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-plus fa-sm text-white-50"></i> Make Payment
    </a> --}}
    @if (Auth::user()->role == 'Admin')

        <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#paymentModal">
            <i class="fas fa-plus fa-sm text-white-50"></i> New Record
        </button>
        <!-- Modal -->
        <div class="modal fade" id="paymentModal" tabindex="-1"
            aria-labelledby="paymentModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Record</h5>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="amount">Disburse Amount (RM)</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="company">Organization</label>
                                <select class="form-control" id="company">
                                    @foreach ($companies as $company)
                                        <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="campaign_name">Campaign Name</label>
                                <select class="form-control" id="campaign_name">
                                    @foreach ($campaigns as $campaign)
                                        <option value="{{ $campaign->id }}">{{ $campaign->campaign_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>  
        
    @else
        
    @endif
</div>

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <!-- Campaign Lists -->
        <div class="card shadow mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right pt-3">{{$campaigns->links()}}</div>
                        @if(count($campaigns) > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="table-secondary text-dark">
                                        <th scope="col">#</th>
                                        <th scope="col">Campaign Name</th>
                                        <th scope="col">Disburse Date</th>
                                        <th scope="col">Disburse Amount</th>
                                        <th scope="col">Total Disburse (A)</th>
                                        <th scope="col">Total Donation (B)</th>
                                        <th scope="col">Total Balance (B - A)</th>
                                    </tr>
                                </thead>
                                <tbody class="table-group-divider">
                                    @foreach ($campaigns as $key => $campaign)
                                    <tr>
                                        <td>{{ $campaigns->firstItem() + $key  }}</td>
                                        <td>
                                            {{-- <a href="/financial-report">{{ $campaign->campaign_name }}</a> --}}
                                            <p>{{ $campaign->campaign_name }}</p>
                                            <p class="mb-0">{{ $campaign->company->company_name }}</p>
                                            <p>
                                                @if ($campaign->third_party_id !== null)
                                                    @foreach ($companies as $key => $company)
                                                        @if ($campaign->third_party_id == $company->_id)
                                                            Third Party : {{ $company->company_name }}
                                                        @endif
                                                    @endforeach
                                                @else
                                                @endif
                                            </p>
                                        </td>
                                        <td>{{ date('d M Y', strtotime('01/07/2023')) }}</td>
                                        <td>RM{{ number_format(40, 2, '.', ',') }}</td>
                                        <td>RM{{ number_format(80, 2, '.', ',') }}</td>
                                        <td>RM{{ number_format(100, 2, '.', ',') }}</td>
                                        <td>RM{{ number_format(20, 2, '.', ',') }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <p class="text-center">There is no campaign to be displayed.</p>
                        @endif
                    </div>
                </div>

            </div>
        </div>

    </div>
    
</div>

@endsection