@extends('layouts.admin')

@section('title')
    NGO & User
@endsection

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List of NGO</h1>
        <a href="/new-ngo" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-plus fa-sm text-white-50"></i> New NGO
        </a>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-info alert-block">
            <strong>{{ $message }}</strong>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <strong>{{ $message }}</strong>
        </div>
    @endif

    @if ($message = Session::get('deleteuser'))
        <div class="alert alert-danger alert-block">
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12">

            <div class="card shadow mb-4">
                <div class="card-body">

                    <div class="float-right pt-3">{{ $ngos->links() }}</div>
                    @if (count($ngos) > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="table-secondary text-dark">
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Status</th>
                                        <th scope="col"><i class="fa fa-cogs"></i></th>
                                    </tr>
                                </thead>
                                <tbody class="table-group-divider">
                                    @foreach ($ngos as $key => $ngo)
                                        <tr>
                                            <th scope="row">{{ $ngos->firstItem() + $key }}</th>
                                            <td>
                                                <a href="{{ url('view-user') }}/{{ $ngo->_id }}">
                                                    {{ $ngo->name }}
                                                </a>
                                            </td>
                                            <td>{{ $ngo->email }}</td>
                                            <td>
                                                @if ($ngo->status == 1)
                                                    <span class="badge-pill badge-success text-xs">Active</span>
                                                @else
                                                    <span class="badge-pill badge-danger text-xs">Not Active</span>
                                                @endif
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="modal"
                                                    data-target="#exampleModal{{ $ngo->_id }}"><i class="fa fa-trash"
                                                        aria-hidden="true"></i></button>
                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModal{{ $ngo->_id }}" tabindex="-1"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Delete
                                                                    Confirmation</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure you want to delete this user ?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <a class="btn btn-danger"
                                                                    href="{{ url('delete-user') }}/{{ $ngo->_id }}">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <p class="text-center">There is no NGO to be displayed.</p>
                    @endif

                </div>
            </div>

        </div>

    </div>

@endsection
