@extends('layouts.admin')

@section('title')
    Register NGO
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">New NGO</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12">

            <div class="card border-left-primary shadow mb-4">
                <div class="card-body">

                    <form action="{{ url('store-ngo') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        {{-- Personal Details --}}
                        <div class="row-fluid mt-4">
                            <div class="col-md-12">
                                <h5>Personal Details</h5>
                                <hr>
                            </div>
                        </div>
                        <div class="row-fluid g-2 mb-3">
                            <div class="col-md-6">
                                <label class="text-dark">Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" placeholder="Enter fullname"
                                    required>
                            </div>
                            <div class="col-md-6">
                                <label class="text-dark">IC No. <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" oninput="validateICNumber(this)" name="ic_no"
                                    placeholder="Enter identity card" required>
                            </div>
                        </div>

                        <div class="row-fluid g-2 mb-3">
                            <div class="col-md-4">
                                <label class="text-dark">Email <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" placeholder="Enter email address"
                                    required>
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark">Phone No. <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" oninput="validatePhoneNumber(this)"
                                    name="phone_number" placeholder="Enter phone number" required>
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark">Upload Identity Card <span class="text-danger">*</span></label>
                                <div class="row-fluid mb-3">
                                    <input class="p-1" name="ic_copy" type="file">
                                </div>
                            </div>
                        </div>

                        {{-- Organization Details --}}
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <h5>Orgnanization Details</h5>
                                <hr>
                            </div>
                        </div>
                        <div class="row-fluid g-2 mb-3">
                            <div class="col-md-6">
                                <label class="text-dark">Organization Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="company_name"
                                    placeholder="Yayasan Malaysia Berhad">
                            </div>
                            <div class="col-md-6">
                                <label class="text-dark">SSM No. <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="ssm_no" placeholder="PPM-012-34-56789101">
                            </div>
                        </div>

                        <div class="row-fluid g-2 mb-3">
                            <div class="col-md-6">
                                <label class="text-dark">Email (Organization) <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="company_email"
                                    placeholder="organization@gmail.com">
                            </div>
                            <div class="col-md-6">
                                <label class="text-dark">Phone No. (Office) <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" oninput="validatePhoneNumber(this)"
                                    name="company_phone" placeholder="0123456789">
                            </div>
                        </div>

                        <div class="row-fluid g-2 mb-3">
                            <div class="col-md-6">
                                <label class="text-dark">Company Address <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="address"
                                    placeholder="No 1, Level 1, Jalan Kenanga, 83600 Batu Pahat, Johor.">
                            </div>
                            <div class="col-md-6">
                                <label class="text-dark">Company Details</label>
                                <input type="text" class="form-control" name="company_details"
                                    placeholder="NGO berdaftar">
                            </div>
                        </div>


                        <div class="row-fluid">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="text-dark">e-ROSES / SSM / Organization Registration Certificate</label>
                                    <div class="row-fluid mb-3">
                                        <input class="p-1" name="organization_cert" type="file">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="text-dark">Company / Organization Profile (If Available)</label>
                                    <div class="row-fluid mb-3">
                                        <input class="p-1" name="company_profile" type="file">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="text-dark">First Page of Bank Statement (Proof of Bank Account)</label>
                                    <div class="row-fluid mb-3">
                                        <input class="p-1" name="bank_statement" type="file">
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Bank Details --}}
                        <div class="row-fluid mt-4">
                            <div class="col-md-12">
                                <h5>Account Bank Details</h5>
                                <hr>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="text-dark">Bank's Name</label>
                                    <select name="bank_code" class="form-control">
                                        <option value="">Please Choose</option>
                                        <option value="01">AmBank Berhad</option>
                                        <option value="02">Alliance Bank Berhad</option>
                                        <option value="03">Al-Rajhi Banking and Investment Corporation (Malaysia)
                                            Berhad
                                        </option>
                                        <option value="04">Affin Bank Berhad</option>
                                        <option value="05">Agro Bank</option>
                                        <option value="06">Bank of China (Malaysia) Berhad</option>
                                        <option value="07">Bank Muamalat Malaysia Berhad</option>
                                        <option value="08">Bank Islam Malaysia Berhad</option>
                                        <option value="09">Bank Rakyat Berhad</option>
                                        <option value="10">Bank Simpanan Nasional Berhad</option>
                                        <option value="11">Bank of America</option>
                                        <option value="12">Bank of Tokyo-Mitsubishi UFJ (Malaysia) Berhad</option>
                                        <option value="13">BNP Paribas (M) Bhd / BNP Paribas (Islamic)</option>
                                        <option value="14">CIMB Bank Berhad</option>
                                        <option value="15">Citibank</option>
                                        <option value="16">Deutsche Bank (M) Bhd</option>
                                        <option value="17">Hong Leong Bank Berhad/ Hong Leong Finance</option>
                                        <option value="18">HSBC Bank Berhad</option>
                                        <option value="19">Indust &amp; Comm Bank of China (M) Berhad</option>
                                        <option value="20">JP Morgan Chase</option>
                                        <option value="21">Kuwait Finance House</option>
                                        <option value="22">Malayan Banking Berhad</option>
                                        <option value="23">Mizuho Bank (M) Berhad</option>
                                        <option value="24">OCBC Bank Berhad</option>
                                        <option value="25">Public Bank Berhad / Public Finance Berhad</option>
                                        <option value="26">RHB Bank Berhad</option>
                                        <option value="27">Standard Chartered Bank Berhad</option>
                                        <option value="28">Sumitomo Mitsui Banking Corporation (M) Bhd</option>
                                        <option value="29">The Royal Bank of Scotland Bhd</option>
                                        <option value="30">United Overseas Bank (M) Bhd</option>
                                        <option value="31">China Construction Bank (Malaysia) Berhad</option>
                                        <option value="32">Bangkok Bank Berhad</option>
                                        <option value="33">MBSB</option>
                                        <option value="00">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="text-dark">Owner's Name</label>
                                    <input type="text" class="form-control" name="bank_owner"
                                        placeholder="Yayasan Malaysia Berhad">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="text-dark">Account No.</label>
                                    <input type="text" class="form-control" oninput="validateAccountNumber(this)" name="account_no"
                                        placeholder="012909019922">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="text-right py-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function validatePhoneNumber(input) {
            // Remove any non-numeric characters from the input
            input.value = input.value.replace(/\D/g, '');

            // Limit the input to a maximum of 11 digits
            if (input.value.length > 11) {
                input.value = input.value.slice(0, 11);
            }
        }
    </script>
    <script>
        function validateICNumber(input) {
            // Remove any non-numeric characters from the input
            input.value = input.value.replace(/\D/g, '');

            // Limit the input to a maximum of 11 digits
            if (input.value.length > 11) {
                input.value = input.value.slice(0, 12);
            }
        }
    </script>
    <script>
        function validateAccountNumber(input) {
            // Remove any non-numeric characters from the input
            input.value = input.value.replace(/\D/g, '');

            // Limit the input to a maximum of 11 digits
            if (input.value.length > 11) {
                input.value = input.value.slice(0, 18);
            }
        }
    </script>
@endsection
