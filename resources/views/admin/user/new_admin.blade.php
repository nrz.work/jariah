@extends('layouts.admin')

@section('title')
User
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">New Admin</h1>
</div>

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <div class="card border-left-primary shadow mb-4">
            <div class="card-body">

                <form action="store-admin" class="col-md-8" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" placeholder="John Doe">
                    </div>
                    <div class="form-group">
                        <label>IC No.</label>
                        <input type="text" class="form-control" name="ic_no" placeholder="901203045678">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" placeholder="johndoe@email.com">
                    </div>
                    <div class="form-group">
                        <label>Phone No. </label>
                        <input type="text" class="form-control" name="phone_number" placeholder="0123456789">
                    </div>

                    <div class="text-right py-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
                
            </div>
        </div>

    </div>

</div>


@endsection