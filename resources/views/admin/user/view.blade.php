@extends('layouts.admin')

@section('title')
User
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Update User</h1>
</div>

@if ($message = Session::get('updateuser'))
<div class="alert alert-info alert-block">
    <strong>{{ $message }}</strong>
</div>
@endif

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <div class="card border-left-primary shadow mb-4">
            <div class="card-body">
                @if ( $user->role == 'NGO' )

                    <div class="row-fluid">

                        <div class="col-md-8">
                            <form action="{{ url('edit-user') }}/{{ $user->_id }}" method="post">
                                @csrf

                                {{-- Personal Details --}}
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <h5>Personal Details</h5>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row g-2 mb-4">
                                    <div class="col-md-6">
                                        <label class="text-dark">Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="name" value="{{ $user->name ?? '' }}"
                                            required>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-dark">IC No. <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" oninput="validateICNumber(this)" name="ic_no"
                                        value="{{ $user->ic_no ?? '' }}" required>
                                    </div>
                                </div>

                                <div class="row g-2 mb-4">
                                    <div class="col-md-4">
                                        <label class="text-dark">Email <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" name="email" value="{{ $user->email ?? '' }}"
                                            required>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Password</label>
                                        <div class="input-group">
                                            <input type="password" id="passwordInput" name="password" class="form-control" placeholder="Fill in to update your password">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button" id="togglePasswordBtn">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="text-dark">Phone No. <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" oninput="validatePhoneNumber(this)"
                                            name="phone_number" {{ $user->phone_number ?? '' }} required>
                                    </div>
                                </div>

                                <div class="row g-2 mb-4">
                                    <div class="col-md-4">
                                        <label class="text-dark">Upload Identity Card <span class="text-danger">*</span></label>
                                        <div class="row-fluid mb-3">
                                            <input class="p-1" name="ic_copy" type="file">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <p>
                                                Status  
                                            @if ( $user->status == 1 )
                                                <span class="badge-pill badge-success text-xs">Active</span>
                                            @else
                                                <span class="badge-pill badge-danger text-xs">Not Active</span>
                                            @endif      
                                            </p>      
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1">
                                                <label class="form-check-label" for="inlineRadio1">Active</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0">
                                                <label class="form-check-label" for="inlineRadio2">Not Active</label>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Role</label>
                                            <select class="form-control" name="role">
                                                <optgroup label="Current Role : {{ $user->role }}">
                                                    <option value="Admin">Admin</option>
                                                    <option value="NGO">NGO</option>
                                                    <option value="User">User</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label>IC Copy</label>
                                            <div class="row-fluid mb-3 pb-4">
                                                <input class="p-1" name="ic_copy" type="file">
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>

                                {{-- Organization Details --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>Orgnanization Details</h5>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row g-2 mb-4">
                                    <div class="col-md-6">
                                        <label class="text-dark">Organization Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="company_name"
                                        value="{{ $company->company_name ?? '' }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-dark">SSM No. <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="ssm_no" value="{{ $company->ssm_no ?? '' }}">
                                    </div>
                                </div>

                                <div class="row g-2 mb-4">
                                    <div class="col-md-6">
                                        <label class="text-dark">Email (Organization) <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="company_email"
                                        value="{{ $company->company_email ?? '' }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-dark">Phone No. (Office) <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" oninput="validatePhoneNumber(this)"
                                            name="company_phone" value="{{ $company->company_phone ?? '' }}">
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <div class="col-md-12">
                                        <label class="text-dark">Company Address <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="address" cols="30" rows="3">{{ $company->address ?? '' }}</textarea>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <div class="col-md-12">
                                        <label class="text-dark">Company Details</label>
                                        <textarea class="form-control" name="company_details" cols="30" rows="3">{{ $company->company_details ?? '' }}</textarea>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-dark">e-ROSES / SSM / Organization Registration Certificate</label>
                                            <div class="row-fluid mb-3">
                                                <input class="p-1" name="organization_cert" type="file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-dark">Company / Organization Profile (If Available)</label>
                                            <div class="row-fluid mb-3">
                                                <input class="p-1" name="company_profile" type="file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-dark">First Page of Bank Statement (Proof of Bank Account)</label>
                                            <div class="row-fluid mb-3">
                                                <input class="p-1" name="bank_statement" type="file">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Bank Details --}}
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <h5>Account Bank Details</h5>
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-dark">Bank's Name</label>
                                            <select name="bank_code" class="form-control">
                                                <option value="">Please Choose</option>
                                                <option value="01">AmBank Berhad</option>
                                                <option value="02">Alliance Bank Berhad</option>
                                                <option value="03">Al-Rajhi Banking and Investment Corporation (Malaysia)
                                                    Berhad
                                                </option>
                                                <option value="04">Affin Bank Berhad</option>
                                                <option value="05">Agro Bank</option>
                                                <option value="06">Bank of China (Malaysia) Berhad</option>
                                                <option value="07">Bank Muamalat Malaysia Berhad</option>
                                                <option value="08">Bank Islam Malaysia Berhad</option>
                                                <option value="09">Bank Rakyat Berhad</option>
                                                <option value="10">Bank Simpanan Nasional Berhad</option>
                                                <option value="11">Bank of America</option>
                                                <option value="12">Bank of Tokyo-Mitsubishi UFJ (Malaysia) Berhad</option>
                                                <option value="13">BNP Paribas (M) Bhd / BNP Paribas (Islamic)</option>
                                                <option value="14">CIMB Bank Berhad</option>
                                                <option value="15">Citibank</option>
                                                <option value="16">Deutsche Bank (M) Bhd</option>
                                                <option value="17">Hong Leong Bank Berhad/ Hong Leong Finance</option>
                                                <option value="18">HSBC Bank Berhad</option>
                                                <option value="19">Indust &amp; Comm Bank of China (M) Berhad</option>
                                                <option value="20">JP Morgan Chase</option>
                                                <option value="21">Kuwait Finance House</option>
                                                <option value="22">Malayan Banking Berhad</option>
                                                <option value="23">Mizuho Bank (M) Berhad</option>
                                                <option value="24">OCBC Bank Berhad</option>
                                                <option value="25">Public Bank Berhad / Public Finance Berhad</option>
                                                <option value="26">RHB Bank Berhad</option>
                                                <option value="27">Standard Chartered Bank Berhad</option>
                                                <option value="28">Sumitomo Mitsui Banking Corporation (M) Bhd</option>
                                                <option value="29">The Royal Bank of Scotland Bhd</option>
                                                <option value="30">United Overseas Bank (M) Bhd</option>
                                                <option value="31">China Construction Bank (Malaysia) Berhad</option>
                                                <option value="32">Bangkok Bank Berhad</option>
                                                <option value="33">MBSB</option>
                                                <option value="00">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-dark">Owner's Name</label>
                                            <input type="text" class="form-control" name="bank_owner"
                                                value="{{ $company->bank_owner ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-dark">Account No.</label>
                                            <input type="text" class="form-control" oninput="validateAccountNumber(this)" name="account_no"
                                            value="{{ $company->acc_no ?? '' }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="text-right py-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                
                            </form>
                        </div>

                        <div class="col-md-4">
                            <ul class="py-4">
                                @if ( $user->ic_copy !== null )
                                    <li><a href="{{ $user->ic_copy }}">Copy of Identification Card</a></li>
                                @else 
                                    <li class="text-danger">No copy of identification card has been uploaded</a></li>
                                @endif
                                @if ( $company->ssm_cert !== null )
                                    <li><a href="{{ $company->ssm_cert }}">Organization Certificate</a></li>
                                @else 
                                    <li class="text-danger">No organization certificate has been uploaded</a></li>
                                @endif
                                @if ( $company->company_profile !== null )
                                    <li><a href="{{ $company->company_profile }}">Organization Profile</a></li>
                                @else 
                                @endif
                                @if ( $company->bank_statement !== null )
                                    <li><a href="{{ $company->bank_statement }}">Proof of Bank Account</a></li>
                                @else 
                                    <li class="text-danger">No proof of bank account has been uploaded</a></li>
                                @endif
                            </ul>
                        </div>

                    </div>

                @else 

                    <div class="row-fluid">

                        <div class="col-md-8">
                            <form action="{{ url('edit-user') }}/{{ $user->_id }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $user->name ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label>IC No.</label>
                                    <input type="text" class="form-control" name="ic_no" value="{{ $user->ic_no ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" value="{{ $user->email ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input type="password" id="passwordInput" name="password" class="form-control" placeholder="Fill in to update your password">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button" id="togglePasswordBtn">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Phone No. </label>
                                    <input type="text" class="form-control" name="phone_number" value="{{ $user->phone_number ?? '' }}">
                                </div>

                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control" name="role">
                                        <optgroup label="Current Role : {{ $user->role }}">
                                            <option value="Admin">Admin</option>
                                            <option value="NGO">NGO</option>
                                            <option value="User">User</option>
                                        </optgroup>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>
                                                Status  
                                            @if ( $user->status == 1 )
                                                <span class="badge-pill badge-success text-xs">Active</span>
                                            @else
                                                <span class="badge-pill badge-danger text-xs">Not Active</span>
                                            @endif      
                                            </p>      
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1">
                                                <label class="form-check-label" for="inlineRadio1">Active</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0">
                                                <label class="form-check-label" for="inlineRadio2">Not Active</label>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>IC Copy</label>
                                            <div class="row-fluid mb-3 pb-4">
                                                <input class="p-1" name="ic_copy" type="file">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-right py-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                
                            </form>
                        </div>

                        <div class="col-md-4">
                            <ul class="py-4">
                                @if ( $user->ic_copy !== null )
                                    <li><a href="">Copy of Identification Card</a></li>
                                @else 
                                    <li class="text-danger">No copy of identification card has been uploaded</a></li>
                                @endif
                            </ul>
                        </div>

                    </div>

                @endif
            </div>
        </div>

    </div>

</div>


@endsection