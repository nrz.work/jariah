@extends('layouts.admin')

@section('title')
Organization
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Update Organization</h1>
</div>

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row-fluid">

                    <div class="col-md-8">
                        <form action="{{ url('edit-organization')}}/{{ $company->_id }}" method="post"  enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="text-dark">SSM No.</label>
                                <input type="text" class="form-control" name="ssm_no" value="{{ $company->ssm_no }}">
                                {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                            </div>
                            <div class="form-group">
                                <label class="text-dark">Address</label>
                                <textarea type="text" class="form-control" name="address">{{ $company->address }}</textarea>
                                {{-- <select class="form-control" name="state_code">
                                    <option value="">Please Choose</option>
                                    <option value="01">Johor</option>
                                    <option value="02">Kedah</option>
                                    <option value="03">Kelantan</option>
                                    <option value="04">Melaka</option>
                                    <option value="05">Negeri Sembilan</option>
                                    <option value="06">Pahang</option>
                                    <option value="07">Pulau Pinang</option>
                                    <option value="08">Perak</option>
                                    <option value="09">Perlis</option>
                                    <option value="10">Sabah</option>
                                    <option value="11">Sarawak</option>
                                    <option value="12">Selangor</option>
                                    <option value="13">Terengganu</option>
                                    <option value="14">Wilayah Persekutuan Kuala Lumpur</option>
                                    <option value="15">Wilayah Persekutuan Labuan</option>
                                    <option value="16">Wilayah Persekutuan Putrajaya</option>
                                    <option value="00">Others</option>
                                </select> --}}
                            </div>
                            <div class="form-group">
                                <label class="text-dark">Phone No. </label>
                                <input type="text" class="form-control" name="company_phone" value="{{ $company->company_phone }}">
                            </div>
                            <div class="form-group">
                                <label class="text-dark">Company Details</label>
                                <input type="text" class="form-control" name="company_details" value="{{ $company->company_details }}">
                            </div>
                            {{-- <div class="form-group">
                                <label class="text-dark">Phone No. (Office)</label>
                                <input type="text" class="form-control" name="company_phone" value="">
                            </div> --}}
                            {{-- <div class="row"> --}}
                                <div class="form-group">
                                    <label class="text-dark">e-ROSES / SSM / Organization Registration Certificate</label>
                                    <div class="row-fluid mb-3 pb-4">
                                        <input class="p-1" name="organization_cert" type="file">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="text-dark">Company / Organization Profile (If Available)</label>
                                    <div class="row-fluid mb-3 pb-4">
                                        <input class="p-1" name="company_profile" type="file">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="text-dark">First Page of Bank Statement (Proof of Bank Account)</label>
                                    <div class="row-fluid mb-3 pb-4">
                                        <input class="p-1" name="bank_statement" type="file">
                                    </div>
                                </div>
                            {{-- </div> --}}
                            <hr>
                            <div class="form-group">
                                <label class="text-dark">Bank's Name</label>
                                <select name="bank_code" class="form-control">
                                    <option value="">Please Choose</option>
                                    <option value="01">AmBank Berhad</option>
                                    <option value="02">Alliance Bank Berhad</option>
                                    <option value="03">Al-Rajhi Banking and Investment Corporation (Malaysia) Berhad</option>
                                    <option value="04">Affin Bank Berhad</option>
                                    <option value="05">Agro Bank</option>
                                    <option value="06">Bank of China (Malaysia) Berhad</option>
                                    <option value="07">Bank Muamalat Malaysia Berhad</option>
                                    <option value="08">Bank Islam Malaysia Berhad</option>
                                    <option value="09">Bank Rakyat Berhad</option>
                                    <option value="10">Bank Simpanan Nasional Berhad</option>
                                    <option value="11">Bank of America</option>
                                    <option value="12">Bank of Tokyo-Mitsubishi UFJ (Malaysia) Berhad</option>
                                    <option value="13">BNP Paribas (M) Bhd / BNP Paribas (Islamic)</option>
                                    <option value="14">CIMB Bank Berhad</option>
                                    <option value="15">Citibank</option>
                                    <option value="16">Deutsche Bank (M) Bhd</option>
                                    <option value="17">Hong Leong Bank Berhad/ Hong Leong Finance</option>
                                    <option value="18">HSBC Bank Berhad</option>
                                    <option value="19">Indust &amp; Comm Bank of China (M) Berhad</option>
                                    <option value="20">JP Morgan Chase</option>
                                    <option value="21">Kuwait Finance House</option>
                                    <option value="22">Malayan Banking Berhad</option>
                                    <option value="23">Mizuho Bank (M) Berhad</option>
                                    <option value="24">OCBC Bank Berhad</option>
                                    <option value="25">Public Bank Berhad / Public Finance Berhad</option>
                                    <option value="26">RHB Bank Berhad</option>
                                    <option value="27">Standard Chartered Bank Berhad</option>
                                    <option value="28">Sumitomo Mitsui Banking Corporation (M) Bhd</option>
                                    <option value="29">The Royal Bank of Scotland Bhd</option>
                                    <option value="30">United Overseas Bank (M) Bhd</option>
                                    <option value="31">China Construction Bank (Malaysia) Berhad</option>
                                    <option value="32">Bangkok Bank Berhad</option>
                                    <option value="33">MBSB</option>
                                    <option value="00">Others</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="text-dark">Owner's Name</label>
                                <input type="text" class="form-control" name="bank_owner" value="{{ $company->bank_owner }}">
                            </div>
                            <div class="form-group">
                                <label class="text-dark">Account No.</label>
                                <input type="text" class="form-control" name="account_no" value="{{ $company->account_no }}">
                            </div>

                            <div class="text-right py-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-4">
                        <ul class="py-4">
                            @if ( $company->ssm_cert !== null )
                                <li><a href="">Organization Certificate</a></li>
                            @else 
                                <li class="text-danger">No organization certificate has been uploaded</a></li>
                            @endif
                            @if ( $company->company_profile !== null )
                                <li><a href="">Organization Profile</a></li>
                            @else 
                            @endif
                            @if ( $company->bank_statement !== null )
                                <li><a href="">Proof of Bank Account</a></li>
                            @else 
                                <li class="text-danger">No proof of bank account has been uploaded</a></li>
                            @endif
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>

</div>


@endsection