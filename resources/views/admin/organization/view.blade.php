@extends('layouts.admin')

@section('title')
Organization
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Organization Details</h1>
    <a href="/update-organization" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-edit fa-sm text-white-50"></i> Update
    </a>
</div>

@if ($message = Session::get('updatecompany'))
<div class="alert alert-info alert-block">
    <strong>{{ $message }}</strong>
</div>
@endif

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <div class="card shadow mb-4">
            <div class="card-body">

                <div class="row justify-content-center my-3">
                    {{-- <img class="img-fluid" src="{{ asset('frontend/img/ydm.png') }}" alt="logo" width="100"> --}}
                    <img class="img-fluid" src="{{ $company->company_profile }}" alt="logo" width="15%">
                </div>

                <div class="row justify-content-center">
                    @if ( Auth::user()->status == '1')
                        <span class="badge-pill badge-success text-xs">Active</span>
                    @else
                        <span class="badge-pill badge-danger text-xs">Not Active</span>
                    @endif 
                </div>

                <hr>

                <div class="row justify-content-center mt-3">
                    <p class="text-dark">List of Documents</p>
                </div>

                <div class="row justify-content-center mb-3">
                    {{-- List of document in file type --}}
                    <ul>
                        @if ( $company->ssm_cert !== null )
                            <li><a href="{{ $company->ssm_cert }}">Organization Certificate</a></li>
                        @else 
                            <li>No organization certificate has been uploaded</a></li>
                        @endif
                        @if ( $company->company_profile !== null )
                            <li><a href="{{ $company->company_profile }}">Organization Profile</a></li>
                        @else 
                        @endif
                        @if ( $company->bank_statement !== null )
                            <li><a href="{{ $company->bank_statement }}">Proof of Bank Account</a></li>
                        @else 
                            <li>No proof of bank account has been uploaded</a></li>
                        @endif
                    </ul>
                    
                </div>
                        
            </div>
        </div>

    </div>

</div>


<!-- Content Row -->
<div class="row">

    <div class="col-md-7 mb-3">

        <div class="card shadow h-100">
            <div class="card-body">

                <h4 class="text-dark">Campaign</h4>

                <hr>

                <table class="table table-borderless">
                    <tr>
                        <td>Organization Name</td>
                        <td class="text-dark">{{ $company['company_name'] }}</td>
                    </tr>
                    <tr>
                        <td>SSM No.</td>
                        <td class="text-dark">{{ $company['ssm_no'] }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td class="text-dark">{{ $company['company_email'] }}</td>
                    </tr>
                    <tr>
                        <td>Phone No.</td>
                        <td class="text-dark">{{ $company['company_phone'] }}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td class="text-dark">{{ $company['company_details'] }}</td>
                    </tr>
                    <tr>
                        <td>Company Address</td>
                        <td class="text-dark">{{ $company['address'] }}</td>
                    </tr>
                </table>

            </div>
        </div>

    </div>

    <div class="col-md-5 mb-3">

        <div class="card shadow h-100">
            <div class="card-body">

                <h4 class="text-dark">Bank Details</h4>
                
                <hr>

                <table class="table table-borderless">
                    <tr>
                        <td>Bank's Name</td>
                        <td class="text-dark">
                            @if ( $company['bank_code'] == '00' )
                                Others
                            @elseif ( $company['bank_code'] == '01' )
                                AmBank Berhad
                            @elseif ( $company['bank_code'] == '02' )
                                Alliance Bank Berhad
                            @elseif ( $company['bank_code'] == '03' )
                                Al-Rajhi Banking and Investment Corporation (Malaysia) Berhad
                            @elseif ( $company['bank_code'] == '04' )
                                Affin Bank Berhad
                            @elseif ( $company['bank_code'] == '05' )
                                Agro Bank
                            @elseif ( $company['bank_code'] == '06' )
                                Bank of China (Malaysia) Berhad
                            @elseif ( $company['bank_code'] == '07' )
                                Bank Muamalat Malaysia Berhad
                            @elseif ( $company['bank_code'] == '08' )
                                Bank Islam Malaysia Berhad
                            @elseif ( $company['bank_code'] == '09' )
                                Bank Rakyat Berhad
                            @elseif ( $company['bank_code'] == '10' )
                                Bank Simpanan Nasional Berhad
                            @elseif ( $company['bank_code'] == '11' )
                                Bank of America
                            @elseif ( $company['bank_code'] == '12' )
                                Bank of Tokyo-Mitsubishi UFJ (Malaysia) Berhad
                            @elseif ( $company['bank_code'] == '13' )
                                BNP Paribas (M) Bhd / BNP Paribas (Islamic)
                            @elseif ( $company['bank_code'] == '14' )
                                CIMB Bank Berhad
                            @elseif ( $company['bank_code'] == '15' )
                                Citibank
                            @elseif ( $company['bank_code'] == '16' )
                                Deutsche Bank (M) Bhd
                            @elseif ( $company['bank_code'] == '17' )
                                Hong Leong Bank Berhad/ Hong Leong Finance
                            @elseif ( $company['bank_code'] == '18' )
                                HSBC Bank Berhad
                            @elseif ( $company['bank_code'] == '19' )
                                Indust &amp; Comm Bank of China (M) Berhad
                            @elseif ( $company['bank_code'] == '20' )
                                JP Morgan Chase
                            @elseif ( $company['bank_code'] == '21' )
                                Kuwait Finance House
                            @elseif ( $company['bank_code'] == '22' )
                                Malayan Banking Berhad
                            @elseif ( $company['bank_code'] == '23' )
                                Mizuho Bank (M) Berhad
                            @elseif ( $company['bank_code'] == '24' )
                                OCBC Bank Berhad
                            @elseif ( $company['bank_code'] == '25' )
                                Public Bank Berhad / Public Finance Berhad
                            @elseif ( $company['bank_code'] == '26' )
                                RHB Bank Berhad
                            @elseif ( $company['bank_code'] == '27' )
                                Standard Chartered Bank Berhad
                            @elseif ( $company['bank_code'] == '28' )
                                Sumitomo Mitsui Banking Corporation (M) Bhd
                            @elseif ( $company['bank_code'] == '29' )
                                The Royal Bank of Scotland Bhd
                            @elseif ( $company['bank_code'] == '30' )
                                United Overseas Bank (M) Bhd
                            @elseif ( $company['bank_code'] == '31' )
                                China Construction Bank (Malaysia) Berhad
                            @elseif ( $company['bank_code'] == '32' )
                                Bangkok Bank Berhad
                            @elseif ( $company['bank_code'] == '33' )
                                MBSB
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Owner's Name</td>
                        <td class="text-dark">{{ $company['bank_owner'] }}</td>
                    </tr>
                    <tr>
                        <td>Account No.</td>
                        <td class="text-dark">{{ $company['account_no'] }}</td>
                    </tr>
                </table>

            </div>
        </div>

    </div>
    
</div>

@endsection