@extends('layouts.admin')

@section('title')
    Statistic
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Statistic by Year</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <!-- Campaign Lists -->
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form action="{{ url('statistic-by-year') }}" method="get">
                        @csrf
                        <div class="row">
                            <div class="col-md-5 mb-3">
                                <label for="select" class="form-label">Year</label>
                                <select class="form-select" name="year" id="year">
                                    <!-- JavaScript will generate options here -->
                                </select>
                            </div>

                            <div class="col-md-2 mb-3">
                                <button type="submit" class="btn btn-info btn-block" style="margin-top: 2rem">View</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12">

            <!-- Overall Transaction -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Overall Transaction</h6>
                </div>
                <div class="card-body">
                    <div class="row text-center">
                        <div class="col-md-3 mb-3">
                            <p>Total Transaction</p>
                            <h2 class="text-success">{{ $donation_count }}</h2>
                        </div>
                        <div class="col-md-3 mb-3">
                            <p>Total Donation</p>
                            <h2 class="text-warning">RM{{ number_format($total_donation_sum, 2) }}</h2>
                        </div>
                        <div class="col-md-3 mb-3">
                            <p>Total Service Charge (6%)</p>
                            <h2 class="text-info">
                                RM{{ number_format($total_donation_sum, 2) - (number_format($total_donation_sum, 2) - $total_net_donation) }}
                            </h2>
                        </div>
                        <div class="col-md-3 mb-3">
                            <p>Total Donation (Net)</p>
                            <h2 class="text-primary">
                                RM{{ number_format($total_donation_sum, 2) - number_format($total_net_donation, 2) }}</h2>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Area Chart -->
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Total Donor</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myLineChart"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <!-- Area Chart -->
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Total Donation (RM)</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myBarChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <!-- Overall Transaction -->
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="table-secondary text-dark">
                                    <th scope="col">#</th>
                                    <th scope="col">Campaign</th>
                                    <th scope="col">Total Donation</th>
                                    <th scope="col">Total Service Charge</th>
                                    <th scope="col">Total Amount of Donation (Net)</th>
                                </tr>
                            </thead>
                            <tbody class="table-group-divider">
                                @php
                                    $count = 1;
                                    $campaignTotals = []; // Initialize an associative array to store totals for each campaign ID
                                @endphp
                                @foreach ($total_amount_campaign_donation as $item)
                                    @php
                                        // Check if the campaign ID exists in the associative array, if not, initialize it to 0
                                        if (!isset($campaignTotals[$item->campaign_id])) {
                                            $campaignTotals[$item->campaign_id] = 0;
                                        }
                                        
                                        // Add the current amount to the total for the campaign ID
                                        $campaignTotals[$item->campaign_id] += $item->amount;
                                    @endphp
                                @endforeach

                                <!-- Display the total amount for each campaign ID after the loop -->
                                @foreach ($campaignTotals as $campaignId => $totalAmount)
                                    @php
                                        // use App\Models\Campaign;
                                        // Retrieve the campaign associated with the current $campaignId
                                        $campaign = App\Models\Campaign::find($campaignId);
                                    @endphp
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td><a
                                                href="{{ url('statistic-by-campaign') }}/{{ $campaign->_id }}">{{ $campaign->campaign_name }}</a>
                                        </td>
                                        <td>RM{{ number_format($totalAmount, 2) }}</td>
                                        <td>RM{{ number_format($totalAmount, 2) * 0.06 }}</td>
                                        <td>RM{{ number_format($totalAmount, 2) - number_format($totalAmount, 2) * 0.06 }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        // Get the current year
        var currentYear = new Date().getFullYear();

        // Get the select element by its ID
        var yearSelect = document.getElementById("year");

        // Number of years to include in the future
        var numYears = 5;

        // Loop to add options for the current year, the previous year, and the next five years
        for (var i = -1; i <= numYears; i++) {
            var option = document.createElement("option");
            var year = currentYear + i;
            option.value = year;
            option.text = year;
            yearSelect.appendChild(option);
        }
    </script>

    <!-- Your JavaScript code to create the bar chart -->
    <script>
        // Get the canvas element
        var ctx = document.getElementById('myLineChart').getContext('2d');

        var yearNames = @json($getYear);
        var data = [];

        // Fetch total donor count for each year and populate the data array
        @foreach ($getYear as $year)
            @if ($user->role == 'Admin')
                @php
                    $totalDonor = App\Models\Donation::where('billplz_status', 'paid')
                        ->whereBetween('created_at', [new MongoDB\BSON\UTCDateTime(strtotime("{$year}-01-01 00:00:00") * 1000), new MongoDB\BSON\UTCDateTime(strtotime("{$year}-12-31 23:59:59") * 1000)])
                        ->count();
                @endphp

                data.push({{ $totalDonor }});
            @else
                @php
                    $campaignIds = App\Models\Campaign::where('company_id', $company->_id)->pluck('_id');
                    $totalDonor = App\Models\Donation::where('billplz_status', 'paid')
                        ->whereIn('campaign_id', $campaignIds)
                        ->whereBetween('created_at', [new MongoDB\BSON\UTCDateTime(strtotime("{$year}-01-01 00:00:00") * 1000), new MongoDB\BSON\UTCDateTime(strtotime("{$year}-12-31 23:59:59") * 1000)])
                        ->count();
                @endphp

                data.push({{ $totalDonor }});
            @endif
        @endforeach

        // Create a line chart
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: yearNames,
                datasets: [{
                    label: 'Yearly Data',
                    data: data,
                    borderColor: 'rgb(75, 192, 192)', // Line color
                    borderWidth: 2
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>

    <script>
        var getYear = @json($getYear);
        var data = [];

        // Fetch total donor count for each year and populate the data array
        @foreach ($getYear as $year)
            @if ($user->role == 'Admin')
                @php
                    $totalDonor = App\Models\Donation::where('billplz_status', 'paid')
                        ->whereBetween('created_at', [new MongoDB\BSON\UTCDateTime(strtotime("{$year}-01-01 00:00:00") * 1000), new MongoDB\BSON\UTCDateTime(strtotime("{$year}-12-31 23:59:59") * 1000)])
                        ->sum('amount');
                    
                    $total_net_donation = number_format($totalDonor, 2) - number_format($totalDonor * 0.06, 2);
                @endphp

                data.push({{ $total_net_donation }});
            @else
                @php
                    $campaignIds = App\Models\Campaign::where('company_id', $company->_id)->pluck('_id');
                    $totalDonor = App\Models\Donation::where('billplz_status', 'paid')
                        ->whereIn('campaign_id', $campaignIds)
                        ->whereBetween('created_at', [new MongoDB\BSON\UTCDateTime(strtotime("{$year}-01-01 00:00:00") * 1000), new MongoDB\BSON\UTCDateTime(strtotime("{$year}-12-31 23:59:59") * 1000)])
                        ->sum('amount');
                    
                    $total_net_donation = number_format($totalDonor, 2) - number_format($totalDonor * 0.06, 2);
                @endphp

                data.push({{ $total_net_donation }});
            @endif
        @endforeach

        var chartData = {
            labels: getYear,
            datasets: [{
                label: "Total Sum",
                backgroundColor: "rgba(75, 192, 192, 0.2)",
                borderColor: "rgba(75, 192, 192, 1)",
                borderWidth: 1,
                data: data
            }],
        };

        // Your bar chart options remain unchanged
        var options = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true,
                },
            },
        };

        // Get the canvas element
        var ctx = document.getElementById("myBarChart").getContext("2d");

        // Create the bar chart
        var myBarChart = new Chart(ctx, {
            type: "bar",
            data: chartData,
            options: options,
        });
    </script>
@endsection
