@extends('layouts.admin')

@section('title')
    Campaign
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">New Campaign</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12 mb-4">
            <div class="card border-left-primary shadow mb-4">
                <div class="card-body">

                    <form action="{{ url('store-campaign') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row-fluid g-2 mb-3">
                            <div class="col-md-6">
                                <label class="text-dark" for="campaign_name">Campaign Name</label>
                                <input name="campaign_name" type="text" class="form-control" required>
                            </div>

                            <div class="col-md-6">
                                <label class="text-dark" for="campaign_category">Choose Category</label>
                                <select name="campaign_category" class="form-select">
                                    <option disabled selected>Please Select</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->_id }}">{{ $category->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row-fluid mb-3">
                            <div class="col-md-4">
                                <label class="text-dark" for="total_donation">Total Donation</label>
                                <input name="total_donation" type="text" class="form-control"
                                    oninput="validateNumber(this)" required>
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark" for="start_date">Start Date</label>
                                <input type="date" class="form-control" name="start_date">
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark" for="end_date">End Date</label>
                                <input type="date" class="form-control" name="end_date">
                            </div>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label class="text-dark" for="description">Description</label>
                            <textarea name="campaign_description" class="ckeditor form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>

                        <div class="row-fluid mb-3">

                            @if (Auth::user()->role == 'Admin')
                                <div class="col-md-4">
                                    <label class="text-dark" for="ngo">Choose NGO</label>
                                    <select class="form-select" name="company_id" required>
                                        <option disabled selected>Please Select</option>
                                        @foreach ($company as $item)
                                            <option value="{{ $item->_id }}">{{ $item->company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif

                            <div class="col-md-4">
                                <label class="text-dark" for="image">Upload Image</label>
                                <br>
                                <input class="pt-1" name="image" type="file" id="formFile">
                            </div>

                            <div class="col-md-4">
                                <p class="text-dark" for="flag_kilat">Jariah Kilat</p>
                                {{-- <input name="flag_kilat" type="text" class="form-control" value="{{ $campaign->flag_kilat }}" required> --}}
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="flag_kilat" id="inlineRadio1"
                                        value="1">
                                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="flag_kilat" id="inlineRadio2"
                                        value="0">
                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 text-right py-3">
                            <button type="submit" class="btn btn-sm btn-primary">
                                Create Campaign
                            </button>
                        </div>

                    </form>

                </div>
            </div>
        </div>

    </div>
    <script>
        function validateNumber(input) {
            // Remove any non-numeric characters from the input
            input.value = input.value.replace(/\D/g, '');

            // Limit the input to a maximum of 11 digits
            if (input.value.length > 11) {
                input.value = input.value.slice(10, 11);
            }
        }
    </script>
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
    </script>
@endsection
