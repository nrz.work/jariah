@extends('layouts.admin')

@section('title')
    Campaign
@endsection

<style>
    /* timeline perkembangan */
    ul.timeline {
        list-style-type: none;
        position: relative;
        z-index: 0;
    }

    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 7px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }

    ul.timeline>li {
        margin: 20px 0;
        padding: 20px;
        background-color: #e9ecef;
        border-radius: 5px;
    }

    ul.timeline>li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #1D65BA;
        left: 0px;
        width: 15px;
        height: 15px;
        z-index: 400;
    }
</style>

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Campaign Progress</h1>
        <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal"
            data-target="#newProgress"><i class="fa fa-plus" aria-hidden="true"></i> New Progress</button>
        <!-- Modal -->
        <div class="modal fade" id="newProgress" tabindex="-1" aria-labelledby="newProgressLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newProgressLabel">New Progress</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ url('store-progress') }}/{{ $campaign->_id }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">

                            <div class="row-fluid mb-3">
                                <label class="text-dark" for="image">Upload Image</label>
                                <input class="custom-file pt-1" name="image" type="file">
                            </div>

                            <div class="row-fluid mb-3">
                                <label class="text-dark">Description</label>
                                <textarea name="description" class="ckeditor form-control" rows="5"></textarea>
                                <small><i>Keep us informed about the progress of your campaign so that everyone has clear
                                        information on your campaign.</i></small>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('addprogress'))
        <div class="alert alert-success alert-block">
            <strong>{{ $message }}</strong>
        </div>
    @endif

    @if ($message = Session::get('deleteprogress'))
        <div class="alert alert-danger alert-block">
            <strong>{{ $message }}</strong>
        </div>
    @endif

       @if ($message = Session::get('successupdate'))
        <div class="alert alert-success alert-block">
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-body">

                    <div class="float-right pt-3">{{ $progress->links() }}</div>
                    @if (count($progress) > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="table-secondary text-dark">
                                        <th scope="col">#</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Image</th>
                                        <th scope="col"><i class="fa fa-cogs" aria-hidden="true"></i></th>
                                    </tr>
                                </thead>
                                <tbody class="table-group-divider">
                                    @foreach ($progress as $key => $prog)
                                        <tr>
                                            <th scope="row">{{ $progress->firstItem() + $key }}</th>
                                            <td>{!! $prog->description !!}</td>
                                            <td><img src="{{ $prog->image }}" width="100rem" alt="progress"></td>
                                            <td>
                                                <button type="button" class="btn btn-outline-info" data-toggle="modal"
                                                    data-target="#updateProgress{{ $prog->_id }}">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </button>
                                                <!-- Update Modal -->
                                                <div class="modal fade" id="updateProgress{{ $prog->_id }}"
                                                    tabindex="-1" aria-labelledby="updateProgress" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="newProgressLabel">Update
                                                                    Progress</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{ url('edit-progress') }}/{{ $prog->_id }}"
                                                                method="POST" enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="modal-body">

                                                                    <div class="row-fluid mb-3">
                                                                        <label class="text-dark" for="image">Upload
                                                                            Image</label>
                                                                        <input class="custom-file pt-1" name="image"
                                                                            type="file">
                                                                    </div>

                                                                    <div class="row-fluid mb-3">
                                                                        <label class="text-dark">Description</label>
                                                                        <textarea name="description" class="ckeditor form-control" rows="5">{{ $prog->description }}</textarea>
                                                                        <small><i>Keep us informed about the progress of
                                                                                your campaign so that everyone has clear
                                                                                information on your campaign.</i></small>
                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close</button>
                                                                    <button type="submit"
                                                                        class="btn btn-primary">Submit</a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="button" class="btn btn-outline-danger" data-toggle="modal"
                                                    data-target="#deleteProgress{{ $prog->_id }}">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                                <!-- Delete Modal -->
                                                <div class="modal fade" id="deleteProgress{{ $prog->_id }}"
                                                    tabindex="-1" aria-labelledby="deleteProgress" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Delete Progress</h5>
                                                                <button type="button" class="close"
                                                                    data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Are you sure you want to delete this progress ?</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <a class="btn btn-danger"
                                                                    href="{{ url('delete-progress') }}/{{ $prog->_id }}">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <p class="text-center">There is no progress to display.</p>
                    @endif

                </div>
            </div>
        </div>

    </div>

    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
    </script>
@endsection
