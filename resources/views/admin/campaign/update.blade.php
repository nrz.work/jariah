@extends('layouts.admin')

@section('title')
    Campaign
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Update Campaign</h1>
    </div>

    @if ($message = Session::get('updatecampaign'))
        <div class="alert alert-success alert-block">
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12 mb-4">
            <div class="card border-left-primary shadow mb-4">
                <div class="card-body">
                    <form action="{{ url('edit-campaign') }}/{{ $campaign->id }}" method="POST" id="dynamic_form"
                        enctype="multipart/form-data">
                        @csrf

                        <div class="row-fluid g-2 mb-3">
                            <div class="col-md-6">
                                <label class="text-dark" for="campaign_name">Campaign Name</label>
                                <input name="campaign_name" type="text" class="form-control"
                                    value="{{ $campaign->campaign_name }}" required>
                            </div>

                            <div class="col-md-6">
                                <label class="text-dark" for="campaign_category">Category</label>
                                <select name="campaign_category" class="form-select">
                                    <option disabled>Current category : {{ $category->category_name }}</option>
                                    @foreach ($categories as $cat)
                                        <option value="{{ $cat->_id }}">{{ $cat->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row-fluid mb-3">
                            <div class="col-md-4">
                                <label class="text-dark" for="total_donation">Total Donation</label>
                                <input name="total_donation" type="text" class="form-control"
                                    value="{{ $campaign->total_donation }}" required>
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark" for="start_date">Start Date</label>
                                <input type="date" class="form-control" name="start_date"
                                    value="{{ $campaign->start_date }}">
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark" for="end_date">End Date</label>
                                <input type="date" class="form-control" name="end_date"
                                    value="{{ $campaign->end_date }}">
                            </div>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label class="text-dark" for="description">Description</label>
                            <textarea name="campaign_description" class="ckeditor form-control" id="exampleFormControlTextarea1" rows="3">{{ $campaign->campaign_description }}</textarea>
                        </div>

                        <div class="row-fluid mb-3">

                            <div class="col-md-4">
                                <label class="text-dark" for="image">Upload Image</label>
                                <br>
                                <input class="pt-1" name="image" type="file" id="formFile">
                            </div>

                            <div class="col-md-8">
                                <p class="text-dark" for="flag_kilat">Jariah Kilat</p>
                                {{-- <input name="flag_kilat" type="text" class="form-control" value="{{ $campaign->flag_kilat }}" required> --}}
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="flag_kilat" id="inlineRadio1"
                                        value="1" {{ $campaign->flag_kilat == 1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="flag_kilat" id="inlineRadio2"
                                        value="0" {{ $campaign->flag_kilat == 0 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 text-right py-3">
                            <button type="submit" class="btn btn-sm btn-primary">
                                Update Campaign
                            </button>
                            <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-toggle="modal"
                                data-target="#deletecampaign{{ $campaign->_id }}">
                                Delete Campaign
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deletecampaign{{ $campaign->_id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ $campaign->campaign_name }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this campaign ?
                    <ul>
                        <li>Campaign Progress will be deleted.</li>
                        <li>All donation paid & not paid will deleted.</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <form action="{{ url('delete') }}/{{ $campaign->id }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
    </script>
@endsection
