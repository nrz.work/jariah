@extends('layouts.admin')

@section('title')
Statistic
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ $campaign['campaign_name'] }}</h1>
</div>

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <!-- Overall Transaction -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Overall Transaction</h6>
            </div>
            <div class="card-body">
                <div class="row text-center">
                    <div class="col-md-3 mb-3">
                        <p>Total Transaction</p>
                        <h2 class="text-success">1</h2>
                    </div>
                    <div class="col-md-3 mb-3">
                        <p>Total Donation</p>
                        <h2 class="text-warning">RM{{ number_format(100, 2, '.', ',') }}</h2>
                    </div>
                    <div class="col-md-3 mb-3">
                        <p>Total Donation (Net)</p>
                        <h2 class="text-primary">RM{{ number_format(94, 2, '.', ',') }}</h2>
                    </div>
                    <div class="col-md-3 mb-3">
                        <p>Total Service Charge</p>
                        <h2 class="text-info">RM{{ number_format(6, 2, '.', ',') }}</h2>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</div>

<!-- Content Row -->
<div class="row">

    <!-- Area Chart -->
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Total Transaction (Donor)</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                        aria-labelledby="dropdownMenuLink">
                        <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- Area Chart -->
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Total Donation (RM)</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                        aria-labelledby="dropdownMenuLink">
                        <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myBarChart"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Bar Chart --}}

<!-- Include Chart.js library -->
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<!-- Your JavaScript code to create the bar chart -->
<script>
  // Sample data for the bar chart
  var data = {
    labels: ["January", "February", "March", "April", "May"],
    datasets: [{
      label: "Sales",
      backgroundColor: "rgba(75, 192, 192, 0.2)",
      borderColor: "rgba(75, 192, 192, 1)",
      borderWidth: 1,
      data: [65, 59, 80, 81, 56],
    }],
  };

  // Bar chart options
  var options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  };

  // Get the canvas element
  var ctx = document.getElementById("myBarChart").getContext("2d");

  // Create the bar chart
  var myBarChart = new Chart(ctx, {
    type: "bar",
    data: data,
    options: options,
  });
</script>
@endsection