@extends('layouts.admin')

@section('title')
    Campaign
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-12">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Campaign</h1>
                <a href="{{ url('new-campaign') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                    <i class="fas fa-plus fa-sm text-white-50"></i> New Campaign
                </a>
            </div>
        </div>
        <div class="col-md-12">
            @if ($message = Session::get('addcampaign'))
                <div class="alert alert-info alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('deletecampaign'))
                <div class="alert alert-danger alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('updateparty'))
                <div class="alert alert-info alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 mb-4">
            {{-- <div class="card-body"> --}}
                <div class="float-right pt-3">{{ $campaign->links() }}</div>
                @if (count($campaign) > 0)
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="table-secondary text-dark">
                                    <th scope="col">#</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Total Target</th>
                                    {{-- <th scope="col">Total Net</th> --}}
                                    <th scope="col">Status</th>
                                    <th scope="col">End Date</th>
                                    <th scope="col"><i class="fa fa-cogs" aria-hidden="true"></i></th>
                                </tr>
                            </thead>
                            <tbody class="table-group-divider">
                                @foreach ($campaign as $key => $campaigns)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>
                                            <a href="{{ url('update-campaign') }}/{{ $campaigns->id }}"
                                                class="text-capitalize">
                                                {{ $campaigns->campaign_name }}
                                                @if ($campaigns->flag_kilat == 1)
                                                    <i class="fa fa-bolt text-warning" aria-hidden="true"></i>
                                                @else
                                                @endif
                                            </a>
                                            <p class="mb-0">{{ $campaigns->company->company_name }}</p>
                                            <p>
                                                @if ($campaigns->third_party_id !== null)
                                                    @foreach ($companies as $key => $company)
                                                        @if ($campaigns->third_party_id == $company->_id)
                                                            Third Party : {{ $company->company_name }}
                                                        @endif
                                                    @endforeach
                                                @else
                                                @endif
                                            </p>
                                        </td>
                                        <td>
                                            RM{{ number_format($campaigns->total_donation, 2, '.', ',') }}
                                        </td>
                                        {{-- <td>RM{{ number_format($campaigns->total_donation, 2, '.', ',') }}</td> --}}
                                        <td>
                                            <span class="badge-pill badge-success text-xs">Active</span>
                                        </td>
                                        <td>{{ date('d M Y', strtotime($campaigns->end_date)) }}</td>
                                        <td>
                                            <a href="{{ url('statistic-by-campaign') }}/{{ $campaigns->_id }}" class="btn btn-sm btn-outline-primary mb-1"
                                                style="width: 90px">Statistic</a>
                                            <br>
                                            <a href="{{ url('campaign-progress') }}/{{ $campaigns->_id }}"
                                                class="btn btn-sm btn-outline-success mb-1" style="width: 90px">Progress</a>
                                            <br>
                                            {{-- <a href="" class="btn btn-sm btn-outline-warning mb-1" style="width: 90px">Assign Third Party</a> --}}
                                            <button type="button" class="btn btn-sm btn-outline-warning"
                                                data-toggle="modal" data-target="#exampleModal{{ $campaigns->_id }}"
                                                style="width: 90px">Third Party</button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal{{ $campaigns->_id }}" tabindex="-1"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Assign Third
                                                                Party</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{ url('assign-party') }}/{{ $campaigns->_id }}"
                                                            method="post">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <label class="text-dark"
                                                                    for="campaign_name">Organization</label>
                                                                <select name="third_party_id" class="form-select">
                                                                    @foreach ($companies as $company)
                                                                        <option value="{{ $company->_id }}">
                                                                            {{ $company->company_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Submit</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <p class="text-center">There is no campaign to display</p>
                @endif
            {{-- </div> --}}
        </div>
    </div>
@endsection
