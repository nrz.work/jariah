@extends('layouts.admin')

@section('title')
    Donation
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Donation</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12">

            <!-- Campaign Lists -->
            <div class="card shadow mb-4">
                <div class="card-body">

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <em class="text-xs d-block">i. Total amount of donations collected.</em>
                            <em class="text-xs d-block">ii. Total amount of donation (net) is the amount after deduction of
                                service charge.</em>
                        </div>
                    </div>
                    
                    <form action="{{ url('donation') }}" method="GET">
                        @csrf
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" placeholder="Search Reference No." name="billplz_id" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-info btn-block">Search</button>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="table-secondary text-dark">
                                            <th scope="col">Start Date</th>
                                            <th scope="col">Reference No.</th>
                                            <th scope="col">Campaign</th>
                                            <th scope="col">Total Amount</th>
                                            <th scope="col">Service Charge</th>
                                            <th scope="col">Total Amount of Donation (Net)</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-group-divider">
                                        @foreach ($donation as $item)
                                            <tr class="text-center">
                                                <td>{{ date('d M Y', strtotime($item->created_at)) }}</td>
                                                <td>{{ $item->billplz_id }}</td>
                                                <td>{{ $item->campaign->campaign_name }}</td>
                                                <td>RM{{ number_format($item->amount, 2) }}</td>
                                                <td>6%</td>
                                                <td>RM{{ number_format($item->amount, 2) - number_format($item->amount, 2) * 0.06 }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex justify-content-center">
                                {{ $donation->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
