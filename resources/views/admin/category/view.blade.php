@extends('layouts.admin')

@section('title')
Category
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List of Categories</h1>
    {{-- <a href="/new-ngo" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-plus fa-sm text-white-50"></i> New Category
    </a> --}}
    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#newCategory"><i class="fa fa-plus" aria-hidden="true"></i> New Category</button>
    <!-- Modal -->
    <div class="modal fade" id="newCategory" tabindex="-1" aria-labelledby="newCategoryLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="newCategoryLabel">New Category</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form action="" method="post">
                @csrf
                <div class="modal-body">


                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit"  class="btn btn-primary">Submit</a>
                </div>
            </form>
        </div>
        </div>
    </div>
</div>

@if ($message = Session::get('add'))
<div class="alert alert-info alert-block">
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('delete'))
<div class="alert alert-danger alert-block">
    <strong>{{ $message }}</strong>
</div>
@endif

<!-- Content Row -->
<div class="row">

    <div class="col-md-12">

        <div class="card shadow mb-4">
            <div class="card-body">

                <div class="float-right pt-3">{{$categories->links()}}</div>
                @if(count($categories) > 0)
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                          <tr class="table-secondary text-dark">
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col"><i class="fa fa-cogs"></i></th>
                          </tr>
                        </thead>
                        <tbody class="table-group-divider">
                            @foreach ($categories as $key => $category)
                            <tr>
                                <th scope="row">{{ $categories->firstItem() + $key  }}</th>
                                <td>
                                    <a href="{{ url('update_category') }}/{{ $category->_id }}">
                                        {{ $category->category_name }}
                                    </a>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal{{ $category->_id }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal{{ $category->_id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            Are you sure you want to delete this category ?
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <a class="btn btn-danger" href="{{ url('delete') }}/{{ $category->_id }}">Delete</a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                    <p class="text-center">There is no NGO to be displayed.</p>
                @endif

            </div>
        </div>

    </div>

</div>

@endsection