@extends('layouts.admin')

@section('title')
Dashboard
@endsection

@section('content')
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>

<!-- Content Row -->
<div class="row">

    <!-- Donation -->
    <div class="col-md-4 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1">
                            Total Donation (Monthly)
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><td>{{ number_format($total_donation) }}</div>
                        <div class="text-xs text-gray-800 mb-1">
                            <a href="{{ url('donation') }}">
                                More >></i>
                            </a> 
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-donate fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Total Net -->
    <div class="col-md-4 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1">
                            Total Net (Monthly)
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><td>RM{{ $net_total }}</div>
                    </div>
                    <div class="col-auto mt-3">
                        <i class="fas fa-hand-holding-usd fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Total Campaigns -->
    <div class="col-md-4 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1">
                            Total Campaigns
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ number_format($count_campaigns) }}</div>
                        <div class="text-xs text-gray-800 mb-1">
                            <a href="/view-campaign">
                                More >></i>
                            </a> 
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-bullhorn fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
                      
</div>

@endsection