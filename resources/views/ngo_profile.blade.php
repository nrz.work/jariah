@extends('layouts.app')

@section('title')
    Kempen
@endsection

<style>
    /* read tentang penganjur */
    .dynamic-content {
        overflow: hidden;
        max-height: 80px;
        /* Adjust this value based on your design */
        transition: max-height 0.3s ease-out;
    }

    .dynamic-content.expanded {
        max-height: none;
    }

    .see-more-button {
        color: #4E4DE7;
    }
</style>

@section('content')


    <div class="row px-4">
        <div class="col-md-12 border-bottom pb-4">
            <div class="text-center pb-2">
                <img class="border rounded-2 p-2" width="20%" src="{{ $company->company_profile }}" alt="logo">
            </div>
            <div class="text-center">
                <h4 class="font-500">{{ $company->company_name }}</h4>
            </div>
        </div>

        <div class="col-md-12 border-bottom py-4">
            <p class="text-dark font-600 mb-0">Tentang Penganjur</p>
            <p class="dynamic-content mb-0" style="text-align: justify">{!! $company['company_details'] !!}</p>
            <a href="#" class="see-more-button" onclick="toggleDynamicContent()">... <br> Papar semua <i
                    class="fa fa-chevron-down" aria-hidden="true"></i></a>
        </div>

        <div class="col-md-12 border-bottom py-4">
            <h6 class="font-500 pb-2">Maklumat Penganjur</h6>

            <label class="text-dark font-600 mb-0">Alamat</label>
            <p class="mb-2">{{ $company['address'] ?: 'Tiada Maklumat' }}</p>

            <label class="text-dark font-600 mb-0">Emel</label>
            <p class="mb-2">{{ $company['company_email'] ?: 'Tiada Maklumat' }}</p>

            <label class="text-dark font-600 mb-0">Hubungi</label>
            <p class="mb-0">{{ $company['company_phone'] ?: 'Tiada Maklumat' }}</p>
        </div>

        <div class="col-md-12 py-4">
            <p class="text-dark font-600 mb-0">Semua Kempen Dari Penganjur</p>

            {{-- Tab --}}
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="nav-link fs-6 active" id="active-tab" data-bs-toggle="tab" data-bs-target="#nav-active"
                        type="button" role="tab" aria-controls="nav-active" aria-selected="true">Kempen Aktif</button>
                    <button class="nav-link fs-6" id="expired-tab" data-bs-toggle="tab" data-bs-target="#nav-expired"
                        type="button" role="tab" aria-controls="nav-expired" aria-selected="false">Kempen
                        Tamat</button>
                </div>
            </nav>
            {{-- End Tab --}}

            <div class="tab-content" id="nav-tabContent">

                {{-- Active Campaign --}}
                <div class="tab-pane fade show active" id="nav-active" role="tabpanel" aria-labelledby="active-tab">
                    <div class="row mb-0">
                    @if (count($active) > 0)
                        @foreach ($active as $kempen_aktif)
                            {{-- @dd() --}}
                            <div class="col-6 g-0">
                                <div class="card">
                                    <a href="{{ url('campaign/show') }}/{{ $kempen_aktif['slug'] }}">
                                        <img src="{{ $kempen_aktif['image'] }}" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <p class="card-text text-dark two-line-truncate line-height-s font-500">
                                                {{ $kempen_aktif['campaign_name'] }}</p>
                                            <div class="progress" role="progressbar" aria-label="Basic example"
                                                aria-valuenow="{{ $kempen_aktif['amount'] }}" aria-valuemin="0"
                                                aria-valuemax="{{ $kempen_aktif['total_donation'] }}">
                                                <div class="progress-bar"
                                                    style="width: {{ ($kempen_aktif['amount'] / $kempen_aktif['total_donation']) * 100 }}%">
                                                </div>
                                            </div>
                                            <div class="row mb-0">
                                                <div class="col-7">
                                                    <span class="text-dark mb-2"><b>RM{{ number_format($kempen_aktif->donation()->sum('amount')) }}
                                                        </b></span><span class="text-secondary"> /
                                                        RM{{ number_format($kempen_aktif['total_donation']) }}</span>
                                                </div>
                                                <div class="col-5 text-end">
                                                    <span
                                                        class="text-dark font-11 font-500">{{ number_format(($kempen_aktif->donation()->sum('amount') / $kempen_aktif['total_donation']) * 100, 2) }}%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer p-0">
                                            <p class="color-blue-dark text-truncate p-1 mb-0" style="max-width: 200px">
                                                {{-- <img class="px-2" src="{{ asset('frontend/img/yayasan digital.png') }}"
                                                    alt="logo"> --}}
                                                <img class="px-2" width="18%"
                                                    src="{{ $kempen_aktif->company->company_profile }}" alt="logo">
                                                {{ $kempen_aktif->company->company_name }}
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="container d-flex justify-content-center align-items-center" style="height: 70vh;">
                            <p class="text-center" style="word-wrap: break-word; padding: 0 20px;">
                                Tiada kempen yang aktif daripada organisasi ini.
                            </p>
                        </div>
                    @endif
                    </div>
                </div>
                {{-- End Active Campaign --}}

                {{-- Expired Campaign --}}
                <div class="tab-pane fade" id="nav-expired" role="tabpanel" aria-labelledby="expired-tab">
                    <div class="row mb-0">
                    @if (count($expired) > 0)
                        @foreach ($expired as $campaign)
                            {{-- @dd() --}}
                            <div class="col-6 g-0">
                                <div class="card">
                                    <a href="{{ url('campaign/show') }}/{{ $campaign['slug'] }}">
                                        <img src="{{ $campaign['image'] }}" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <p class="card-text text-dark two-line-truncate line-height-s font-500">
                                                {{ $campaign['campaign_name'] }}</p>
                                            <div class="progress" role="progressbar" aria-label="Basic example"
                                                aria-valuenow="{{ $campaign['amount'] }}" aria-valuemin="0"
                                                aria-valuemax="{{ $campaign['total_donation'] }}">
                                                <div class="progress-bar"
                                                    style="width: {{ ($campaign['amount'] / $campaign['total_donation']) * 100 }}%">
                                                </div>
                                            </div>
                                            <div class="row mb-0">
                                                <div class="col-7">
                                                    <span class="text-dark mb-2"><b>RM{{ number_format($campaign->donation()->sum('amount')) }}
                                                        </b></span><span class="text-secondary"> /
                                                        RM{{ number_format($campaign['total_donation']) }}</span>
                                                </div>
                                                <div class="col-5 text-end">
                                                    <span
                                                        class="text-dark font-11 font-500">{{ number_format(($campaign->donation()->sum('amount') / $campaign['total_donation']) * 100, 2) }}%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer p-0">
                                            <p class="color-blue-dark text-truncate p-1 mb-0" style="max-width: 200px">
                                                {{-- <img class="px-2" src="{{ asset('frontend/img/yayasan digital.png') }}"
                                                    alt="logo"> --}}
                                                <img class="px-2" width="18%"
                                                    src="{{ $campaign->company->company_profile }}" alt="logo">
                                                {{ $campaign->company->company_name }}
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="container d-flex justify-content-center align-items-center" style="height: 70vh;">
                            <p class="text-center" style="word-wrap: break-word; padding: 0 20px;">
                                Tiada kempen yang telah tamat daripada organisasi ini.
                            </p>
                        </div>
                    @endif
                    </div>
                </div>
                {{-- End Expired Campaign --}}
            </div>

        </div>
    </div>

    {{-- Tentang Penganjur --}}
    <script>
        function toggleDynamicContent() {
            var dynamicContent = document.querySelector(".dynamic-content");
            var btnText = document.querySelector(".see-more-button");
            var ellipsis = dynamicContent.querySelector(".ellipsis");

            if (dynamicContent.classList.contains("expanded")) {
                dynamicContent.classList.remove("expanded");
                dynamicContent.classList.add("collapsed");
                btnText.innerHTML = "... <br> Papar semua <i class='fa fa-chevron-down'></i>";
            } else {
                dynamicContent.classList.remove("collapsed");
                dynamicContent.classList.add("expanded");
                btnText.innerHTML = "Tutup";
            }
        }
    </script>

@endsection
