@extends('layouts.app')

@section('title')
    Kempen
@endsection

@section('content')
    {{-- Kempen --}}
    <div class="row">

        @if (count($list_campaign) > 0)
            <div class="col-md-12 px-4">
                <h4 class="color-blue-dark font-500">Carian Ditemui</h4>
            </div>

            @foreach ($list_campaign as $campaign)
                <div class="card card-style shadow-none m-0 px-2">
                    <div class="content mb-0">
                        <div class="row">
                            <div class="col-4 pe-3">
                                <a href="{{ url('campaign/show') }}/{{ $campaign->slug }}">
                                    <img src="{{ $campaign->image }}" class="rounded-s" width="100%">
                                </a>
                            </div>

                            <div class="col-8">
                                <div class="row mb-1">
                                    <div class="col-10">
                                        <p class="color-black two-line-truncate line-height-s font-500">
                                            {{ $campaign['campaign_name'] }}</p>
                                    </div>
                                    {{-- <div class="col-2 text-end">
                                            <i onclick="myFunction(this)" class="fa fa-heart fa-2x color-blue-dark"></i>
                                        </div> --}}
                                </div>

                                {{-- <a href="{{ url('campaign') }}/{{ $campaign['_id'] }}"> --}}
                                <a href="{{ url('campaign/show') }}/{{ $campaign['slug'] }}">
                                    <div class="progress" role="progressbar" aria-label="Basic example"
                                        aria-valuenow="{{ $campaign['amount'] }}" aria-valuemin="0"
                                        aria-valuemax="{{ $campaign['total_donation'] }}">
                                        <div class="progress-bar"
                                            style="width: {{ ($campaign['amount'] / $campaign['total_donation']) * 100 }}%">
                                        </div>
                                    </div>
                                    <div class="row mb-0">
                                        <div class="col-8">
                                            <span class="color-blue-dark mb-2"><b>RM{{ number_format($campaign->donation()->sum('amount')) }}
                                                </b> / RM{{ number_format($campaign['total_donation']) }}</span>
                                        </div>
                                        <div class="col-4 text-end">
                                            <span
                                                class="color-blue-dark font-500 mb-2">{{ number_format(($campaign->donation()->sum('amount') / $campaign['total_donation']) * 100, 2) }}%</span>
                                        </div>
                                    </div>
                                    <div class="row my-0 mt-2">
                                        <div class="col-8">
                                            <img src="{{ asset('frontend/img/yayasan digital.png') }}" alt="logo">
                                            <span class="color-blue-dark">{{ $campaign['company']['company_name'] }}</span>
                                        </div>
                                        {{-- <div class="col-4 text-end">
                                            <span class="color-blue-dark font-9">{{ $campaign['count_days'] }} Hari
                                                lagi <i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                        </div> --}}
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="container d-flex justify-content-center align-items-center" style="height: 70vh;">
                <p class="text-center" style="word-wrap: break-word; padding: 0 20px;">
                    Maaf, kami tidak jumpa sebarang tajuk yang sedang anda cari.
                </p>
            </div>
        @endif

    </div>
    {{-- End Kempen --}}
@endsection
