@extends('layouts.app')

@section('og:image')
    <meta property="og:image" content="{{ $campaign['image'] }}">
@endsection

@section('title')
    Kempen
@endsection

@section('styles')
    <style>
        /* sticky button */
        div.sticky {
            position: -webkit-sticky;
            position: sticky;
            bottom: 0;
            padding-bottom: 70px;
            font-size: 20px;
        }

        /* read more */
        /* .truncate-text {
                                overflow: hidden;
                                text-overflow: ellipsis;
                                display: -webkit-box;
                                -webkit-line-clamp: 3;
                                -webkit-box-orient: vertical;
                            }

                            .expanded-text {
                                -webkit-line-clamp: initial;
                            } */

        /* tentang kempen */
        .long-text-content {
            overflow: hidden;
            max-height: 100px;
            /* Adjust as needed */
            transition: max-height 0.3s ease-in-out;
        }

        .long-text-expanded {
            max-height: none !important;
        }

        .see-more-link {
            display: inline-block;
            margin-top: 10px;
            cursor: pointer;
        }
    </style>
    {{-- <style>
    /* Style the button */
    .fixed-button {
        position: fixed;
        bottom: 5rem;
        width: 35%;
        text-align: center;
        z-index: 999;
    }
</style> --}}
@endsection

{{-- @dd($count_days) --}}
@section('content')
    <div class="row mb-3">
        <img class="img-fluid" src="{{ $campaign['image'] }}" alt="">
    </div>

    {{-- @dd($campaign) --}}

    <div class="row-fluid px-3">
        <div class="col-md-12 mb-1">
            <h3 class="font-500 mt-0">{{ $campaign['campaign_name'] }}</h3>
        </div>

        <div class="col-md-12 mt-3">
            <div class="row mb-2">
                <div class="col text-start text-truncate">
                    <a href="{{ url('campaign/organization-profile') }}/{{ $campaign['company_id'] }}"
                        class="color-blue-dark">
                        <img width="10%" src="{{ $company->company_profile }}" alt="logo">
                        &nbsp; {{ $company->company_name }}
                    </a>
                </div>
                <div class="col text-end">
                    <a href="https://wa.me/?text={{ urlencode('e-Jariah Campaign: ' . url('campaign/show/' . $campaign['slug'])) }}"
                        class="px-1"><img src="{{ asset('frontend/img/whatsapp_logo.png') }}" width="15%"
                            alt="whatsapp_link"></a>
                    <a href="https://twitter.com/intent/tweet/?url={{ url('campaign/show/' . $campaign['slug']) }}"
                        class="px-1"><img src="{{ asset('frontend/img/twitter_logo.png') }}" width="15%"
                            alt="twitter_link"></a>
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('campaign/show') }}/{{ $campaign['slug'] }}"
                        class="px-1"><img src="{{ asset('frontend/img/facebook_logo.png') }}" width="15%"
                            alt="facebook_link"></a>
                    <a href="#" id="copyLink" data-clipboard-text="{{ url('campaign/show/' . $campaign['slug']) }}">
                        <img src="{{ asset('frontend/img/link icon.png') }}" width="15%" alt="share_link">
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-8">
                    <p class="mb-0"><b>Jumlah Terkumpul</b></p>
                </div>
                <div class="progress" role="progressbar" aria-label="Basic example"
                    aria-valuenow="{{ $totalDonationAmount }}" aria-valuemin="0"
                    aria-valuemax="{{ $campaign['total_donation'] }}">
                    <div class="progress-bar"
                        style="width: {{ ($totalDonationAmount / $campaign['total_donation']) * 100 }}%">
                    </div>
                </div>
                <div class="row mb-0 pt-2">
                    <div class="col-12 text-end">
                        <p class="mb-0"><b class="text-dark">RM {{ $totalDonationAmount ?? '0' }}</b> /
                            RM{{ number_format($campaign['total_donation']) }}</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12 mb-4">
            <p class="font-700 mb-0">Tentang Kempen :</p>
            <div class="long-text-container">
                <div class="long-text-content mb-0">
                    {!! $campaign['campaign_description'] !!}
                </div>
                <a href="#" class="see-more-link">Baca lebih lanjut</a>
            </div>

        </div>

        <div class="col-md-12 mb-2">
            <div class="d-flex align-items-center justify-content-between">
                <div>
                    <h6>Perkembangan <span class="text-primary">Kempen</span></h6>
                    <p class="mb-0">Dapatkan maklumat terkini berkaitan kempen ini.</p>
                </div>
                <div>
                    <a href="{{ url('campaign/campaign-progress/') }}/{{ $campaign['_id'] }}" class="btn btn-sm btn-light"
                        style="width: 100px">Lihat</a>
                </div>
            </div>
        </div>


        <div class="col-md-12 mt-4" style="padding-bottom: 2rem">
            <h6 class="pb-2">Senarai <span class="text-primary">Jariah</span></h6>

            @if (count($donations) > 0)
                <div class="card m-0">
                    @foreach ($donations as $donation)
                        @foreach ($users as $user)
                            @if ($user->email == $donation->email)
                                <div class="card-body border-bottom">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div>
                                            <p class="text-dark mb-0">
                                                <b>
                                                    @if ($donation->agreement_name == 'Yes')
                                                        Hamba Allah
                                                    @else
                                                        @if ($user->email == $donation->email)
                                                            {{ $user->name }}
                                                        @else
                                                            {{ $donation->name }}
                                                        @endif
                                                    @endif
                                                </b>
                                            </p>
                                            <p class="mb-0">{{ $donation->created_at->format('d M Y') }}</p>
                                        </div>
                                        <div>
                                            <p class="mb-0">Amount <span
                                                    class="text-success">RM{{ $donation->amount }}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row mb-0">
                                        <div class="col-md-12">
                                            <p class="mb-0 dynamic-content">{{ $donation->doa }}</p>
                                            <button class="see-more-button text-primary">Papar semua</button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endforeach
                </div>
            @else
                <p class="text-center">Tiada sumbangan diterima.</p>
            @endif

        </div>

        <div class="col-md-12 mb-3 sticky text-center">
            <a href="{{ url('donate/show') }}/{{ $campaign['_id'] }}"
                class="btn btn-primary bg-blue font-500 rounded-2 w-100">BERI JARIAH</a>
        </div>
        {{-- <div class="fixed-button">
            <a href="{{ url('donate/show') }}/{{ $campaign['_id'] }}" class="btn btn-primary bg-blue font-500 rounded-2">BERI JARIAH</a>
        </div> --}}
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    {{-- tentang kempen --}}
    <script>
        $(document).ready(function() {
            $(".see-more-link").click(function(e) {
                e.preventDefault();

                var container = $(this).closest(".long-text-container");
                var content = container.find(".long-text-content");

                content.toggleClass("long-text-expanded");
                $(this).text(content.hasClass("long-text-expanded") ? "Tutup" : "Baca lebih lanjut");
            });
        });
    </script>

    {{-- doa --}}
    <script>
        $(document).ready(function() {
            $('.dynamic-content').each(function() {
                const content = $(this);
                const seeMoreButton = content.closest('.card-body').find('.see-more-button');

                if (content.text().length > 55) {
                    content.addClass('truncated').data('full-content', content.text());
                    content.text(content.text().substring(0, 55) + ' ...');
                    seeMoreButton.show();

                    seeMoreButton.click(function() {
                        if (content.hasClass('truncated')) {
                            content.removeClass('truncated').text(content.data('full-content'));
                            seeMoreButton.text('Tutup');
                        } else {
                            content.addClass('truncated').text(content.data('full-content')
                                .substring(0, 55) + ' ...');
                            seeMoreButton.text('Papar semua');
                        }
                    });
                } else {
                    seeMoreButton.hide();
                }
            });
        });
    </script>

    {{-- copy link to dashboard --}}
    <script>
        var clipboard = new ClipboardJS('#copyLink');

        clipboard.on('success', function(e) {
            e.clearSelection();
            alert('Link has been copied to clipboard: ' + e.text);
        });
    </script>

    <script>
        // JavaScript to handle button visibility
        var lastScrollY = 0;
        var button = document.querySelector('.fixed-button');

        window.addEventListener('scroll', function() {
            var currentScrollY = window.scrollY || window.pageYOffset;

            if (currentScrollY > lastScrollY) {
                // Scrolling down, hide the button
                button.style.transform = 'translateY(100%)';
            } else {
                // Scrolling up, show the button
                button.style.transform = 'none';
            }

            lastScrollY = currentScrollY;
        });
    </script>

@endsection
