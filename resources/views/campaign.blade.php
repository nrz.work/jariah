@extends('layouts.app')

@section('title')
    Kempen
@endsection
{{-- @dd($campaign) --}}
@section('content')

    {{-- Kempen --}}
    <div class="row">

        <div class="col-md-12 px-4">
            <h1 class="color-blue-dark font-500 font-18">Jariah Berterusan</h1>
        </div>

        <div class="card-row pt-1 px-4 mb-3">
            <div class="col-md-12">
                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                    <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off"
                        {{ request()->is('campaign') ? 'checked' : '' }}>
                    <label class="btn btn-xxs btn-outline-primary rounded-pill" for="btnradio1"
                        onclick="window.location.href='{{ url('campaign') }}'">Semua</label>

                    @foreach ($category as $cat)
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio{{ $cat->id }}"
                            autocomplete="off" {{ request()->is('campaign/f/' . $cat->id) ? 'checked' : '' }}>
                        <label class="btn btn-xxs btn-outline-primary rounded-pill" for="btnradio{{ $cat->id }}"
                            onclick="window.location.href='{{ url('campaign/f') }}/{{ $cat->id }}'">{{ $cat->category_name }}</label>
                    @endforeach
                </div>
            </div>
        </div>


        {{-- @if (is_iterable($campaign)) --}}
        @if (count($allCampaign) > 0)
            @foreach ($allCampaign as $campaign)
                <div class="card card-style shadow-none m-0 px-2">
                    <div class="content mb-0">
                        <div class="row">
                            <div class="col-4 pe-3">
                                {{-- <a href="{{ url('campaign/show') }}/{{ $campaign['_id'] }}"> --}}
                                <a href="{{ url('campaign/show') }}/{{ $campaign['slug'] }}">
                                    <img src="{{ $campaign['image'] }}" class="rounded-s" width="100%">
                                </a>
                            </div>

                            <div class="col-8">
                                <div class="row mb-1">
                                    <div class="col-10">
                                        <p class="color-black two-line-truncate line-height-s font-500">
                                            {{ $campaign['campaign_name'] }}</p>
                                    </div>
                                    {{-- <div class="col-2 text-end">
                                            <i onclick="myFunction(this)" class="fa fa-heart fa-2x color-blue-dark"></i>
                                        </div> --}}
                                </div>

                                {{-- <a href="{{ url('campaign') }}/{{ $campaign['_id'] }}"> --}}
                                <a href="{{ url('campaign/show') }}/{{ $campaign['slug'] }}">
                                    <div class="progress" role="progressbar" aria-label="Basic example"
                                        aria-valuenow="{{ $campaign['amount'] }}" aria-valuemin="0"
                                        aria-valuemax="{{ $campaign['total_donation'] }}">
                                        <div class="progress-bar"
                                            style="width: {{ ($campaign['amount'] / $campaign['total_donation']) * 100 }}%">
                                        </div>
                                    </div>
                                    <div class="row mb-0">
                                        <div class="col-8">
                                            <span class="color-blue-dark mb-2"><b>RM{{ number_format($campaign->donation()->sum('amount')) }}
                                                </b> / RM{{ number_format($campaign['total_donation']) }}</span>
                                        </div>
                                        <div class="col-4 text-end">
                                            <span
                                                class="color-blue-dark font-500 mb-2">{{ number_format(($campaign->donation()->sum('amount') / $campaign['total_donation']) * 100, 2) }}%</span>
                                        </div>
                                    </div>
                                    <div class="row my-0 mt-2">
                                        <div class="col-8">
                                            {{-- <img src="{{ asset('frontend/img/yayasan digital.png') }}" alt="logo"> --}}
                                            <img class="pe-1" width="30rem" src="{{ $campaign['company']['company_profile'] }}" alt="logo">
                                            <span class="color-blue-dark">{{ $campaign['company']['company_name'] }}</span>
                                        </div>
                                        {{-- <div class="col-4 text-end">
                                            <span class="color-blue-dark font-9">{{ $campaign['count_days'] }} Hari
                                                lagi <i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                        </div> --}}
                                    </div>
                                </a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="d-flex justify-content-center">
                {{ $allCampaign->links() }}
            </div>
        @else
            <div class="container d-flex justify-content-center align-items-center" style="height: 70vh;">
                <p class="text-center" style="word-wrap: break-word; padding: 0 20px;">
                    Maaf, tiada kempen yang aktif di dalam kategori ini sekarang.
                </p>
            </div>
        @endif
        {{-- @else
            {{ var_dump($campaign) }}
        @endif --}}

    </div>
    {{-- End Kempen --}}
@endsection
