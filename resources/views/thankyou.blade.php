@extends('layouts.app')

@section('title')
    Terima Kasih
@endsection

@section('content')
    {{-- Content --}}
    <div class="pt-2 px-4">

        <div class="row mb-0">
            <div class="col-md-12 text-center py-5">
                <h3>Pembayaran telah berjaya!</h3>
                <i class="fa fa-check-circle-o text-success py-5" style="font-size:150px" aria-hidden="true"></i>
                <p class="m-0">Terima kasih atas sumbangan anda.</p>
                <p class="m-0"><a href="">Hubungi kami</a> jika terdapat sebarang pertanyaan.</p>
            </div>
        </div>
        
    </div>
    {{-- End Content --}}
@endsection
