@extends('layouts.app')

@section('title')
    Kempen
@endsection

<style>
    /* timeline perkembangan */
    ul.timeline {
        list-style-type: none;
        position: relative;
        z-index: 0;
    }

    ul.timeline:before {
        content: ' ';
        background: #f6f6f6;
        display: inline-block;
        position: absolute;
        left: 7px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }

    ul.timeline>li {
        margin: 20px 0;
        padding: 20px;
        background-color: #f6f6f6;
        border-radius: 5px;
    }

    ul.timeline>li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #4E4DE7;
        left: 0px;
        width: 15px;
        height: 15px;
        z-index: 400;
    }
</style>

@section('content')
    <div class="row-fluid px-4">

        <a href="{{ url('campaign/show') }}/{{ $campaign['slug'] }}"><i class="fa fa-arrow-left" aria-hidden="true"></i>
            Kembali ke kempen</a>

        <h5 class="pt-3">Perkembangan <span class="text-primary">Kempen</span></h5>

        <div class="row pt-3 mb-0">
            <div class="col-md-12 border-left">

                <h6 class="font-500 mt-0 mb-2">{{ $campaign['campaign_name'] }}</h6>

                <span class="color-blue-dark">
                    {{-- @foreach ($company as $key => $item)
                        @if ($campaign->company_id == $item->_id)
                            <img src="{{ asset('frontend/img/yayasan digital.png') }}" alt="logo">
                            &nbsp; {{ $item['company_name'] }}
                        @endif
                    @endforeach --}}
                    <img class="pe-1" width="25rem" src="{{ $company->company_profile }}" alt="logo"> {{ $company['company_name'] }}
                </span>

            </div>
        </div>

        <div class="row mt-3">
            <div class="col-auto">
                <ul class="timeline">
                    @if (count($progress) > 0)
                        @foreach ($progress as $item)
                            <li class="text-dark">
                                <span class="font-10">{{ date('d M Y', strtotime($item['created_at'])) }}</span>
                                <p class="mb-0">{!! $item->description !!}</p>
                                <img class="img-fluid" src="{{ $item->image }}" alt="update_kempen">
                            </li>
                        @endforeach
                    @else
                        <li>
                            <p class="text-dark m-0">{{ date('d M Y', strtotime($campaign['created_at'])) }}</p>
                            <p class="mb-3">Kempen bermula</p>
                            <a href="{{ url('donate/show') }}/{{ $campaign['_id'] }}"
                                class="btn btn-sm btn-light border-0">Beri Sumbangan</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>

    </div>
@endsection
