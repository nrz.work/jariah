@extends('layouts.app')

@section('title')
    Utama
@endsection

@section('styles')
    <style>
        .carousel-indicators {
            position: absolute;
            bottom: 0;
            left: 0%;
            transform: translateY(115%);
            display: flex;
            justify-content: center;
            list-style: none;
        }

        .carousel-indicators li {
            width: 10px;
            height: 10px;
            background-color: #ccc;
            border-radius: 50%;
            margin: 0 5px;
            cursor: pointer;
        }

        .carousel-indicators li.active {
            background-color: #4E4DE7;
        }
    </style>
@endsection

@section('content')
    <div class="row ">
        {{-- Jariah Kilat --}}
        @if (count($campaigns_flag_kilat) > 0)
            <div class="row bg-primary-light p-3">
                {{-- carousel --}}
                <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($campaigns_flag_kilat as $index => $item)
                            <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                                <div class="card">
                                    <a href="{{ url('campaign/show') }}/{{ $item['slug'] }}">
                                        <img src="{{ $item->image }}" class="card-img-top" alt="{{ $item->campaign_name }}">
                                        <div class="card-body">
                                            <h6 class="card-text font-14 font-500 text-truncate" style="max-width: 450px;">
                                                {{ $item->campaign_name }}</h6>
                                            <span class="color-blue-dark d-inline-block">
                                                <img class="pe-1" width="22rem"
                                                    src="{{ $item->company->company_profile }}" alt="logo">
                                                {{ $item->company->company_name }}
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- Bullet indicators -->
                    <ol class="carousel-indicators">
                        @php
                            $count = 0; // Initialize the count variable outside the loop
                        @endphp
                        @foreach ($campaigns_flag_kilat as $index => $item)
                            <li data-bs-target="#myCarousel" data-bs-slide-to="{{ $count }}"
                                @if ($count === 0) class="active" @endif></li>
                            @php
                                $count++;
                            @endphp
                        @endforeach
                    </ol>
                </div>
                {{-- End carousel --}}
            </div>
        @endif
        {{-- End Jariah Kilat --}}

        {{-- Kategori --}}
        <div class="row mb-0">
            <div class="col-auto px-4 pb-4">
                <h1 class="text-dark font-500 font-18">Kategori</h1>
            </div>


            <div class="row text-center px-4 mb-0">
                <div class="col">
                    <a href="{{ url('campaign/f') }}/{{ $c1->_id }}" class="d-block">
                        <img src="{{ asset('backend/img/category_icon/kanak-kanak.png') }}" width="60rem"
                            alt="kanak-kanak">
                        <p class="">{{ $c1->category_name }}</p>
                    </a>
                </div>
                <div class="col">
                    <a href="{{ url('campaign/f') }}/{{ $c2->_id }}" class="d-block">
                        <img src="{{ asset('backend/img/category_icon/bencana alam.png') }}" width="60rem"
                            alt="bencana alam">
                        <p>{{ $c2->category_name }}</p>
                    </a>
                </div>
                <div class="col">
                    <a href="{{ url('campaign/f') }}/{{ $c3->_id }}" class="d-block">
                        <img src="{{ asset('backend/img/category_icon/baik pulih.png') }}" width="60rem" alt="baik pulih">
                        <p>{{ $c3->category_name }}</p>
                    </a>
                </div>
                <div class="col">
                    <a data-bs-toggle="collapse" href="#kategori-kempen" role="button" aria-expanded="false"
                        aria-controls="kategori-kempen">
                        <img src="{{ asset('backend/img/category_icon/lain-lain.png') }}" width="60rem" alt="lain-lain">
                        <p>Lain-lain</p>
                    </a>
                </div>
                <div class="collapse" id="kategori-kempen">
                    <div class="card-body p-0">
                        <div class="row mb-0">
                            {{-- <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c4->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/pusat tahfiz.png') }}" width="60rem"
                                        alt="">
                                    <p>Pusat Tahfiz</p>
                                </a>
                            </div> --}}
                            <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c5->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/haiwan.png') }}" width="60rem"
                                        alt="haiwan">
                                    <p>{{ $c5->category_name }}</p>
                                </a>
                            </div>
                            <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c6->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/oku.png') }}" width="60rem"
                                        alt="oku">
                                    <p>{{ $c6->category_name }}</p>
                                </a>
                            </div>
                            <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c7->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/kecemasan.png') }}" width="60rem"
                                        alt="kecemasan">
                                    <p>{{ $c7->category_name }}</p>
                                </a>
                            </div>
                            <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c9->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/kegiatan sosial.png') }}" width="60rem"
                                        alt="kegiatan sosial">
                                    <p>{{ $c9->category_name }}</p>
                                </a>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c8->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/rumah ibadah.png') }}" width="60rem"
                                        alt="rumah ibadah">
                                    <p>{{ $c8->category_name }}</p>
                                </a>
                            </div>
                            <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c10->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/bantuan kesihatan.png') }}"
                                        width="60rem" alt="bantuan kesihatan">
                                    <p>{{ $c10->category_name }}</p>
                                </a>
                            </div>
                            <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c11->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/bantuan pendidikan.png') }}"
                                        width="60rem" alt="bantuan pendidikan">
                                    <p>{{ $c11->category_name }}</p>
                                </a>
                            </div>
                            <div class="col">
                                <a href="{{ url('campaign/f') }}/{{ $c12->_id }}" class="d-block">
                                    <img src="{{ asset('backend/img/category_icon/bantuan perniagaan.png') }}"
                                        width="60rem" alt="bantuan perniagaan">
                                    <p>{{ $c12->category_name }}</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div class="card-row pt-1 px-4">
                <div class="col-md-12">
                    <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
                        <label class="btn btn-xxs btn-outline-primary rounded-pill" for="btnradio1"
                            onclick="window.location.href='{{ url('campaign') }}'">Semua</label>

                        @foreach ($category as $cat)
                            <input type="radio" class="btn-check" name="btnradio" id="btnradio{{ $cat->id }}"
                                autocomplete="off">
                            <label class="btn btn-xxs btn-outline-primary rounded-pill" for="btnradio{{ $cat->id }}"
                                onclick="window.location.href='{{ url('campaign/f') }}/{{ $cat->id }}'">{{ $cat->category_name }}</label>
                        @endforeach
                    </div>
                </div>
            </div> --}}

        </div>
        {{-- End Kategori --}}

        {{-- Semua Kempen --}}
        <div class="row mt-0">
            <div class="row px-4 mb-0">
                <div class="col-auto">
                    <h1 class="text-dark font-500 font-18">Semua Kempen</h1>
                </div>
            </div>

            <div class="row p-3 mb-0">
                @foreach ($campaigns->take(6) as $campaign)
                    {{-- @dd() --}}
                    <div class="col-6 g-0">
                        <div class="card">
                            <a href="{{ url('campaign/show') }}/{{ $campaign['slug'] }}">
                                <img src="{{ $campaign['image'] }}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <p class="card-text text-dark two-line-truncate line-height-s font-500">
                                        {{ $campaign['campaign_name'] }}</p>
                                    <div class="row mb-0">
                                        <div class="col-md-12 text-end">
                                            <span
                                                class="text-dark font-11 font-500">{{ number_format(($campaign->donation()->sum('amount') / $campaign['total_donation']) * 100, 2) }}%</span>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="progress" role="progressbar" aria-label="Basic example"
                                                aria-valuenow="{{ $campaign['amount'] }}" aria-valuemin="0"
                                                aria-valuemax="{{ $campaign['total_donation'] }}">
                                                <div class="progress-bar"
                                                    style="width: {{ ($campaign['amount'] / $campaign['total_donation']) * 100 }}%">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="text-dark"><b>RM{{ number_format($campaign->donation()->sum('amount')) }}
                                                </b></span><span class="text-secondary"> /
                                                RM{{ number_format($campaign['total_donation']) }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer p-0">
                                    <p class="color-blue-dark text-truncate p-1 mb-0" style="max-width: 200px">
                                        <img class="px-2" width="18%"
                                            src="{{ $campaign->company->company_profile }}" alt="logo">
                                        {{ $campaign->company->company_name }}
                                    </p>
                                </div>
                            </a>
                        </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="row px-4 mt-0">
                <a href="/campaign" class="btn btn-primary bg-blue font-500 rounded-2">LIHAT SEMUA</a>
            </div>
        </div>
        {{-- End Semua Kempen --}}
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    {{-- <script>
        $(document).ready(function() {
            $(document).on('click', '#heart-icon', function() {
                var campaignId = $(this).data('campaign-id');
                console.log(campaignId);

                $.ajax({
                    url: 'campaign/like',
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "campaign_id": campaignId
                    },
                    success: function() {
                        $('#heart-icon').removeClass('fa-heart-o').addClass('fa-heart');
                        console.log('Saved to database');
                    },
                    error: function() {
                        console.log('Error occurred');
                    }
                });
            });
        });
    </script> --}}

    <script>
        $(document).ready(function() {
            $('.fa-heart-o').on('click', function() {
                var campaignId = $(this).data('campaign-id');
                var icon = $(this); // Store reference to clicked icon
                console.log(campaignId);
                $.ajax({
                    url: 'campaign-like',
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "campaign_id": campaignId
                    },
                    success: function(response) {
                        icon.removeClass('fa-heart-o').addClass('fa-heart');
                        console.log('Saved to database');
                    },
                    error: function(error) {
                        console.log('Error occurred', error);
                    }
                });
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#myCarousel').carousel({
                // interval: 3000, // Time in milliseconds between slide transitions
                wrap: true // Set to true to enable continuous loop
            });
        });
    </script>
@endsection
